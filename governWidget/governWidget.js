define([
	"dojo",
    "dojo/Evented",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/has", // feature detection
	"esri/tasks/IdentifyTask",
	"esri/tasks/IdentifyParameters",
    "dijit/_WidgetBase",
    "dijit/a11yclick", // Custom press, release, and click synthetic events which trigger on a left mouse click, touch, or space/enter keyup.
    "dijit/_TemplatedMixin",
	"dijit/layout/TabContainer",
	"dijit/layout/ContentPane",
	"dijit/layout/LayoutContainer",
	"dijit/Dialog",
    "dojo/on",
    "dojo/Deferred",  
    "dojo/text!widgets/dijit/templates/governWidget.html", // template html
    "dojo/i18n!widgets/nls/jsapi", // localization
    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
	"dojo/request/handlers",
	"dojo/request"
],
function (
	d,
    Evented,
    declare,
    lang,
    has,
	IdentifyTask, IdentifyParameters,
    _WidgetBase, a11yclick, _TemplatedMixin,
	TabContainer, ContentPane, LayoutContainer,
	Dialog,
    on,
    Deferred,
    dijitTemplate, i18n,
    dom, domConstruct, domClass, domStyle,
	handlers, request
) {
    
	handlers.register("custom", function(response){
		//console.log("Custom Handler", response.text);
		var parser, xmlDoc;
		parser = new DOMParser();
		xmlDoc = parser.parseFromString(response.text,"text/xml");
		return xmlDoc;
	});
	
    var Widget = declare("csj.governWidget", [_WidgetBase, _TemplatedMixin, Evented], {
		
        // template HTML
        templateString: dijitTemplate,
		mapClickHandler: null,
		mainUIdialog: null,
		propertyLocationUIdialog: null,
		propertyAreaUIdialog: null,
		parcelInformationUIdialog: null,
		ownerInformationUIdialog: null,
		legalInformationUIdialog: null,
		multimediaUIdialog: null,
		assessmentUIdialog: null,
		parcelResults: null,
		sqlParcelResults: null,
		addressResults: null,
		propertyAreasResults: null,
		ownerResults: null,
		legalInfoResults: null,
		deedListResults: null,
		applicationListResults: null,
		assessmentYear: null,
		assessmentResults: null,
		attachedDocs: null,
		parcelPIDs: null,
		parcelCurrentIdx: 0,
		numOfAddresses: 0,
		addressCurrentIdx: 0,
		numOfPropertyAreas: 0,
		propertyAreasCurrentIdx: 1,
		numOfOwners: 0,
		ownerCurrentIdx: 0,
        
        // default options
        options: {
            theme: "governButton",
            map: null,
            extent: null,
            fit: false,
            visible: true,
			governWidgetService: "http://lap229/widgetService/widgetService.asmx",
			toolManager: null
        },
        
        // lifecycle: 1
        constructor: function(options, srcRefNode) {
            // mix in settings and defaults
            var defaults = lang.mixin({}, this.options, options);
            // widget node
            this.domNode = srcRefNode;
            // store localized strings
            this._i18n = i18n;
            // properties
            this.set("map", defaults.map);
            this.set("theme", defaults.theme);
            this.set("visible", defaults.visible);
            this.set("extent", defaults.extent);
            this.set("fit", defaults.fit);
			this.set("governWidgetService", defaults.governWidgetService);
			this.set("toolManager", defaults.toolManager);
            // listeners
            this.watch("theme", this._updateThemeWatch);
            this.watch("visible", this._visible);
            // classes
            this._css = {
                container: "homeContainer",
                govern: "governQueryParcel",
                loading: "loading",
				active: "activeTool"
            };
            
        },
        // bind listener for button to action
        postCreate: function() {
            this.inherited(arguments);
            this.own(
                on(this._homeNode, a11yclick, lang.hitch(this, this.Activate))
            );
        },
        // start widget. called by user
        startup: function() {
			console.log("Using Dojo v" + [d.version.major, d.version.minor, d.version.patch].join("."));
            // map not defined
            if (!this.map) {
                this.destroy();
                console.log('GovernWidget::map required');
            }
			
            // when map is loaded
            if (this.map.loaded) {
                this._init();
            } else {
                on.once(this.map, "load", lang.hitch(this, function() {
                    this._init();
                }));
            }
        },
        // connections/subscriptions will be cleaned up during the destroy() lifecycle phase
        destroy: function() {
            this.inherited(arguments);
        },
        /* ---------------- */
        /* Public Events */
        /* ---------------- */
		// ActivateQueryTool
        // DeactivateQueryTool
        // load
        /* ---------------- */
        /* Public Functions */
        /* ---------------- */
        Activate: function() {
			domClass.add(this._homeNode, this._css.active);
			//mapClickFunction = dijit.byId(this.map).onClick;
			//console.log(mapClickFunction);
			//dijit.byId(this.map).onClick = function(){};
			if (this.toolManager) {
				this.toolManager.set("currentTool", "governWidget");
			}
			
			if (this.mapClickHandler) {
				this.mapClickHandler.remove();
			}
            this.mapClickHandler = this.map.on("click", lang.hitch(this, function(evt){
				this._ProcessMapClick(evt);
			}));
        },
		
		Deactivate: function() {
			domClass.remove(this._homeNode, this._css.active);
			if (!this.mapClickHandler) {
				
			} else {
				//dijit.byId(this.map).onClick = function(){};
				this.mapClickHandler.remove();
				if (this.toolManager) {
					if (this.toolManager.get("currentTool")=="governWidget") {
						this.toolManager.set("currentTool", "");
					}
				}
			}
		},
        
        // show widget
        show: function(){
            this.set("visible", true);  
        },
        // hide widget
        hide: function(){
            this.set("visible", false);
        },
        /* ---------------- */
        /* Private Functions */
        /* ---------------- */
        _init: function() {
            // show or hide widget
            this._visible();
            // if no extent set, set extent to map extent
            if(!this.get("extent")){
                this.set("extent", this.map.extent);   
            }
            // widget is now loaded
            this.set("loaded", true);
            this.emit("load", {});
        },
        // show loading spinner
        _showLoading: function(){
            domClass.add(this._homeNode, this._css.loading);
        },
        // hide loading spinner
        _hideLoading: function(){
            domClass.remove(this._homeNode, this._css.loading);
        },
        // theme changed
        _updateThemeWatch: function(attr, oldVal, newVal) {
            domClass.remove(this.domNode, oldVal);
            domClass.add(this.domNode, newVal);
        },
        // show or hide widget
        _visible: function(){
            if(this.get("visible")){
                domStyle.set(this.domNode, 'display', 'block');
            }
            else{
                domStyle.set(this.domNode, 'display', 'none');
            }
        },
		
		_ProcessMapClick: function(evt){
			// show loading spinner
            this._showLoading();
			
			this.ownerCurrentIdx = 0;
			this.addressCurrentIdx = 0;
			this.parcelCurrentIdx = 0;
			this.propertyAreasCurrentIdx = 1;
			
			var identifyComplete = this._IdentifyParcel(evt);
			
			identifyComplete.then(lang.hitch(this, function(idResults){
				if (idResults.length > 0) {
					this._HandleIdentifyResults(idResults);
					var webServiceInfo = this._PerformWebServiceRequests();
					
					webServiceInfo.then(lang.hitch(this, function(result){
						this._hideLoading();
						//alert("Process Map Click");
						if (result=="Success") {
							var mainContent = this._UIMainContent();
							var mainDialog;
							if (!this.mainUIdialog) {
								mainDialog = new Dialog({
									title: "MS Govern Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx],
									content: mainContent
								});
								this.mainUIdialog = mainDialog;
							}else{
								mainDialog = this.mainUIdialog
								mainDialog.set("title", "MS Govern Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
								mainDialog.set("content", mainContent);
							}
							mainDialog.show();
							this._HandleUIMainButtons();
						}
						
					}));
						
				} else {
					console.log("No Parcel at clicked point.");
				}
				
			}));
		},
		
		_IdentifyParcel: function(evt){
			console.log(evt);
			
			var identifyTask, identifyParams;
			identifyTask = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_QueryTaskPublic_MapService/MapServer");
			identifyParams = new IdentifyParameters();
			identifyParams.tolerance = 1;
			identifyParams.returnGeometry = true;
			identifyParams.layerIds = [1];
			identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
			//console.log(this.map);
			identifyParams.width = this.map.width;
			identifyParams.height = this.map.height;
			identifyParams.geometry = evt.mapPoint;
			identifyParams.mapExtent = this.map.extent;

			var deferredIdentify = identifyTask.execute(identifyParams);
			return deferredIdentify;
		},
		
		_HandleIdentifyResults: function(idResults){
			console.log("idResults", idResults);
			var P_IDs = [];
			var numParcels = idResults.length;
			var index;
			for(index=0; index < numParcels; index++){
				P_IDs.push(idResults[index].value);
			}
			
			this.parcelPIDs = P_IDs;
			this.parcelCurrentIdx = 0;
		},
		
		_PerformWebServiceRequests: function(){
			var deferred = new Deferred();
			var getParcelInfoComplete = this._GetParcelInfo(this.parcelPIDs[this.parcelCurrentIdx]);
					
					getParcelInfoComplete.then(lang.hitch(this, function(text){
						//alert("ParcelInfoComplete");
						console.log("The server returned: ", text);
						//var parser, xmlDoc;
						//parser = new DOMParser();
						//xmlDoc = parser.parseFromString(text,"text/xml");
						//console.log("XML Doc: ", xmlDoc);
						var docNode = text.documentElement;
						this.parcelResults = docNode;
						var resultMessage = docNode.getElementsByTagName("Result")[0].textContent;
						if (resultMessage=="Success") {
							var getAddressInfoComplete = this._GetAddressInfo(this.parcelPIDs[this.parcelCurrentIdx]);
							
							getAddressInfoComplete.then(lang.hitch(this, function(addressText){
								//alert("AddressInfoComplete");
								console.log("The server returned: ", addressText);
								var addressDocNode = addressText.documentElement;
								this.addressResults = addressDocNode;
								var addressResultMessage = addressDocNode.getElementsByTagName("Result")[0].textContent;
								if (addressResultMessage=="Success") {
									var getOwnerInfoComplete = this._GetOwnerInfo(this.parcelPIDs[this.parcelCurrentIdx]);
							
									getOwnerInfoComplete.then(lang.hitch(this, function(ownerText){
										//alert("OwnerInfoComplete");
										console.log("The server returned: ", ownerText);
										var ownerDocNode = ownerText.documentElement;
										this.ownerResults = ownerDocNode;
										var ownerResultMessage = ownerDocNode.getElementsByTagName("Result")[0].textContent;
										if (ownerResultMessage=="Success") {
											
											deferred.resolve("Success");
											
										} else {
											alert("Fail");
											console.log("Get Owner Not Successful:", ownerResultMessage);
											deferred.resolve("Fail");
										}
									}));	
								} else {
									alert("Fail");
									console.log("Get Address Not Successful:", addressResultMessage);
									deferred.resolve("Fail");
								}
							}));	
						} else {
							alert("Fail");
							console.log("Get Parcel Not Successful:", resultMessage);
							deferred.resolve("Fail");
						}
					}));
			return deferred.promise;
		},
		
		_GetParcelInfo: function(P_ID){
			console.log("Get Parcel Info", P_ID);
			var deferred = new Deferred();
			//if (/OS 6_/.test(navigator.userAgent)) {
			//	$.ajaxSetup({ cache: false });
			//}
			//$.ajax({
			//	type: "POST",
			//	url: "http://localhost:61529/widgetService.asmx/GetInfoParcel",
			//	data: {
			//		P_ID: P_ID
			//	},
			//	success: function(result){
			//		alert("Got Post Request.");
			//		deferred.resolve(result);
			//	},
			//	DataType: "xml"
			//});
			//return deferred.promise;
			
			//var deferredParcelInfo = request.post("http://lap229/widgetService/widgetService.asmx/GetInfoParcel", {
			//	data: {
			//		P_ID: P_ID
			//	},
			//	handleAs: "custom",
			//	headers: {
			//		"X-Requested-With": null
			//	}
			//});
        
			deferredParcelInfo = request.get(this.governWidgetService + "/GetInfoParcel?P_ID=" + P_ID, {
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredParcelInfo;
		},
		
		_GetSQLParcelInfo: function(P_ID){
			console.log("Get SQL Parcel Info", P_ID);
			var deferredParcelInfo = request.get(this.governWidgetService + "/GetSQLParcelInfo?P_ID=" + P_ID, {
			//var deferredParcelInfo = request.post("http://localhost:61529/widgetService.asmx/GetSQLParcelInfo", {
			//	data: {
			//		P_ID: P_ID
			//	},
			//	handleAs: "custom",
			//	headers: {
			//		"X-Requested-With": null
			//	}
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredParcelInfo;
		},
		
		_GetAddressInfo: function(P_ID){
			console.log("Get Address Info", P_ID);
			var deferredAddressInfo = request.get(this.governWidgetService + "/GetInfoAddress?P_ID=" + P_ID, {
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredAddressInfo;
		},
		
		_GetOwnerInfo: function(P_ID){
			console.log("Get Owner Info", P_ID);
			var deferredOwnerInfo = request.get(this.governWidgetService + "/GetInfoOwner?P_ID=" + P_ID, {
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredOwnerInfo;
		},
		
		_GetPropertyAreasInfo: function(P_ID, Year_ID, Frozen_ID){
			console.log("Get Property Areas Info", P_ID);
			var deferredPropertyAreasInfo = request.get(this.governWidgetService + "/GetInfoPropertyArea?P_ID=" + P_ID + "&Year_ID=" + Year_ID + "&Frozen_ID=" + Frozen_ID, {
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredPropertyAreasInfo;
		},
		
		_GetLegalInfo: function(P_ID, Year_ID){
			console.log("Get Legal Info", P_ID);
			var deferredLegalInfo = request.get(this.governWidgetService + "/GetLegalInfo?P_ID=" + P_ID + "&Year_ID=" + Year_ID, {
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredLegalInfo;
		},
		
		_GetMultimedia: function(P_ID){
			console.log("Get Multimedia", P_ID);
			//var deferredMultimedia = request.get()
			//var deferredMultimedia = request.post("http://localhost:61529/widgetService.asmx/GetDeedList", {
			var deferredMultimedia = request.get(this.governWidgetService + "/GetDeedList?P_ID=" + P_ID, {
				//data: {
				//	P_ID: P_ID
				//},
				//handleAs: "custom",
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredMultimedia;
		},
		
		_GetApplications: function(P_ID){
			console.log("Get Applications", P_ID);
			var deferredApplications = request.get(this.governWidgetService + "/GetApplicationList?P_ID=" + P_ID, {
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredApplications;
		},
		
		_GetAttachmentList: function(PM_ID){
			console.log("Get Attachment List", PM_ID);
			var deferredAttachments = request.get(this.governWidgetService + "/GetApplicationAttachmentList?PM_ID=" + PM_ID, {
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredAttachments;
		},
		
		_GetAssessment: function(P_ID, Year_ID){
			console.log("Get Assessment", P_ID);
			var deferredAssessment = request.get(this.governWidgetService + "/GetAssessment?P_ID=" + P_ID + "&Year_ID=" + Year_ID, {
				headers: {
					"X-Requested-With": null
				},
				handleAs: "custom"
			});
			return deferredAssessment;
		},
		
		// Create UI content
		_UIMainContent: function(){
			var docNode = this.parcelResults;
			var taxMap = docNode.getElementsByTagName("TaxMapNumber")[0].textContent;
			var legalIndex = docNode.getElementsByTagName("LegalIndex")[0].textContent;
			var subdivision = docNode.getElementsByTagName("Subdivision")[0].textContent;
			var lot = docNode.getElementsByTagName("Lot")[0].textContent;
			var ownerText = "";
			var addressText = "";
			
			
			var addressDocNode = this.addressResults;
			var addresses = addressDocNode.getElementsByTagName("DC_MSGovern_ParcelAddress");
			var numOfAddresses = addresses.length;
			var addressIndex;
			var addressLines = 0;
			for(addressIndex=0; addressIndex < numOfAddresses; addressIndex++){
				var address = addresses[addressIndex].getElementsByTagName("FormatedAddress")[0].textContent;
				if (address.length > 0) {
					addressLines += 1;
					if (addressLines > 1) {
						addressText += "\n" + address;
					} else {
						addressText += address;
					}
				}
			}
			
			var ownersDocNode = this.ownerResults;
			var owners = ownersDocNode.getElementsByTagName("DC_MSGovern_Owner");
			var numOfOwners = owners.length;
			var ownerIndex;
			var ownerLines = 0;
			for(ownerIndex=0; ownerIndex < numOfOwners; ownerIndex++){
				var status = owners[ownerIndex].getElementsByTagName("Status")[0].textContent;
				if (status=='Owner') {
					var nameInfo = ownersDocNode.getElementsByTagName("DC_MSGovern_NameInfo")[ownerIndex]
					var companyName = nameInfo.getElementsByTagName("Company")[0].textContent;
					var name1 = nameInfo.getElementsByTagName("FirstName")[0].textContent + " " + nameInfo.getElementsByTagName("MidInitial")[0].textContent + " " + nameInfo.getElementsByTagName("LastName")[0].textContent;
					var name2 = nameInfo.getElementsByTagName("Name2")[0].textContent;
					if (companyName.length > 0) {
						if (ownerLines > 0) {
							ownerText += "\n" + companyName;
							ownerLines += 1;
						} else {
							ownerText += companyName;
							ownerLines += 1;
						}
					} else {
						if (ownerLines > 0) {
							ownerText += "\n" + name1;
							ownerLines += 1;
						} else {
							ownerText += name1;
							ownerLines += 1;
						}
						if (name2.length > 0) {
							if (ownerLines > 0) {
								ownerText += "\n" + name2;
								ownerLines += 1;
							} else {
								ownerText += name2;
								ownerLines += 1;
							}
						}
					}
				}
			}
			var mainContent = "<div style='width:640px; height:480px;' class='governUI'>";
			mainContent += "<table width=100%>";
			mainContent += "<tr><td>Tax Map Number / Roll #</td><td>Parcel Index</td><td rowspan=2><input type='image' id='btnPropertyLocationMaintenance' src='governWidget/dijit/templates/images/PropertyLocationMaintenanceButton.png'></td></tr>";
			mainContent += "<tr><td><input type='text' value='" + taxMap + "' style='width:250px;'></td><td>" + legalIndex + "</td></tr>";
			mainContent += "<tr><td>Subdivision Name / Lot</td><td></td><td rowspan=2><input type='image' id='btnPropertyArea' src='governWidget/dijit/templates/images/PropertyAreaButton.png'></td></tr>";
			mainContent += "<tr><td><input type='text' value='" + subdivision + "' style='width:200px;'><input type='text' value='" + lot + "' style='width:50px;'></td><td></td></tr>";
			mainContent += "<tr><td>Subdivision Number</td><td>Owner Name / Mailing Address</td><td rowspan=2><input type='image' id='btnParcelInformation' src='governWidget/dijit/templates/images/ParcelInformationButton.png'></td></tr>";
			mainContent += "<tr><td></td><td rowspan=2><textarea style='width:250px; height:48px; font-family: Arial;'>" + ownerText + "</textarea></td></tr>";
			mainContent += "<tr><td>Property Location</td><td rowspan=2><input type='image' id='btnOwnerInformation' src='governWidget/dijit/templates/images/OwnerInformationButton.png'></td></tr>";
			mainContent += "<tr><td rowspan=2><textarea style='width:250px; height:48px; font-family: Arial;'>" + addressText + "</textarea></td><td></td></tr>";
			//mainContent += "<tr><td rowspan=2><select size=2 style='width:250px;'>" + addressText + "</select></td><td>Project</td></tr>";
			mainContent += "<tr><td rowspan=2></td><td rowspan=2><input type='image' id='btnLegalDescription' src='governWidget/dijit/templates/images/LegalDescriptionButton.png'></td></tr>";
			mainContent += "<tr><td></td></tr>";
			mainContent += "<tr><td></td><td></td><td rowspan=2><input type='image' id='btnMultimedia' src='governWidget/dijit/templates/images/MultimediaButton.png'></td></tr>";
			mainContent += "<tr><td ></td><td></td></tr>";
			//mainContent += "<tr><td>Site Name</td></tr>";
			//mainContent += "<tr><td rowspan=2></td><td>Occupant Names</td><td rowspan=2><input type='image' id='btnMultimedia' src='governWidget/dijit/templates/images/MultimediaButton.png'></td></tr>";
			//mainContent += "<tr><td rowspan=2></td><td></td></tr>";
			//mainContent += "<tr><td>Split/Merge/Virtual Parcels</td><td></td></tr>";
			//mainContent += "<tr><td rowspan=2></td><td>Parcel(s) Linked</td><td></td></tr>";
			//mainContent += "<tr><td rowspan=2></td><td></td></tr>";
			//mainContent += "<tr><td>Browse and Exit Buttons</td><td></td></tr>";
			mainContent += "<tr><td colspan=2><span><input type='image' id='btnMainUIFirstRecord' src='governWidget/dijit/templates/images/FirstRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='image' id='btnMainUIPrevRecord' src='governWidget/dijit/templates/images/PreviousRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='text' value='" + (this.parcelCurrentIdx+1) + " of " + this.parcelPIDs.length + "' style='height:24px; width:400px; padding:0px; margin:0px; vertical-align:middle;'><input type='image' id='btnMainUINextRecord' src='governWidget/dijit/templates/images/NextRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='image' id='btnMainUILastRecord' src='governWidget/dijit/templates/images/LastRecord.png' style='height:24px; width:24px; vertical-align:middle;'></span></td><td></td></tr>";
			mainContent += "<tr><td colspan=2><input type='image' id='btnPropertyAssessment' src='governWidget/dijit/templates/images/PropertyAssessmentButton.png' style='display:block; margin-left:auto; margin-right:auto;'></td><td></td></tr>";
			mainContent += "</table>";
			mainContent += "</div>";
			return mainContent;
		},
		
		_HandleUIMainButtons: function(){
			var btnPropertyLocMaint = dom.byId("btnPropertyLocationMaintenance");
			on(btnPropertyLocMaint, "click", lang.hitch(this, function(){
				var propertyLocationContent = this._UIPropertyLocationContent();
				var propertyLocationDialog;
				if (!this.propertyLocationUIdialog){
					propertyLocationDialog = new Dialog({
					title: "Property Location Maintenance - PID: " + this.parcelPIDs[this.parcelCurrentIdx],
					content: propertyLocationContent
					});
					this.propertyLocationUIdialog = propertyLocationDialog;
				} else {
					propertyLocationDialog = this.propertyLocationUIdialog;
					propertyLocationDialog.set("title", "Property Location Maintenance - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
					propertyLocationDialog.set("content", propertyLocationContent);
				}
				propertyLocationDialog.show();
				this._HandleUIPropertyLocationButtons();
			}));
			var btnPropertyArea = dom.byId("btnPropertyArea");
			on(btnPropertyArea, "click", lang.hitch(this, function(){
				var getPropertyAreaInfoComplete = this._GetPropertyAreasInfo(this.parcelPIDs[this.parcelCurrentIdx], 2017, 0);
							
				getPropertyAreaInfoComplete.then(lang.hitch(this, function(propertyAreasText){
					console.log("The server returned: ", propertyAreasText);
					var propertyAreasDocNode = propertyAreasText.documentElement;
					//propertyAreasDocNode = propertyAreasDocNode.getElementsByTagName("DocumentElement");
					this.propertyAreasResults = propertyAreasDocNode;
					var propertyAreaContent = this._UIPropertyAreaContent();
					var propertyAreaDialog;
					if (!this.propertyAreaUIdialog){
						propertyAreaDialog = new Dialog({
						title: "Property Area - PID: " + this.parcelPIDs[this.parcelCurrentIdx],
						content: propertyAreaContent
						});
						this.propertyAreaUIdialog = propertyAreaDialog;
					} else {
						propertyAreaDialog = this.propertyAreaUIdialog;
						propertyAreaDialog.set("title", "Property Area - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
						propertyAreaDialog.set("content", propertyAreaContent);
					}
					propertyAreaDialog.show();
					this._HandleUIPropertyAreaButtons();
				}));			
				
			}));
			var btnParcelInformation = dom.byId("btnParcelInformation");
			on(btnParcelInformation, "click", lang.hitch(this, function(){
				var getParcelInfoComplete = this._GetSQLParcelInfo(this.parcelPIDs[this.parcelCurrentIdx]);
							
				getParcelInfoComplete.then(lang.hitch(this, function(parcelInfoText){
					console.log("The server returned: ", parcelInfoText);
					this.sqlParcelResults = parcelInfoText.getElementsByTagName("DocumentElement")[0];
					var parcelInfoContent = this._UIParcelInfoContent();
					var parcelInfoDialog;
					if (!this.parcelInformationUIdialog){
						parcelInfoDialog = new Dialog({
						title: "Parcel Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx],
						content: parcelInfoContent
						});
						this.parcelInformationUIdialog = parcelInfoDialog;
					} else {
						parcelInfoDialog = this.parcelInformationUIdialog;
						parcelInfoDialog.set("title", "Parcel Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
						parcelInfoDialog.set("content", parcelInfoContent);
					}
					parcelInfoDialog.show();
				}));

			}));
			var btnOwnerInformation = dom.byId("btnOwnerInformation");
			on(btnOwnerInformation, "click", lang.hitch(this, function(){
				var ownerInfoContent = this._UIOwnerInfoContent();
				var ownerInfoDialog;
				if (!this.ownerInformationUIdialog){
					ownerInfoDialog = new Dialog({
					title: "Owner Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx],
					content: ownerInfoContent
					});
					this.ownerInformationUIdialog = ownerInfoDialog;
				} else {
					ownerInfoDialog = this.ownerInformationUIdialog;
					ownerInfoDialog.set("title", "Owner Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
					ownerInfoDialog.set("content", ownerInfoContent);
				}
				ownerInfoDialog.show();
				this._HandleUIOwnerInfoButtons();
			}));
			var btnLegalInformation = dom.byId("btnLegalDescription");
			on(btnLegalInformation, "click", lang.hitch(this, function(){
				var currentDate = new Date();
				var currentYear = currentDate.getFullYear();
				var getLegalInfoComplete = this._GetLegalInfo(this.parcelPIDs[this.parcelCurrentIdx], currentYear);
					
				getLegalInfoComplete.then(lang.hitch(this, function(legalInfoText){
					console.log("The server returned: ", legalInfoText);
					this.legalInfoResults = legalInfoText.getElementsByTagName("DocumentElement")[0];
					var legalInfoContent = this._UILegalInfoContent();
					var legalInfoDialog;
					if (!this.legalInformationUIdialog){
						legalInfoDialog = new Dialog({
						title: "Legal Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx],
						content: legalInfoContent
						});
						this.legalInformationUIdialog = legalInfoDialog;
					} else {
						legalInfoDialog = this.legalInformationUIdialog;
						legalInfoDialog.set("title", "Legal Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
						legalInfoDialog.set("content", legalInfoContent);
					}
					legalInfoDialog.show();
					//this._HandleUIOwnerInfoButtons();
				}));
				
			}));
			var btnMultimedia = dom.byId("btnMultimedia");
			on(btnMultimedia, "click", lang.hitch(this, function(){
				var getMultimediaComplete = this._GetMultimedia(this.parcelPIDs[this.parcelCurrentIdx]);
							
				getMultimediaComplete.then(lang.hitch(this, function(multimediaText){
					console.log("The server returned: ", multimediaText);
					this.deedListResults = multimediaText.getElementsByTagName("DocumentElement")[0];
					var getApplicationsComplete = this._GetApplications(this.parcelPIDs[this.parcelCurrentIdx]);
					
					getApplicationsComplete.then(lang.hitch(this, function(applicationsText){
						console.log("The server returned: ", applicationsText);
						this.applicationListResults = applicationsText.getElementsByTagName("DocumentElement")[0];
						var multimediaContent = this._UIMultimediaContent();
						var multimediaDialog;
						if (!this.multimediaUIdialog){
							multimediaDialog = new Dialog({
							title: "Multimedia Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx],
							content: multimediaContent
							});
							this.multimediaUIdialog = multimediaDialog;
						} else {
							multimediaDialog = this.multimediaUIdialog;
							multimediaDialog.set("title", "Multimedia Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
							multimediaDialog.set("content", multimediaContent);
						}
						multimediaDialog.show();
						this._HandleUIMultimediaButtons();
					}));
					
				}));
				
			}));
			var btnAssessmentInformation = dom.byId("btnPropertyAssessment");
			on(btnAssessmentInformation, "click", lang.hitch(this, function(){
				var currentDate = new Date();
				var currentYear = currentDate.getFullYear();
				if (!this.assessmentYear) {
					this.assessmentYear = currentYear;
				}
				var getAssessmentComplete = this._GetAssessment(this.parcelPIDs[this.parcelCurrentIdx], this.assessmentYear);
					
				getAssessmentComplete.then(lang.hitch(this, function(assessmentText){
					console.log("The server returned: ", assessmentText);
					this.assessmentResults = assessmentText.getElementsByTagName("DocumentElement")[0];
					var assessmentInfoContent = this._UIAssessmentInfoContent();
					var assessmentInfoDialog;
					if (!this.assessmentUIdialog){
						assessmentInfoDialog = new Dialog({
						title: "Property Assessment - PID: " + this.parcelPIDs[this.parcelCurrentIdx],
						content: assessmentInfoContent
						});
						this.assessmentUIdialog = assessmentInfoDialog;
					} else {
						assessmentInfoDialog = this.assessmentUIdialog;
						assessmentInfoDialog.set("title", "Property Assessment - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
						assessmentInfoDialog.set("content", assessmentInfoContent);
					}
					assessmentInfoDialog.show();
					this._HandleUIAssessmentInfoButtons();
				}));
				
			}));
			var btnNextRecord = dom.byId("btnMainUINextRecord");
			on(btnNextRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.parcelCurrentIdx);
				if (this.parcelCurrentIdx+1 < this.parcelPIDs.length) {
					this.parcelCurrentIdx += 1;
					var webServiceInfo = this._PerformWebServiceRequests();
					
					webServiceInfo.then(lang.hitch(this, function(result){	
						if (result == "Success") {
							mainDialog = this.mainUIdialog;
							mainContent = this._UIMainContent();
							mainDialog.set("title", "MS Govern Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
							mainDialog.set("content", mainContent);
							this._HandleUIMainButtons();
						}
					}));
				}
			}));
			var btnPrevRecord = dom.byId("btnMainUIPrevRecord");
			on(btnPrevRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.parcelCurrentIdx);
				if (this.parcelCurrentIdx > 0) {
					this.parcelCurrentIdx -= 1;
					var webServiceInfo = this._PerformWebServiceRequests();
					
					webServiceInfo.then(lang.hitch(this, function(result){	
						if (result == "Success") {
							mainDialog = this.mainUIdialog;
							mainContent = this._UIMainContent();
							mainDialog.set("title", "MS Govern Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
							mainDialog.set("content", mainContent);
							this._HandleUIMainButtons();
						}
						
					}));
				}
			}));
			var btnFirstRecord = dom.byId("btnMainUIFirstRecord");
			on(btnFirstRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.parcelCurrentIdx);
				if (this.parcelCurrentIdx != 0) {
					this.parcelCurrentIdx = 0;
					var webServiceInfo = this._PerformWebServiceRequests();
					
					webServiceInfo.then(lang.hitch(this, function(result){	
						if (result == "Success") {
							mainDialog = this.mainUIdialog;
							mainContent = this._UIMainContent();
							mainDialog.set("title", "MS Govern Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
							mainDialog.set("content", mainContent);
							this._HandleUIMainButtons();
						}
						
					}));
				}
			}));
			var btnLastRecord = dom.byId("btnMainUILastRecord");
			on(btnLastRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.parcelCurrentIdx);
				if (this.parcelCurrentIdx != this.parcelPIDs.length -1) {
					this.parcelCurrentIdx = this.parcelPIDs.length -1;
					var webServiceInfo = this._PerformWebServiceRequests();
					
					webServiceInfo.then(lang.hitch(this, function(result){	
						if (result == "Success") {
							mainDialog = this.mainUIdialog;
							mainContent = this._UIMainContent();
							mainDialog.set("title", "MS Govern Information - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
							mainDialog.set("content", mainContent);
							this._HandleUIMainButtons();
						}
						
					}));
				}
			}));
		},
		
		_UIPropertyLocationContent: function(){
			var addressDocNode = this.addressResults;
			var addresses = addressDocNode.getElementsByTagName("DC_MSGovern_ParcelAddress");
			var numOfAddresses = addresses.length;
			this.numOfAddresses = numOfAddresses;
			var address;
			
			var LocLineBefore = "";
			var HouseNo1 = "";
			var HouseNo2 = "";
			var cboDir = "";
			var StreetName = "";
			var StreetSuffix = "";
			var cboDir2 = "";
			var SecAddressInd = "";
			var UnitNum = "";
			var LocLineAfter = "";
			var InactiveYear = "";
			var City = "";
			var Postal = "";
			var chkMainLoc = "";
			
			if (numOfAddresses > 0) {
				address = addresses[this.addressCurrentIdx];
				console.log(address);
				LocLineBefore = address.getElementsByTagName("LineBefore")[0].textContent;
				HouseNo1 = address.getElementsByTagName("CivicNumber")[0].textContent;
				HouseNo2 = address.getElementsByTagName("CivicAlphaPart")[0].textContent;
				cboDir = address.getElementsByTagName("PreDirection")[0].textContent;
				cboDir = "<option value='" + cboDir + "'>" + cboDir + "</option>";
				StreetName = address.getElementsByTagName("Street")[0].textContent;
				StreetName = StreetName.replace("'", "&#39");
				StreetSuffix = address.getElementsByTagName("Suffix")[0].textContent;
				cboDir2 = address.getElementsByTagName("PostDirection")[0].textContent;
				cboDir2 = "<option value='" + cboDir2 + "'>" + cboDir2 + "</option>";
				SecAddressInd = address.getElementsByTagName("SecondAddressIndex")[0].textContent;
				UnitNum = address.getElementsByTagName("Unit")[0].textContent;
				LocLineAfter = address.getElementsByTagName("LineAfter")[0].textContent;
				//InactiveYear = address.getElementsByTagName("")
				City = address.getElementsByTagName("City")[0].textContent;
				City = City.replace("'", "&#39");
				Postal = address.getElementsByTagName("ZipPostalCode")[0].textContent;
				chkMainLoc = address.getElementsByTagName("LocationSequence")[0].textContent;
				if (chkMainLoc < 1) {
					chkMainLoc = " checked";
				} else {
					chkMainLoc = "";
				}
			}
			
			var propertyLocationContent = "<div style='width:640px; height:480px;' class='governUI'>";
			propertyLocationContent += "<table width=100%>";
			propertyLocationContent += "<tr><td></td><td colspan=2>Location Line Before</td><td></td></tr>";
			propertyLocationContent += "<tr><td></td><td colspan=2><input type='text' value='" + LocLineBefore + "'></td><td></td></tr>";
			propertyLocationContent += "<tr><td>House No.</td><td>Direction</td><td>Street Name</td><td>Street Suffix</td></tr>";
			propertyLocationContent += "<tr><td><input type='text' value='" + HouseNo1 + "' style='width:50px;'><input type='text' value='" + HouseNo2 + "' style='width:50px;'></td><td><select>" + cboDir + "</select></td><td><input type='text' value='" + StreetName + "'></td><td><select></select></td></tr>";
			propertyLocationContent += "<tr><td></td><td>Direction</td><td>Secondary Address Indicator</td><td>Unit Number</td></tr>";
			propertyLocationContent += "<tr><td></td><td><select>" + cboDir2 + "</select></td><td><select></select></td><td><input type='text' value='" + UnitNum + "'></td></tr>";
			propertyLocationContent += "<tr><td></td><td colspan=2>Location Line After</td><td></td></tr>";
			propertyLocationContent += "<tr><td></td><td colspan=2><input type='text' value='" + LocLineAfter + "'></td><td></td></tr>";
			propertyLocationContent += "<tr><td>Inactive Year</td><td colspan=2>City</td><td>Postal Code</td></tr>";
			propertyLocationContent += "<tr><td><input type='text' style='width:100px;'></td><td colspan=2><input type='text' value='St. John&#39s'></td><td><input type='text' value='" + Postal + "'></td></tr>";
			propertyLocationContent += "<tr><td></td><td colspan=2><span><input type='checkbox'" + chkMainLoc + ">Main Location</span><span><input type='checkbox'>Inactive Address</span></td><td></td></tr>";
			propertyLocationContent += "</table>";
			propertyLocationContent += "<div class='recordNav'>";
			propertyLocationContent += "<span><input type='image' id='btnLocInfoUIFirstRecord' src='governWidget/dijit/templates/images/FirstRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='image' id='btnLocInfoUIPrevRecord' src='governWidget/dijit/templates/images/PreviousRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='text' value='" + (this.addressCurrentIdx+1) + " of " + numOfAddresses + "' style='height:24px; width:400px; padding:0px; margin:0px; vertical-align:middle;'><input type='image' id='btnLocInfoUINextRecord' src='governWidget/dijit/templates/images/NextRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='image' id='btnLocInfoUILastRecord' src='governWidget/dijit/templates/images/LastRecord.png' style='height:24px; width:24px; vertical-align:middle;'></span>"
			propertyLocationContent += "</div>";
			propertyLocationContent += "</div>";
			return propertyLocationContent;
		},
		
		_HandleUIPropertyLocationButtons: function(){
			var btnNextRecord = dom.byId("btnLocInfoUINextRecord");
			on(btnNextRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.addressCurrentIdx);
				if (this.addressCurrentIdx+1 < this.numOfAddresses) {
					this.addressCurrentIdx += 1;			
					propertyLocationDialog = this.propertyLocationUIdialog;
					propertyLocationContent = this._UIPropertyLocationContent();
					propertyLocationDialog.set("title", "Property Location Maintenance");
					propertyLocationDialog.set("content", propertyLocationContent);
					this._HandleUIPropertyLocationButtons();
				}
			}));
			var btnPrevRecord = dom.byId("btnLocInfoUIPrevRecord");
			on(btnPrevRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.addressCurrentIdx);
				if (this.addressCurrentIdx > 0) {
					this.addressCurrentIdx -= 1;
					propertyLocationDialog = this.propertyLocationUIdialog;
					propertyLocationContent = this._UIPropertyLocationContent();
					propertyLocationDialog.set("title", "Property Location Maintenance");
					propertyLocationDialog.set("content", propertyLocationContent);
					this._HandleUIPropertyLocationButtons();
				}
			}));
			var btnFirstRecord = dom.byId("btnLocInfoUIFirstRecord");
			on(btnFirstRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.addressCurrentIdx);
				if (this.addressCurrentIdx != 0) {
					this.addressCurrentIdx = 0;
					propertyLocationDialog = this.propertyLocationUIdialog;
					propertyLocationContent = this._UIPropertyLocationContent();
					propertyLocationDialog.set("title", "Property Location Maintenance");
					propertyLocationDialog.set("content", propertyLocationContent);
					this._HandleUIPropertyLocationButtons();
				}
			}));
			var btnLastRecord = dom.byId("btnLocInfoUILastRecord");
			on(btnLastRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.addressCurrentIdx);
				if (this.addressCurrentIdx != this.numOfAddresses -1) {
					this.addressCurrentIdx = this.numOfAddresses -1;
					propertyLocationDialog = this.propertyLocationUIdialog;
					propertyLocationContent = this._UIPropertyLocationContent();
					propertyLocationDialog.set("title", "Property Location Maintenance");
					propertyLocationDialog.set("content", propertyLocationContent);
					this._HandleUIPropertyLocationButtons();
				}
			}));
		},
		
		_UIPropertyAreaContent: function(){
			var propertyAreaDocNode = this.propertyAreasResults;
			var propertyAreas = propertyAreaDocNode.getElementsByTagName("PropertyAreas");
			var numOfPropertyAreas = propertyAreas.length - 1;
			this.numOfPropertyAreas = numOfPropertyAreas;
			var propertyArea;
			
			var zoning = "";
			var zoningPercent = "";
			var nbhd = "";
			var subNBHD = "";
			var floodZone = "";
			var ward = "";
			var ward2017 = "";
			var bia = "";
			var esa = "";
			var heritageArea = "";
			var bga = "";
			var incNBHD = "";
			var inspTerritory = "";
			var cityDistrict = "";
			var revTrackingArea = "";
			var munPlanDist = "";
			var planArea = "";
			var heritageBldg = "";
			var chkHeritageBldg = "";
			var devAgree = "";
			var archaelogicalSite = "";
			var waterway = "";
			var watershed = "";
			var wetland = "";
			var gouldsFloodPlain = "";
			var floodHazArea = "";
			var eva = "";
			var batteryDevArea = "";
			var lightPlanes = "";
			var landfillArea = "";
			var landScreen = "";
			var dtParkingArea = "";
			var resParkingArea = "";
			var churchillSquare = "";
			
			if (numOfPropertyAreas > 0) {
				propertyArea = propertyAreas[this.propertyAreasCurrentIdx];
				console.log(propertyArea);
				zoning = propertyArea.getElementsByTagName("ZONING_Name")[0]
				zoning = (!zoning) ? "" : zoning.textContent;
				zoningPercent = propertyArea.getElementsByTagName("ZONE_PCNT")[0].textContent;
				nbhd = propertyArea.getElementsByTagName("NBHD_Name")[0];
				nbhd = (!nbhd) ? "" : nbhd.textContent;
				subNBHD = propertyArea.getElementsByTagName("SUB_NBHD")[0];
				subNBHD = (!subNBHD) ? "" : subNBHD.textContent;
				floodZone = propertyArea.getElementsByTagName("FLOOD_ZONE")[0];
				floodZone = (!floodZone) ? "" : floodZone.textContent;
				ward = propertyArea.getElementsByTagName("WARD")[0];
				ward = (!ward) ? "" : ward.textContent;
				ward2017 = propertyArea.getElementsByTagName("NEW_WARD")[0];
				ward2017 = (!ward2017) ? "" : ward2017.textContent;
				bia = propertyArea.getElementsByTagName("DIST_OTHER_Name")[0];
				bia = (!bia) ? "" : bia.textContent;
				esa = propertyArea.getElementsByTagName("ESA")[0];
				esa = (!esa) ? "" : esa.textContent;
				heritageArea = propertyArea.getElementsByTagName("HERITAGE")[0];
				heritageArea = (!heritageArea) ? "" : heritageArea.textContent;
				bga = propertyArea.getElementsByTagName("BGA")[0];
				bga = (!bga) ? "" : bga.textContent;
				incNBHD = propertyArea.getElementsByTagName("INC_NBHD")[0];
				incNBHD = (!incNBHD) ? "" : incNBHD.textContent;
				inspTerritory = propertyArea.getElementsByTagName("INSP_TERRITORY")[0];
				inspTerritory = (!inspTerritory) ? "" : inspTerritory.textContent;
				cityDistrict = propertyArea.getElementsByTagName("DIST_CITY")[0];
				cityDistrict = (!cityDistrict) ? "" : cityDistrict.textContent;
				revTrackingArea = propertyArea.getElementsByTagName("REVENU_AREA")[0];
				revTrackingArea = (!revTrackingArea) ? "" : revTrackingArea.textContent;
				
				munPlanDist = propertyArea.getElementsByTagName("MUN_PLAN_Name")[0];
				munPlanDist = (!munPlanDist) ? "" : munPlanDist.textContent;
				planArea = propertyArea.getElementsByTagName("PLAN_AREA_Name")[0];
				planArea = (!planArea) ? "" : planArea.textContent;
				planArea = planArea.replace("'", "&#39");
				heritageBldg = propertyArea.getElementsByTagName("HERI_BLDG")[0];
				heritageBldg = (!heritageBldg) ? "" : heritageBldg.textContent;
				console.log("heritageBldg", heritageBldg);
				if (heritageBldg == -1) {
					chkHeritageBldg = " checked";
				}
				devAgree = propertyArea.getElementsByTagName("DEV_AGREE")[0];
				devAgree = (!devAgree) ? "" : devAgree.textContent;
				archaelogicalSite = propertyArea.getElementsByTagName("ARCH_SITE")[0];
				archaelogicalSite = (!archaelogicalSite) ? "" : archaelogicalSite.textContent;
				waterway = propertyArea.getElementsByTagName("WATERWAY")[0];
				waterway = (!waterway) ? "" : waterway.textContent;
				watershed = propertyArea.getElementsByTagName("WATERSHED")[0];
				watershed = (!watershed) ? "" : watershed.textContent;
				wetland = propertyArea.getElementsByTagName("WETLAND")[0];
				wetland = (!wetland) ? "" : wetland.textContent;
				gouldsFloodPlain = propertyArea.getElementsByTagName("GOULDS_FLOO")[0];
				gouldsFloodPlain = (!gouldsFloodPlain) ? "" : gouldsFloodPlain.textContent;
				floodHazArea = propertyArea.getElementsByTagName("FLOOD_HAZ")[0];
				floodHazArea = (!floodHazArea) ? "" : floodHazArea.textContent;
				eva = propertyArea.getElementsByTagName("EVA")[0];
				eva = (!eva) ? "" : eva.textContent;
				batteryDevArea = propertyArea.getElementsByTagName("BATTERY")[0];
				batteryDevArea = (!batteryDevArea) ? "" : batteryDevArea.textContent;
				lightPlanes = propertyArea.getElementsByTagName("LIGHT_PLAN")[0];
				lightPlanes = (!lightPlanes) ? "" : lightPlanes.textContent;
				landfillArea = propertyArea.getElementsByTagName("LANDFILL")[0];
				landfillArea = (!landfillArea) ? "" : landfillArea.textContent;
				landScreen = propertyArea.getElementsByTagName("LAND_SCREE")[0];
				landScreen = (!landScreen) ? "" : landScreen.textContent;
				dtParkingArea = propertyArea.getElementsByTagName("DT_PARK")[0];
				dtParkingArea = (!dtParkingArea) ? "" : dtParkingArea.textContent;
				resParkingArea = propertyArea.getElementsByTagName("RES_PARK")[0];
				resParkingArea = (!resParkingArea) ? "" : resParkingArea.textContent;
				churchillSquare = propertyArea.getElementsByTagName("CHURCHILL")[0];
				churchillSquare = (!churchillSquare) ? "" : churchillSquare.textContent;
			}
			
			var generalTabContent = "<table width=100%>";
			generalTabContent += "<tr><td>Current Zoning</td><td>Neighbourhood Code</td><td>BIA</td></tr>";
			generalTabContent += "<tr><td><input type='text' value='" + zoning + "'></td><td><input type='text' value='" + nbhd + "'></td><td><input type='text' value='" + bia + "'></td></tr>";
			generalTabContent += "<tr><td>Zoning Percent</td><td>Sub Neighbourhood</td><td>Flood Zone</td></tr>";
			generalTabContent += "<tr><td><input type='text' value='" + zoningPercent + "'></td><td><input type='text' value='" + subNBHD + "'></td><td><input type='text' value='" + floodZone + "'></td></tr>";
			generalTabContent += "<tr><td>Ward</td><td>Ward 2017</td><td>Environmental Service Area</td></tr>";
			generalTabContent += "<tr><td><input type='text' value='" + ward + "'></td><td><input type='text' value='" + ward2017 + "'></td><td><input type='text' value='" + esa + "'></td></tr>";
			generalTabContent += "<tr><td>Heritage Area</td><td>Bulk Garbage Area</td><td>Income Neighbourhood</td></tr>";
			generalTabContent += "<tr><td><input type='text' value='" + heritageArea + "'></td><td><input type='text' value='" + bga + "'></td><td><input type='text' value='" + incNBHD + "'></td></tr>";
			generalTabContent += "<tr><td>Inspection Territory</td><td>City District</td><td>Revenue Tracking Area</td></tr>";
			generalTabContent += "<tr><td><input type='text' value='" + inspTerritory + "'></td><td><input type='text' value='" + cityDistrict + "'></td><td><input type='text' value='" + revTrackingArea + "'></td></tr>";
			generalTabContent += "</table>";
			
			var pdeTabContent = "<table width=100%>";
			pdeTabContent += "<tr><td colspan=2>Municipal Plan District</td><td colspan=2>Planning Area</td><td>Heritage Building ?</td></tr>";
			pdeTabContent += "<tr><td colspan=2><input type='text' value='" + munPlanDist + "'></td><td colspan=2><input type='text' value='" + planArea + "'></td><td><input type='checkbox'" + chkHeritageBldg + "></td></tr>";
			pdeTabContent += "<tr><td>Development Agreement</td><td>Archaeological Site</td><td>Waterway</td><td>Watershed</td><td>Wetland</td></tr>";
			pdeTabContent += "<tr><td><input type='text' value='" + devAgree + "'></td><td><input type='text' value='" + archaelogicalSite + "'></td><td><input type='text' value='" + waterway + "'></td><td><input type='text' value='" + watershed + "'></td><td><input type='text' value='" + wetland + "'></td></tr>";
			pdeTabContent += "<tr><td>Goulds Flood Plain</td><td>Flood Hazard Area</td><td>E.V.A (Section 11.3)</td><td>Battery Dev. Area</td><td>Light Planes</td></tr>";
			pdeTabContent += "<tr><td><input type='text' value='" + gouldsFloodPlain + "'></td><td><input type='text' value='" + floodHazArea + "'></td><td><input type='text' value='" + eva + "'></td><td><input type='text' value='" + batteryDevArea + "'></td><td><input type='text' value='" + lightPlanes + "'></td></tr>";
			pdeTabContent += "<tr><td>Landfill Protection Area</td><td>Landscaping Screening</td><td>Downtown Parking Area</td><td>Residential Parking Area</td><td>Churchill Square</td></tr>";
			pdeTabContent += "<tr><td><input type='text' value='" + landfillArea + "'></td><td><input type='text' value='" + landScreen + "'></td><td><input type='text' value='" + dtParkingArea + "'></td><td><input type='text' value='" + resParkingArea + "'></td><td><input type='text' value='" + churchillSquare + "'></td></tr>";
			pdeTabContent += "</table>";
			
			var propertyAreaContent = ""; //<div style='width:640px; height:480px;'>";
			propertyAreaContent += "<div data-dojo-type='dijit/layout/LayoutContainer' id='layoutContainer' style='width:640px; height: 480px;' class='governUI'>";
			propertyAreaContent += "<div data-dojo-type='dijit/layout/TabContainer' data-dojo-props=\"region:'top'\" style='width:100%; height:87%;'>";
			propertyAreaContent += "<div data-dojo-type='dijit/layout/ContentPane' title='General' data-dojo-props='selected:true'>";
			propertyAreaContent += generalTabContent;
			propertyAreaContent += "</div>";
			propertyAreaContent += "<div data-dojo-type='dijit/layout/ContentPane' title='PDE'>";
			propertyAreaContent += pdeTabContent;
			propertyAreaContent += "</div>";
			propertyAreaContent += "</div>";
			propertyAreaContent += "<div data-dojo-type='dijit/layout/ContentPane' data-dojo-props=\"region:'bottom'\" style='width:100%; height:8%;' class='recordNav'>";
			propertyAreaContent += "<span><input type='image' id='btnAreaInfoUIFirstRecord' src='governWidget/dijit/templates/images/FirstRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='image' id='btnAreaInfoUIPrevRecord' src='governWidget/dijit/templates/images/PreviousRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='text' value='" + this.propertyAreasCurrentIdx + " of " + this.numOfPropertyAreas + "' style='height:24px; width:400px; padding:0px; margin:0px; vertical-align:middle;'><input type='image' id='btnAreaInfoUINextRecord' src='governWidget/dijit/templates/images/NextRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='image' id='btnAreaInfoUILastRecord' src='governWidget/dijit/templates/images/LastRecord.png' style='height:24px; width:24px; vertical-align:middle;'></span>"
			propertyAreaContent += "</div>";
			propertyAreaContent += "</div>";
			return propertyAreaContent;
		},
		
		_HandleUIPropertyAreaButtons: function(){
			var btnNextRecord = dom.byId("btnAreaInfoUINextRecord");
			on(btnNextRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.propertyAreasCurrentIdx);
				if (this.propertyAreasCurrentIdx < this.numOfPropertyAreas) {
					this.propertyAreasCurrentIdx += 1;			
					propertyAreaDialog = this.propertyAreaUIdialog;
					propertyAreaContent = this._UIPropertyAreaContent();
					propertyAreaDialog.set("title", "Property Area - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
					propertyAreaDialog.set("content", propertyAreaContent);
					this._HandleUIPropertyAreaButtons();
				}
			}));
			var btnPrevRecord = dom.byId("btnAreaInfoUIPrevRecord");
			on(btnPrevRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.propertyAreasCurrentIdx);
				if (this.propertyAreasCurrentIdx > 1) {
					this.propertyAreasCurrentIdx -= 1;
					propertyAreaDialog = this.propertyAreaUIdialog;
					propertyAreaContent = this._UIPropertyAreaContent();
					propertyAreaDialog.set("title", "Property Area - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
					propertyAreaDialog.set("content", propertyAreaContent);
					this._HandleUIPropertyAreaButtons();
				}
			}));
			var btnFirstRecord = dom.byId("btnAreaInfoUIFirstRecord");
			on(btnFirstRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.propertyAreasCurrentIdx);
				if (this.propertyAreasCurrentIdx != 1) {
					this.propertyAreasCurrentIdx = 1;
					propertyAreaDialog = this.propertyAreaUIdialog;
					propertyAreaContent = this._UIPropertyAreaContent();
					propertyAreaDialog.set("title", "Property Area - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
					propertyAreaDialog.set("content", propertyAreaContent);
					this._HandleUIPropertyAreaButtons();
				}
			}));
			var btnLastRecord = dom.byId("btnAreaInfoUILastRecord");
			on(btnLastRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.propertyAreasCurrentIdx);
				if (this.propertyAreasCurrentIdx != this.numOfPropertyAreas) {
					this.propertyAreasCurrentIdx = this.numOfPropertyAreas;
					propertyAreaDialog = this.propertyAreaUIdialog;
					propertyAreaContent = this._UIPropertyAreaContent();
					propertyAreaDialog.set("title", "Property Area - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
					propertyAreaDialog.set("content", propertyAreaContent);
					this._HandleUIPropertyAreaButtons();
				}
			}));
		},
		
		_UIParcelInfoContent: function(){
			var parcelNode = this.sqlParcelResults;
			//var parcelDocNode = this.parcelResults;
			
			var taxMap = (!parcelNode.getElementsByTagName("TAX_MAP")[0]) ? "" : parcelNode.getElementsByTagName("TAX_MAP")[0].textContent; 
			var effectiveYear = (!parcelNode.getElementsByTagName("EFFECTIVE_YEAR")[0]) ? "" : parcelNode.getElementsByTagName("EFFECTIVE_YEAR")[0].textContent;
			var inactiveYear = (!parcelNode.getElementsByTagName("INACTIVE_YEAR")[0]) ? "" : parcelNode.getElementsByTagName("INACTIVE_YEAR")[0].textContent;
			var subdivisionName = (!parcelNode.getElementsByTagName("SUBD")[0]) ? "" : parcelNode.getElementsByTagName("SUBD")[0].textContent;
			var lotNum = (!parcelNode.getElementsByTagName("LOT")[0]) ? "" : parcelNode.getElementsByTagName("LOT")[0].textContent;
			var mapNum = (!parcelNode.getElementsByTagName("LEGAL")[0]) ? "" : parcelNode.getElementsByTagName("LEGAL")[0].textContent;
			var source = (!parcelNode.getElementsByTagName("SOURCE")[0]) ? "" : parcelNode.getElementsByTagName("SOURCE")[0].textContent;
			var status = (!parcelNode.getElementsByTagName("STATUS")[0]) ? "" : parcelNode.getElementsByTagName("STATUS")[0].textContent;
			var freeholdLeaseholdStatus = (!parcelNode.getElementsByTagName("FHSTATUS")[0]) ? "" : parcelNode.getElementsByTagName("FHSTATUS")[0].textContent;
			var nonParcelID = (!parcelNode.getElementsByTagName("NON_PARCEL_ID")[0]) ? "" : parcelNode.getElementsByTagName("NON_PARCEL_ID")[0].textContent;
			var nonParcelType = (!parcelNode.getElementsByTagName("NON_PARCEL_TYPE")[0]) ? "" : parcelNode.getElementsByTagName("NON_PARCEL_TYPE")[0].textContent;
			var nonParcelFlag = (!parcelNode.getElementsByTagName("NON_PARCEL_FLAG")[0]) ? "" : parcelNode.getElementsByTagName("NON_PARCEL_FLAG")[0].textContent;
			var chkNonParcelFlag = (nonParcelFlag == -1) ? " checked" : "";
			var approvedSubdivision = (!parcelNode.getElementsByTagName("APPROVED_SUBD")[0]) ? "" : parcelNode.getElementsByTagName("APPROVED_SUBD")[0].textContent;
			var chkApprovedSubdivision = (approvedSubdivision == -1) ? " checked" : "";
			var virtualParcel = (!parcelNode.getElementsByTagName("VIRTUAL_PARCEL")[0]) ? "" : parcelNode.getElementsByTagName("VIRTUAL_PARCEL")[0].textContent;
			var chkVirtualParcel = (virtualParcel == -1) ? " checked" : "";
			var occupancyCode = (!parcelNode.getElementsByTagName("OCCUPANCY_TYPE")[0]) ? "" : parcelNode.getElementsByTagName("OCCUPANCY_TYPE")[0].textContent;
			var parcelIndex = (!parcelNode.getElementsByTagName("LEGAL_INDEX")[0]) ? "" : parcelNode.getElementsByTagName("LEGAL_INDEX")[0].textContent;
			var x = (!parcelNode.getElementsByTagName("X")[0]) ? "" : parcelNode.getElementsByTagName("X")[0].textContent;
			var y = (!parcelNode.getElementsByTagName("Y")[0]) ? "" : parcelNode.getElementsByTagName("Y")[0].textContent;
			var xCoord = "";
			var yCoord = "";
			var maCheckout = (!parcelNode.getElementsByTagName("MA_CHECK_OUT")[0]) ? "" : parcelNode.getElementsByTagName("MA_CHECK_OUT")[0].textContent;
			console.log("maCheckout:", maCheckout);
			var chkMaCheckout = "";
			var inspCheckout = (!parcelNode.getElementsByTagName("INSP_CHECK_OUT")[0]) ? "" : parcelNode.getElementsByTagName("INSP_CHECK_OUT")[0].textContent;
			console.log("inspCheckout:", inspCheckout);
			var chkInspCheckout = "";
			var propReviewed2013Reassess = (!parcelNode.getElementsByTagName("REASSESSED")[0]) ? "" : parcelNode.getElementsByTagName("REASSESSED")[0].textContent;
			var chkPropReviewed2013Reassess = (propReviewed2013Reassess == -1) ? " checked" : "";
			var internalComments = (!parcelNode.getElementsByTagName("COMMENTS")[0]) ? "" : parcelNode.getElementsByTagName("COMMENTS")[0].textContent;
			var externalNotes = (!parcelNode.getElementsByTagName("NOTES")[0]) ? "" : parcelNode.getElementsByTagName("NOTES")[0].textContent;
			
			var generalTabContent = "<table width=100%>";
			generalTabContent += "<tr><td style='width:50px;'></td><td style='width:50px;'></td><td style='width:50px;'></td><td style='width:50px;'></td><td style='width:120px;'></td><td></td></tr>";
			generalTabContent += "<tr><td colspan=4>Tax Map Number / Roll #</td><td>Year Effective</td><td>Year Inactive</td></tr>";
			generalTabContent += "<tr><td colspan=4><input type='text' value='" + taxMap + "' style='width:250px;'></td><td><input type='text' value='" + effectiveYear + "' style='width:80px;'></td><td><input type='text' value='" + inactiveYear + "' style='width:80px;'></td></tr>";
			generalTabContent += "<tr><td colspan=4>Subdivision Name</td><td>Lot</td><td></td></tr>";
			generalTabContent += "<tr><td colspan=4><input type='text' value='" + subdivisionName + "' style='width:250px;'></td><td><input type='text' value='" + lotNum + "' style='width:60px;'></td><td></td></tr>";
			generalTabContent += "<tr><td>Map Number</td><td colspan=3>Source</td><td>Status</td><td>Freehold / Leasehold Status</td></tr>";
			generalTabContent += "<tr><td><input type='text' value='" + mapNum + "' style='width:100px;'></td><td colspan=3><input type='text' value='" + source + "' style='width:160px;'></td><td><input type='text' value='" + status + "' style='width:80px;'><td><input type='text' value='" + freeholdLeaseholdStatus + "' style='width:100px;'></td></tr>";
			generalTabContent += "<tr><td colspan=3></td><td colspan=2>Non-Parcel ID.</td><td>Non-Parcel Type</td>";
			generalTabContent += "<tr><td colspan=3><input type='checkbox'" + chkNonParcelFlag + ">Non-Parcel Flag</td><td colspan=2><input type='text' style='width:100px;' value='" + nonParcelID + "'></td><td><input type='text' value='" + nonParcelType + "'></td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='checkbox'" + chkApprovedSubdivision + ">Approved Subdivision</td><td></td><td></td><td></td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='checkbox'" + chkVirtualParcel + ">Virtual Parcel</td><td></td><td></td><td></td></tr>";
			generalTabContent += "<tr><td colspan=3>Occupancy Code</td><td colspan=2>Parcel Index</td><td></td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='text' value='" + occupancyCode + "'></td><td colspan=2><input type='text' style='width:100px;' value='" + parcelIndex + "'></td><td></td></tr>";
			generalTabContent += "<tr><td colspan=2>X</td><td colspan=2>Y</td><td colspan=2></td></tr>";
			generalTabContent += "<tr><td colspan=2><input type='text' style='width:100px;' value='" + x + "'></td><td colspan=2><input type='text' style='width:100px;' value='" + y + "'></td><td colspan=2></td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='checkbox'>MA Check Out</td><td></td><td colspan=2><input type='checkbox'" + chkPropReviewed2013Reassess + ">Prop. Reviewed-2013 ReAssessment</td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='checkbox'>Inspector Check Out</td><td colspan=3></td></tr>";
			generalTabContent += "</table>";
			
			var notesTabContent = "<table width=100%>";
			notesTabContent += "<tr><td>Internal Comments</td></tr>";
			notesTabContent += "<tr><td><textarea style='width:100%; height:210px; font-family: Arial;'>" + internalComments + "</textarea></td></tr>";
			notesTabContent += "<tr><td>External Notes</td></tr>";
			notesTabContent += "<tr><td><textarea style='width:100%; height:140px; font-family: Arial;'>" + externalNotes + "</textarea></td></tr>";
			notesTabContent += "</table>";
			
			var parcelInfoContent = ""; //<div style='width:640px; height:480px;'>";
			parcelInfoContent += "<div data-dojo-type='dijit/layout/TabContainer' style='width:640px; height:480px;' class='governUI'>";
			parcelInfoContent += "<div data-dojo-type='dijit/layout/ContentPane' title='General' data-dojo-props='selected:true'>";
			parcelInfoContent += generalTabContent;
			parcelInfoContent += "</div>";
			parcelInfoContent += "<div data-dojo-type='dijit/layout/ContentPane' title='Notes'>";
			parcelInfoContent += notesTabContent;
			parcelInfoContent += "</div>";
			parcelInfoContent += "</div>";
			return parcelInfoContent;
		},
		
		_UIOwnerInfoContent: function(){
			var ownerDocNode = this.ownerResults;
			console.log(ownerDocNode);
			var Owners = ownerDocNode.getElementsByTagName("DC_MSGovern_Owner");
			var OwnerNames = ownerDocNode.getElementsByTagName("DC_MSGovern_NameInfo");
			var numOfOwners = Owners.length;
			this.numOfOwners = numOfOwners;
			var owner;
			var ownerName;
			
			var DateEffective = "";
			var cboSource = "";
			var percentOwnership = "";
			var prioritySeq = "";
			var prioritySeqVisible = "block;";
			var ownerStatus = "";
			var rdoAltSource = "";
			var rdoCurrent = "";
			var rdoPrevious = "";
			var chkOccupant = "";
			var chkNoCorrespondece = "";
			var Notes = "";
			var NameInfo = "";
			var NameAddress = "";
			
			if (numOfOwners > 0) {
				owner = Owners[this.ownerCurrentIdx];
				ownerName = OwnerNames[this.ownerCurrentIdx];
				console.log(owner);
				DateEffective = owner.getElementsByTagName("AsOfDate")[0].textContent;
				cboSource = owner.getElementsByTagName("SourceInfoCode")[0].textContent;
				percentOwnership = owner.getElementsByTagName("OwnershipPercentage")[0].textContent;
				prioritySeq = owner.getElementsByTagName("PrioritySequence")[0].textContent;
				if (prioritySeq < 1) {
					prioritySeqVisible = "none;";
				}
				ownerStatus = owner.getElementsByTagName("Status")[0].textContent;
				if (ownerStatus==0) {
					rdoAltSource = " checked";
				} else if (ownerStatus=="Owner") {
					rdoCurrent = " checked";
				} else if (ownerStatus=="Prior") {
					rdoPrevious = " checked";
				}
				chkOccupant = owner.getElementsByTagName("OccupantFlag")[0].textContent;
				chkNoCorrespondece = owner.getElementsByTagName("NoCorrespondanceFlag")[0].textContent;
				Notes = owner.getElementsByTagName("Notes")[0].textContent;
				Notes = Notes.replace("'", "&#39");
				
				NameInfo = ownerName.getElementsByTagName("Company")[0].textContent;
				if (NameInfo.length >0) {
					NameAddress += "<option value='" + NameInfo + "'>" + NameInfo + "</option>";
				}
				NameInfo = ownerName.getElementsByTagName("FirstName")[0].textContent;
				if (NameInfo.length >0) {
					NameInfo = ownerName.getElementsByTagName("FirstName")[0].textContent + " " + ownerName.getElementsByTagName("MidInitial")[0].textContent + " " + ownerName.getElementsByTagName("LastName")[0].textContent;
					NameAddress += "<option value='" + NameInfo + "'>" + NameInfo + "</option>";
					NameInfo = ownerName.getElementsByTagName("Name2")[0].textContent;
					if (NameInfo.length >0) {
						NameAddress += "<option value='" + NameInfo + "'>" + NameInfo + "</option>";
					}
				}
				NameInfo = ownerName.getElementsByTagName("AddLineA")[0].textContent;
				if (NameInfo.length >0) {
					NameAddress += "<option value='" + NameInfo + "'>" + NameInfo + "</option>";
				}
				NameInfo = ownerName.getElementsByTagName("AddLineB")[0].textContent;
				if (NameInfo.length >0) {
					NameAddress += "<option value='" + NameInfo + "'>" + NameInfo + "</option>";
				}
				NameInfo = ownerName.getElementsByTagName("Street")[0].textContent;
				if (NameInfo.length >0) {
					NameInfo = ownerName.getElementsByTagName("Civic")[0].textContent + " " + ownerName.getElementsByTagName("Street")[0].textContent + " " + ownerName.getElementsByTagName("StreetSuffix")[0].textContent;
					NameAddress += "<option value='" + NameInfo + "'>" + NameInfo + "</option>";
				}
				NameInfo = ownerName.getElementsByTagName("City")[0].textContent + " " + ownerName.getElementsByTagName("State")[0].textContent + " " + ownerName.getElementsByTagName("ZipPostal")[0].textContent;
				NameInfo = NameInfo.replace("'", "&#39");
				NameAddress += "<option value='" + NameInfo + "'>" + NameInfo + "</option>";
			}
			
			var OwnerInfoContent = "<div style='width:640px; height:480px;' class='governUI'>";
			OwnerInfoContent += "<table width=100%>";
			OwnerInfoContent += "<tr><td style='width:100px;'></td><td style='width:150px;'></td><td style='width:250px;'></td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=2>Date Effective (As Of)</td><td rowspan=4><fieldset><legend>Owner Status</legend><input type='radio'" + rdoAltSource + ">Alternate Source<br><input type='radio'" + rdoCurrent + ">Current<br><input type='radio'" + rdoPrevious + ">Previous</fieldset></td><td></td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=2><input type='text' value='" + DateEffective + "'></td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=2>Source of Information</td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=2><select></select></td><td></td></tr>";
			OwnerInfoContent += "<tr><td>% Ownership</td><td><span style='display:"+ prioritySeqVisible + ";'>Priority Seq</td><td></td></tr>";
			OwnerInfoContent += "<tr><td><input type='text' style='width:80px;' value='" + percentOwnership + "'></td><td><span style='display:"+ prioritySeqVisible + ";'><input type='text' style='width:80px;' value='" + prioritySeq + "'></span></td><td><input type='checkbox'>Occupant</td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=2></td><td><input type='checkbox'>No Correspondence</td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=3>Notes</td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=3><textarea style='width:100%;height:100px;'>" + Notes + "</textarea></td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=3>Name & Address</td><td></td></tr>";
			OwnerInfoContent += "<tr><td colspan=3><select size=4>" + NameAddress + "</select></td><td></td></tr>";
			OwnerInfoContent += "</table>";
			OwnerInfoContent += "<span><input type='image' id='btnOwnerInfoUIFirstRecord' src='governWidget/dijit/templates/images/FirstRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='image' id='btnOwnerInfoUIPrevRecord' src='governWidget/dijit/templates/images/PreviousRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='text' value='" + (this.ownerCurrentIdx+1) + " of " + numOfOwners + "' style='height:24px; width:400px; padding:0px; margin:0px; vertical-align:middle;'><input type='image' id='btnOwnerInfoUINextRecord' src='governWidget/dijit/templates/images/NextRecord.png' style='height:24px; width:24px; vertical-align:middle;'><input type='image' id='btnOwnerInfoUILastRecord' src='governWidget/dijit/templates/images/LastRecord.png' style='height:24px; width:24px; vertical-align:middle;'></span>"
			OwnerInfoContent += "</div>";
			return OwnerInfoContent;
		},
		
		_HandleUIOwnerInfoButtons: function(){
			var btnNextRecord = dom.byId("btnOwnerInfoUINextRecord");
			on(btnNextRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.ownerCurrentIdx);
				if (this.ownerCurrentIdx+1 < this.numOfOwners) {
					this.ownerCurrentIdx += 1;			
					ownerInformationDialog = this.ownerInformationUIdialog;
					ownerInformationContent = this._UIOwnerInfoContent();
					ownerInformationDialog.set("title", "Owner Information");
					ownerInformationDialog.set("content", ownerInformationContent);
					this._HandleUIOwnerInfoButtons();
				}
			}));
			var btnPrevRecord = dom.byId("btnOwnerInfoUIPrevRecord");
			on(btnPrevRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.ownerCurrentIdx);
				if (this.ownerCurrentIdx > 0) {
					this.ownerCurrentIdx -= 1;
					ownerInformationDialog = this.ownerInformationUIdialog;
					ownerInformationContent = this._UIOwnerInfoContent();
					ownerInformationDialog.set("title", "Owner Information");
					ownerInformationDialog.set("content", ownerInformationContent);
					this._HandleUIOwnerInfoButtons();
				}
			}));
			var btnFirstRecord = dom.byId("btnOwnerInfoUIFirstRecord");
			on(btnFirstRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.ownerCurrentIdx);
				if (this.ownerCurrentIdx != 0) {
					this.ownerCurrentIdx = 0;
					ownerInformationDialog = this.ownerInformationUIdialog;
					ownerInformationContent = this._UIOwnerInfoContent();
					ownerInformationDialog.set("title", "Owner Information");
					ownerInformationDialog.set("content", ownerInformationContent);
					this._HandleUIOwnerInfoButtons();
				}
			}));
			var btnLastRecord = dom.byId("btnOwnerInfoUILastRecord");
			on(btnLastRecord, "click", lang.hitch(this, function(){
				console.log("Current Index", this.ownerCurrentIdx);
				if (this.ownerCurrentIdx != this.numOfOwners -1) {
					this.ownerCurrentIdx = this.numOfOwners -1;
					ownerInformationDialog = this.ownerInformationUIdialog;
					ownerInformationContent = this._UIOwnerInfoContent();
					ownerInformationDialog.set("title", "Owner Information");
					ownerInformationDialog.set("content", ownerInformationContent);
					this._HandleUIOwnerInfoButtons();
				}
			}));
		},
		
		_UILegalInfoContent: function(){
			var legalInfo = this.legalInfoResults;
			console.log(legalInfo);
			var lotSize1 = (!legalInfo.getElementsByTagName("SIZE_1")[0]) ? "" : legalInfo.getElementsByTagName("SIZE_1")[0].textContent;
			var size1Units = (!legalInfo.getElementsByTagName("SIZE_UNITS")[0]) ? "" : legalInfo.getElementsByTagName("SIZE_UNITS")[0].textContent;
			var lotSize2 = (!legalInfo.getElementsByTagName("SIZE_2")[0]) ? "" : legalInfo.getElementsByTagName("SIZE_2")[0].textContent;
			var size2Units = (!legalInfo.getElementsByTagName("SIZE_2_UNITS")[0]) ? "" : legalInfo.getElementsByTagName("SIZE_2_UNITS")[0].textContent;
			var totSize = (!legalInfo.getElementsByTagName("SIZE_TOTAL")[0]) ? "" : legalInfo.getElementsByTagName("SIZE_TOTAL")[0].textContent;
			var totSizeUnits = (!legalInfo.getElementsByTagName("SIZE_TOT_UNITS")[0]) ? "" : legalInfo.getElementsByTagName("SIZE_TOT_UNITS")[0].textContent;
			var front = (!legalInfo.getElementsByTagName("FRONT_SIZE")[0]) ? "" : legalInfo.getElementsByTagName("FRONT_SIZE")[0].textContent;
			var depth = (!legalInfo.getElementsByTagName("DEPTH_SIZE")[0]) ? "" : legalInfo.getElementsByTagName("DEPTH_SIZE")[0].textContent;
			var frontDepthUnits = (!legalInfo.getElementsByTagName("FRONT_DEPTH_UNITS")[0]) ? "" : legalInfo.getElementsByTagName("FRONT_DEPTH_UNITS")[0].textContent;
			var incQuestClass = (!legalInfo.getElementsByTagName("ClassDesc")[0]) ? "" : legalInfo.getElementsByTagName("ClassDesc")[0].textContent;
			var propClassGt2M = (!legalInfo.getElementsByTagName("Class2MDesc")[0]) ? "" : legalInfo.getElementsByTagName("Class2MDesc")[0].textContent;
			var propType = (!legalInfo.getElementsByTagName("PROPERTY_TYPE")[0]) ? "" : legalInfo.getElementsByTagName("PROPERTY_TYPE")[0].textContent;
			var specialPurpose = (!legalInfo.getElementsByTagName("SPCPUR")[0]) ? "" : legalInfo.getElementsByTagName("SPCPUR")[0].textContent;
			var chkSpecialPurpose = (specialPurpose==-1) ? " Checked" : "";
			var rollSection = (!legalInfo.getElementsByTagName("ROLL_SECTION")[0]) ? "" : legalInfo.getElementsByTagName("ROLL_SECTION")[0].textContent;
			var primLand = (!legalInfo.getElementsByTagName("PRIMARY_LAND")[0]) ? "" : legalInfo.getElementsByTagName("PRIMARY_LAND")[0].textContent;
			var bookNo = (!legalInfo.getElementsByTagName("BOOK_NO")[0]) ? "" : legalInfo.getElementsByTagName("BOOK_NO")[0].textContent;
			var GISarea = (!legalInfo.getElementsByTagName("GISAREA")[0]) ? "" : legalInfo.getElementsByTagName("GISAREA")[0].textContent;
			var GISdepth = (!legalInfo.getElementsByTagName("GISDEPTH")[0]) ? "" : legalInfo.getElementsByTagName("GISDEPTH")[0].textContent;
			var GISfrontage = (!legalInfo.getElementsByTagName("GISFRONT")[0]) ? "" : legalInfo.getElementsByTagName("GISFRONT")[0].textContent;
			var legalDescription = (!legalInfo.getElementsByTagName("LEGAL")[0]) ? "" : legalInfo.getElementsByTagName("LEGAL")[0].textContent;
			
			var generalTabContent = "<table>";
			generalTabContent += "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
			generalTabContent += "<tr><td>Lot Size</td><td colspan=2>Unit</td><td colspan=2>Lot Size</td><td>Unit</td></tr>";
			generalTabContent += "<tr><td><input type='text' style='width:100px;' value='" + lotSize1 + "'></td><td colspan=2><input type='text' style='width:120px;' value='" + size1Units + "'></td><td colspan=2><input type='text' style='width:100px;' value='" + lotSize2 + "'></td><td><input type='text' style='width:100px;' value='" + size2Units + "'></td></tr>";
			generalTabContent += "<tr><td>Total Size</td><td colspan=2>Unit</td><td>Front</td><td>Depth</td><td>Unit</td></tr>";
			generalTabContent += "<tr><td><input type='text' style='width:100px;' value='" + totSize + "'></td><td colspan=2><input type='text' style='width:120px;' value='" + totSizeUnits + "'></td><td><input type='text' style='width:50px;' value='" + front + "'></td><td><input type='text' style='width:50px;' value='" + depth + "'></td><td><input type='text' style='width:100px;' value='" + frontDepthUnits + "'></td></tr>";
			generalTabContent += "<tr><td colspan=3>Income Questionnaire Classification</td><td colspan=3>Prop Classification Assmt > 2M</td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='text' style='width:280px;' value='" + incQuestClass + "'></td><td colspan=3><input type='text' style='width:280px;' value='" + propClassGt2M + "'></td></tr>";
			generalTabContent += "<tr><td colspan=3>Property Type</td><td colspan=3 rowspan=2>Special Purpose Property <input type='checkbox'" + chkSpecialPurpose + "></td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='text' style='width:280px;' value='" + propType + "'></td></tr>";
			generalTabContent += "<tr><td colspan=3>Roll Section (future use)</td><td colspan=3>Primary Land Use (future use)</td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='text' style='width:280px;' value='" + rollSection + "'></td><td colspan=3><input type='text' style='width:280px;' value='" + primLand + "'></td></tr>";
			generalTabContent += "<tr><td colspan=3>Book Number (future use)</td><td colspan=3></td></tr>";
			generalTabContent += "<tr><td colspan=3><input type='text' style='width:280px;' value='" + bookNo + "'></td><td colspan=3></td></tr>";
			generalTabContent += "<tr><td>GIS Area</td><td>GIS Depth</td><td>GIS Frontage</td><td></td><td></td><td></td></tr>";
			generalTabContent += "<tr><td><input type='text' style='width:80px;' value='" + GISarea + "'></td><td><input type='text' style='width:80px;' value='" + GISdepth + "'></td><td><input type='text' style='width:80px;' value='" + GISfrontage + "'></td><td></td><td></td><td></td></tr>";
			generalTabContent += "<tr><td colspan=6>Legal Description (future use)</td></tr>";
			generalTabContent += "<tr><td colspan=6><textarea style='width:600px;height:45px;'>" + legalDescription + "</textarea></td></tr>";
			generalTabContent += "</table>";
			
			var legalInfoContent = "<div data-dojo-type='dijit/layout/TabContainer' style='width:640px; height:480px;' class='governUI'>";
			legalInfoContent += "<div data-dojo-type='dijit/layout/ContentPane' title='General' data-dojo-props='selected:true'>";
			legalInfoContent += generalTabContent;
			legalInfoContent += "</div>";
			legalInfoContent += "</div>";
			return legalInfoContent;
		},
		
		_UIMultimediaContent: function(){
			var deedList = this.deedListResults;
			
			var deedTabContent = "<div style='width:100%; height:350px; background-color:gainsboro'>";
			deedTabContent += "<table id='deedsTable' class='multimediaTable'>";
			if (deedList){
				deedTabContent += "<tr><th style='width:20%;'>Document Number</th><th style='width:20%;'>Entry Date</th><th style='width:60%;'>Notes</th></tr>";
				var deeds = deedList.getElementsByTagName("Deeds");
				var numOfDeeds = deeds.length;
				for (var i = 0; i < numOfDeeds; i++) {
					console.log(deeds[i]);
					var docNum = (!deeds[i].getElementsByTagName("DOC_NUM")[0]) ? "" : deeds[i].getElementsByTagName("DOC_NUM")[0].textContent;
					var notes = (!deeds[i].getElementsByTagName("NOTES")[0]) ? "" : deeds[i].getElementsByTagName("NOTES")[0].textContent;
					var entryDate = (!deeds[i].getElementsByTagName("ENTRY_DATE")[0]) ? "" : new Date(deeds[i].getElementsByTagName("ENTRY_DATE")[0].textContent);
					var yyyy = entryDate.getFullYear();
					var mm = "" + entryDate.getMonth();
					if (mm.length < 2) {
						mm = "0" + mm;
					}
					var dd = "" + entryDate.getDate();
					if (dd.length < 2) {
						dd = "0" + dd;
					}
					entryDate = yyyy + "-" + mm + "-" + dd;
					deedTabContent += "<tr><td>" + docNum + "</td><td>" + entryDate + "</td><td>" + notes + "</td></tr>";
				}	
			}
			deedTabContent += "</table>";
			deedTabContent += "</div>";
			deedTabContent += '<form action="' + this.governWidgetService + '/GetMultimediaFile" method="get" target="_blank">';
			//deedTabContent += '<form action="http://lap229/widgetService/widgetService.asmx/GetMultimediaFile" method="get" target="_blank">'; //target="deedFile">';
			deedTabContent += '<input type="text" id="documentLocator" name="documentLocator" style="display:none;" value="">';
			deedTabContent += '<input type="submit" id="btnGetDeed" value="View Deed" disabled>';
			deedTabContent += '</form>';
			//deedTabContent += '<iframe name="deedFile" height=5 width=5></iframe>';
			
			
			var applicationsTabContent = "<table width=100%>";
			applicationsTabContent += "<tr><td>Applications/Permits</td></tr>";
			applicationsTabContent += "<tr><td><div style='width:100%; height:180px; background-color:gainsboro; overflow-y:scroll'>";
			
			var applicationList = this.applicationListResults;
			applicationsTabContent += "<table id='applicationsTable' class='multimediaTable'>";
			if (applicationList) {
				applicationsTabContent += "<tr><th style='display:none;'>PM_ID</th><th>Application Number</th><th>Application Date</th><th>Type</th><th>Description</th><th>Void</th><th>Application Status</th><th>Dev Staff</th><th>LAST_MODIF_UID</th><th>AS400_NUM</th><th>FILE_NUM</th><th>FILE_ADD</th><th>FILE_NAME</th><th>FILE_DESC</th><th>FILE_NOTES</th></tr>";
				var applications = applicationList.getElementsByTagName("Applications");
				var numOfApplications = applications.length;
				for (var i = 0; i < numOfApplications; i++) {
					//console.log(applications[i]);
					var pmID = (!applications[i].getElementsByTagName("PM_ID")[0]) ? "" : applications[i].getElementsByTagName("PM_ID")[0].textContent;
					var appNum = (!applications[i].getElementsByTagName("PM_APPLICATION")[0]) ? "" : applications[i].getElementsByTagName("PM_APPLICATION")[0].textContent;
					var appDate = (!applications[i].getElementsByTagName("APPLICATION_DATE")[0]) ? "" : applications[i].getElementsByTagName("APPLICATION_DATE")[0].textContent;
					var appType = (!applications[i].getElementsByTagName("PMTYPE")[0]) ? "" : applications[i].getElementsByTagName("PMTYPE")[0].textContent;
					var appDesc = (!applications[i].getElementsByTagName("Description")[0]) ? "" : applications[i].getElementsByTagName("Description")[0].textContent;
					var appVoid = (!applications[i].getElementsByTagName("VOID")[0]) ? "" : applications[i].getElementsByTagName("VOID")[0].textContent;
					var appStatus = (!applications[i].getElementsByTagName("PMSTATUS")[0]) ? "" : applications[i].getElementsByTagName("PMSTATUS")[0].textContent;
					var appDevOff = (!applications[i].getElementsByTagName("DEV_OFF")[0]) ? "" : applications[i].getElementsByTagName("DEV_OFF")[0].textContent;
					var appLastUID = (!applications[i].getElementsByTagName("LAST_MODIF_UID")[0]) ? "" : applications[i].getElementsByTagName("LAST_MODIF_UID")[0].textContent;
					var appAS400 = (!applications[i].getElementsByTagName("AS400_NUM")[0]) ? "" : applications[i].getElementsByTagName("AS400_NUM")[0].textContent;
					var appFileNum = (!applications[i].getElementsByTagName("FILE_NUM")[0]) ? "" : applications[i].getElementsByTagName("FILE_NUM")[0].textContent;
					var appFileAdd = (!applications[i].getElementsByTagName("FILE_ADD")[0]) ? "" : applications[i].getElementsByTagName("FILE_ADD")[0].textContent;
					var appFileName = (!applications[i].getElementsByTagName("FILE_NAME")[0]) ? "" : applications[i].getElementsByTagName("FILE_NAME")[0].textContent;
					var appFileDesc = (!applications[i].getElementsByTagName("FILE_DESC")[0]) ? "" : applications[i].getElementsByTagName("FILE_DESC")[0].textContent;
					var appFileNotes = (!applications[i].getElementsByTagName("FILE_NOTES")[0]) ? "" : applications[i].getElementsByTagName("FILE_NOTES")[0].textContent;
					console.log(appNum);
					applicationsTabContent += "<tr><td style='display:none;'>" + pmID + "</td><td>" + appNum + "</td><td><div class='longtext'>" + appDate + "<span class='longdesc'>" + appDate + "</span></div></td><td><div class='longtext'>" + appType + "<span class='longdesc'>" + appType + "</span></div></td><td><div style='width:250px;'>" + appDesc + "</div></td><td>" + appVoid + "</td><td>" + appStatus + "</td><td>" + appDevOff + "</td><td>" + appLastUID + "</td><td>" + appAS400 + "</td><td>" + appFileNum + "</td><td>" + appFileAdd + "</td><td>" + appFileName + "</td><td>" + appFileDesc + "</td><td>" + appFileNotes + "</td></tr>";
				}
			}
			applicationsTabContent += "</table>";
			
			applicationsTabContent += "</div></td></tr>";
			applicationsTabContent += "<tr><td><fieldset><legend>Attached Documents</legend><select id='attachmentList' style='width:200px;margin-right:15px;'></select>";
			applicationsTabContent += '<form action="' + this.governWidgetService + '/GetMultimediaFile" method="get" target="_blank" style="display:inline;">';
			//applicationsTabContent += '<form action="http://lap229/widgetService/widgetService.asmx/GetMultimediaFile" method="get" target="_blank" style="display:inline;">';
			applicationsTabContent += '<input type="text" id="PMdocumentLocator" name="documentLocator" style="display:none;" value="">';
			applicationsTabContent += '<input type="submit" id="btnGetPMDocument" value="View" disabled>';
			applicationsTabContent += '</form><p>Attachment Description:</p><textarea id="attachmentDescription" style="width:300px;height:50px;"></textarea></fieldset></td></tr>';
			//applicationsTabContent += '</form><iframe name="attachedFile" height=5 width=5></iframe><p>Attachment Description:</p><textarea id="attachmentDescription" style="width:300px;height:50px;"></textarea></fieldset></td></tr>';
			//applicationsTabContent += "<tr><td>Workflow status of selected Application/Permit</td></tr>";
			//applicationsTabContent += "<tr><td><div style='width:100%; height:150px; background-color:gainsboro'></div></td></tr>";
			applicationsTabContent += "</table>";
			
			var multimediaContent = "<div data-dojo-type='dijit/layout/TabContainer' style='width:640px; height:480px;' class='governUI'>";
			multimediaContent += "<div data-dojo-type='dijit/layout/ContentPane' title='Deeds' data-dojo-props='selected:true'>";
			multimediaContent += deedTabContent;
			multimediaContent += "</div>";
			multimediaContent += "<div data-dojo-type='dijit/layout/ContentPane' title='PDE Applications'>";
			multimediaContent += applicationsTabContent;
			multimediaContent += "</div>";
			multimediaContent += "</div>";
			return multimediaContent;
		},
		
		_HandleUIMultimediaButtons: function(){
			var deedsTable = dom.byId("deedsTable");
			on(deedsTable, "click", lang.hitch(this, function(evt){
				var target = evt.target ? evt.target : evt.srcElement;
				//console.log(evt);
				if (target.nodeName.toLowerCase() == 'td') {
					var clickedRow = target.parentElement;
					var allTableRows = clickedRow.parentElement.children;
					for (i in allTableRows) {
						allTableRows[i].className = '';
					}
					//this.selectedDeed = {docLocator:clickedRow.children[0].textContent, entryDate:clickedRow.children[1].textContent};
					clickedRow.className = 'selected';
					
					var getDeedButton = dom.byId("btnGetDeed");
					var inputDocLocator = dom.byId("documentLocator");
					
					inputDocLocator.value = clickedRow.children[0].textContent;
					getDeedButton.disabled = false;
					//console.log(getDeedButton);
				}
				
			}));
			var applicationsTable = dom.byId("applicationsTable");
			on(applicationsTable, "click", lang.hitch(this, function(evt){
				var target = evt.target ? evt.target : evt.srcElement;
				if (target.nodeName.toLowerCase() == 'td') {
					var clickedRow = target.parentElement;
					var allTableRows = clickedRow.parentElement.children;
					for (i in allTableRows) {
						allTableRows[i].className = '';
					}
					//this.selectedDeed = {docLocator:clickedRow.children[0].textContent, entryDate:clickedRow.children[1].textContent};
					clickedRow.className = 'selected';
					var pmID = clickedRow.children[0].textContent;
					console.log("PM_ID:", pmID);
					var getAttachmentsComplete = this._GetAttachmentList(pmID);
					
					getAttachmentsComplete.then(lang.hitch(this, function(attachmentsText){
						var attachmentsDocNode = attachmentsText.documentElement;
						this.attachedDocs = attachmentsDocNode;
										
						console.log("The server returned: ", attachmentsText);
						var attachmentListBox = dom.byId("attachmentList");
						while (attachmentListBox.options.length) {
							attachmentListBox.remove(0);
						}
						var attachmentListDoc = attachmentsText.getElementsByTagName("DocumentElement")[0];
						if (attachmentListDoc) {
							var attachments = attachmentListDoc.getElementsByTagName("Attachments");
							var numOfAttachments = attachments.length;
							for (var i = 0; i < numOfAttachments; i++) {
								var docType = (!attachments[i].getElementsByTagName("DOC_TYPE")[0]) ? "" : attachments[i].getElementsByTagName("DOC_TYPE")[0].textContent;
								var attachmentItem = new Option(docType, i);
								attachmentListBox.options.add(attachmentItem);
							}
							attachmentListBox.selectedIndex=0;
							// fire the onchange for the attachmentList dropdown.
							 var evObj = document.createEvent("HTMLEvents");
							 evObj.initEvent("change", true, true);
							 attachmentListBox.dispatchEvent(evObj);
						}
					}));
					
					//var getDeedButton = dom.byId("btnGetDeed");
					//var inputDocLocator = dom.byId("documentLocator");
					
					//inputDocLocator.value = clickedRow.children[0].textContent;
					//getDeedButton.disabled = false;
					//console.log(getDeedButton);
				}
				
			}));
			var attachmentListBox = dom.byId("attachmentList");
			on(attachmentListBox, "change", lang.hitch(this, function(evt){
				var attachments = this.attachedDocs.getElementsByTagName("Attachments");
				//console.log("Changed");
				//console.log("SelectedIndex:", attachmentListBox.selectedIndex);
				//console.dir(attachments);
				//console.log("Attachments Length: ", this.attachments.length);
				//console.log(this.attachedDocs[attachmentListBox.selectedIndex]);
				var selectedAttachment = attachments[attachmentListBox.selectedIndex];
				var descriptionField = selectedAttachment.getElementsByTagName("FREE_TEXT")[0];
				var description = (!descriptionField) ? "" : (descriptionField.textContent).substr(0);
				var txtAttachmentDescription = dom.byId("attachmentDescription");
				var getPMDocumentButton = dom.byId("btnGetPMDocument");
				getPMDocumentButton.disabled = true
				txtAttachmentDescription.value = description;
					
				if (selectedAttachment.getElementsByTagName("DOCUMENT_LOCATOR")[0]) {
					var docLoc = selectedAttachment.getElementsByTagName("DOCUMENT_LOCATOR")[0].textContent;
					
					//description = description.replace('"', '');
					//console.log(description);
					
					var inputPMDocLocator = dom.byId("PMdocumentLocator");
					
					inputPMDocLocator.value = docLoc;
					getPMDocumentButton.disabled = false;
				}
				
			}));
		},
		
		_UIAssessmentInfoContent: function(){
			var assessment = this.assessmentResults;
			//console.log(assessment);
			var LevyGroup = (!assessment.getElementsByTagName("Levy_Group")[0]) ? "" : assessment.getElementsByTagName("Levy_Group")[0].textContent;
			var NotesComments = (!assessment.getElementsByTagName("Notes_Comments")[0]) ? "" : assessment.getElementsByTagName("Notes_Comments")[0].textContent;
			var AssessorInitials = (!assessment.getElementsByTagName("Assor_Initl")[0]) ? "" : assessment.getElementsByTagName("Assor_Initl")[0].textContent;
			var ChangeReason = (!assessment.getElementsByTagName("Change_Reason")[0]) ? "" : assessment.getElementsByTagName("Change_Reason")[0].textContent;
			var EffDate = (!assessment.getElementsByTagName("EFFECTIVE_DATE")[0]) ? "" : assessment.getElementsByTagName("EFFECTIVE_DATE")[0].textContent;
			var ResRealty = (!assessment.getElementsByTagName("Res_Realty")[0]) ? "" : assessment.getElementsByTagName("Res_Realty")[0].textContent;
			var ResVacLand = (!assessment.getElementsByTagName("Res_Vac")[0]) ? "" : assessment.getElementsByTagName("Res_Vac")[0].textContent;
			var CommRealty = (!assessment.getElementsByTagName("Com_Realty")[0]) ? "" : assessment.getElementsByTagName("Com_Realty")[0].textContent;
			var CommVacLand = (!assessment.getElementsByTagName("Com_Vac")[0]) ? "" : assessment.getElementsByTagName("Com_Vac")[0].textContent;
			var TaxExempt = (!assessment.getElementsByTagName("Tax_Exmpt")[0]) ? "" : assessment.getElementsByTagName("Tax_Exmpt")[0].textContent;
			var TotAssessment = (!assessment.getElementsByTagName("Tot_Assessment")[0]) ? "" : assessment.getElementsByTagName("Tot_Assessment")[0].textContent;
			var ResWtrUnits = (!assessment.getElementsByTagName("Res_Wtr_Units")[0]) ? "" : assessment.getElementsByTagName("Res_Wtr_Units")[0].textContent;
			var CommWtrUnits = (!assessment.getElementsByTagName("Com_Wtr_Units")[0]) ? "" : assessment.getElementsByTagName("Com_Wtr_Units")[0].textContent;
			var ResExemptWtrUnits = (!assessment.getElementsByTagName("Exmpt_Res_Wtr_Units")[0]) ? "" : assessment.getElementsByTagName("Exmpt_Res_Wtr_Units")[0].textContent;
			var CommExemptWtrUnits = (!assessment.getElementsByTagName("Exmpt_Com_Wtr_Units")[0]) ? "" : assessment.getElementsByTagName("Exmpt_Com_Wtr_Units")[0].textContent;
			var ResTotWtrUnits = (!assessment.getElementsByTagName("Tot_Res_Wtr_Units")[0]) ? "" : assessment.getElementsByTagName("Tot_Res_Wtr_Units")[0].textContent;
			var CommTotWtrUnits = (!assessment.getElementsByTagName("Tot_Com_Wtr_Units")[0]) ? "" : assessment.getElementsByTagName("Tot_Com_Wtr_Units")[0].textContent;
			var WtrType = (!assessment.getElementsByTagName("Water_Type")[0]) ? "" : assessment.getElementsByTagName("Water_Type")[0].textContent;
			var TotLeaseArea = (!assessment.getElementsByTagName("Lease_Area")[0]) ? "" : assessment.getElementsByTagName("Lease_Area")[0].textContent;
			var AllowClaim = (!assessment.getElementsByTagName("Allow_Claim")[0]) ? "" : assessment.getElementsByTagName("Allow_Claim")[0].textContent;
			
			var assessmentTabContent = "<table>";
			assessmentTabContent += "<tr><td style='width:160px;'></td><td style='width:80px;'></td><td style='width:160px;'></td><td style='width:100px;'></td><td></td></tr>";
			assessmentTabContent += "<tr><td>Levy Group</td><td></td><td colspan=3>Notes & Comments</td></tr>";
			assessmentTabContent += "<tr><td colspan=2><input type='text' style='width:200px;' value='" + LevyGroup + "'></td><td colspan=3 rowspan=3><textarea style='width:320px;height:60px;'>" + NotesComments + "</textarea></td></tr>";
			assessmentTabContent += "<tr><td colspan=2><input type='checkbox' disabled>Subject to Tax Deferral</td></tr>";
			assessmentTabContent += "<tr><td colspan=2><input type='checkbox' disabled>Not Subject to Partial Billing</td></tr>";
			assessmentTabContent += "<tr><td colspan=2>Assessor Initials</td><td colspan=3>Value Change Reason <input type='text' style='width:180px;' value='" + ChangeReason + "'></td></tr>";
			assessmentTabContent += "<tr><td colspan=2><input type='text' style='width:80px;' value='" + AssessorInitials + "'></td><td colspan=3>Effective Date <input type='text' style='width:100px;' value='" + EffDate + "'></td></tr>";
			assessmentTabContent += "<tr><td></td><td>Assessed Values</td><td></td><td>RESIDENTIAL</td><td>COMMERCIAL</td></tr>";
			assessmentTabContent += "<tr><td>Res. Realty</td><td><input type='text' style='width:80px;' value='" + ResRealty + "'></td><td>Water Units</td><td><input type='text' style='width:80px;' value='" + ResWtrUnits + "'></td><td><input type='text' style='width:80px;' value='" + CommWtrUnits + "'></td></tr>";
			assessmentTabContent += "<tr><td>Res. Vacant Land</td><td><input type='text' style='width:80px;' value='" + ResVacLand + "'></td><td>Exempt Water Units</td><td><input type='text' style='width:80px;' value='" + ResExemptWtrUnits + "'></td><td><input type='text' style='width:80px;' value='" + CommExemptWtrUnits + "'></td></tr>";
			assessmentTabContent += "<tr><td>Comm. Realty</td><td><input type='text' style='width:80px;' value='" + CommRealty + "'></td><td>Water Units Billed</td><td><input type='text' style='width:80px;' value='" + ResTotWtrUnits + "'></td><td><input type='text' style='width:80px;' value='" + CommTotWtrUnits + "'></td></tr>";
			assessmentTabContent += "<tr><td>Comm. Vacant Land</td><td><input type='text' style='width:80px;' value='" + CommVacLand + "'></td><td>Water Type</td><td colspan=2><input type='text' style='width:120px;' value='" + WtrType + "'></td></tr>";
			assessmentTabContent += "<tr><td>Tax Exempt</td><td><input type='text' style='width:80px;' value='" + TaxExempt + "'></td><td colspan=3></td></tr>";
			assessmentTabContent += "<tr><td>Total Assessment</td><td><input type='text' style='width:80px;' value='" + TotAssessment + "'></td><td colspan=3></td></tr>";
			assessmentTabContent += "<tr><td>Total Leasable Area (SM)</td><td><input type='text' style='width:80px;' value='" + TotLeaseArea + "'></td><td>Allowance Claim (Manual)</td><td colspan=2><input type='text' style='width:180px;' value='" + AllowClaim + "'></td></tr>";
			assessmentTabContent += "</table>";
			
			var ConsTaxRate = (!assessment.getElementsByTagName("Cons_Tax_Rate")[0]) ? "" : assessment.getElementsByTagName("Cons_Tax_Rate")[0].textContent;
			var AnnualTaxes = (!assessment.getElementsByTagName("Annual_Taxes")[0]) ? "" : assessment.getElementsByTagName("Annual_Taxes")[0].textContent;
			var TotNonExmpt = (!assessment.getElementsByTagName("Tot_Non_Exmpt")[0]) ? "" : assessment.getElementsByTagName("Tot_Non_Exmpt")[0].textContent;
			var TotAnnualNetTaxes = (!assessment.getElementsByTagName("Tot_Annual_Net_Taxes")[0]) ? "" : assessment.getElementsByTagName("Tot_Annual_Net_Taxes")[0].textContent;
			var EstTax = (!assessment.getElementsByTagName("Est_Tax")[0]) ? "" : assessment.getElementsByTagName("Est_Tax")[0].textContent;
			var EstTaxCom = (!assessment.getElementsByTagName("Est_Tax_Comm")[0]) ? "" : assessment.getElementsByTagName("Est_Tax_Comm")[0].textContent;
			var LastYrAssessment = (!assessment.getElementsByTagName("Last_Yr_Assessment")[0]) ? "" : assessment.getElementsByTagName("Last_Yr_Assessment")[0].textContent;
			var LastYrTaxes = (!assessment.getElementsByTagName("Last_Yr_Taxes")[0]) ? "" : assessment.getElementsByTagName("Last_Yr_Taxes")[0].textContent;
			var Residential = (!assessment.getElementsByTagName("VA_RESIDENTIAL")[0]) ? "" : assessment.getElementsByTagName("VA_RESIDENTIAL")[0].textContent;
			var Commercial = (!assessment.getElementsByTagName("VA_COMMERCIAL")[0]) ? "" : assessment.getElementsByTagName("VA_COMMERCIAL")[0].textContent;
			var Industrial = (!assessment.getElementsByTagName("VA_INDUSTRIAL")[0]) ? "" : assessment.getElementsByTagName("VA_INDUSTRIAL")[0].textContent;
			var OpenSpace = (!assessment.getElementsByTagName("VA_OPEN_SPACE")[0]) ? "" : assessment.getElementsByTagName("VA_OPEN_SPACE")[0].textContent;
			var Exempt = (!assessment.getElementsByTagName("VA_EXEMPT")[0]) ? "" : assessment.getElementsByTagName("VA_EXEMPT")[0].textContent;
			
			var additionalInfoTabContent = "<table>";
			additionalInfoTabContent += "<tr><td>";
			additionalInfoTabContent += "<fieldset><legend>Annual Tax Summary Information</legend>";
			additionalInfoTabContent += "<table>";
			additionalInfoTabContent += "<tr><td>Consolidated Tax Rate</td><td><input type='text' style='width:100px;' value='" + ConsTaxRate + "'></td></tr>";
			additionalInfoTabContent += "<tr><td>Annual Taxes</td><td><input type='text' style='width:100px;' value='" + AnnualTaxes + "'></td></tr>";
			additionalInfoTabContent += "<tr><td>Total Non-Exempt</td><td><input type='text' style='width:100px;' value='" + TotNonExmpt + "'></td></tr>";
			additionalInfoTabContent += "<tr><td>Annual Exemptions</td><td><input type='text' style='width:100px;' value=''></td></tr>";
			additionalInfoTabContent += "<tr><td>Total Annual Net Taxes</td><td><input type='text' style='width:100px;' value='" + TotAnnualNetTaxes + "'></td></tr>";
			additionalInfoTabContent += "<tr><td>Estimated Taxes</td><td><input type='text' style='width:100px;' value='" + EstTax + "'></td></tr>";
			additionalInfoTabContent += "<tr><td>Estimated Taxes Comm.</td><td><input type='text' style='width:100px;' value='" + EstTaxCom + "'></td></tr>";
			additionalInfoTabContent += "</table>";
			additionalInfoTabContent += "</fieldset>";
			additionalInfoTabContent += "</td><td>";
			additionalInfoTabContent += "<fieldset><legend>Prior Year Annual Information</legend>";
			additionalInfoTabContent += "<table>";
			additionalInfoTabContent += "<tr><td>Total Assessment</td><td><input type='text' style='width:100px;' value='" + LastYrAssessment + "'></td></tr>";
			additionalInfoTabContent += "<tr><td>Last Year Taxes</td><td><input type='text' style='width:100px;' value='" + LastYrTaxes + "'></td></tr>";
			additionalInfoTabContent += "</table>";
			additionalInfoTabContent += "</fieldset>";
			additionalInfoTabContent += "</td></tr>";
			additionalInfoTabContent += "</table>";
			additionalInfoTabContent += "<fieldset><legend>Mixed Use</legend>";
			additionalInfoTabContent += "<table>";
			additionalInfoTabContent += "<tr><td>Residential</td><td>Commercial</td><td>Industrial</td><td>Open Space</td><td>Exempt</td></tr>";
			additionalInfoTabContent += "<tr><td><input type='text' style='width:100px;' value='" + Residential + "'></td><td><input type='text' style='width:100px;' value='" + Commercial + "'></td><td><input type='text' style='width:100px;' value='" + Industrial + "'></td><td><input type='text' style='width:100px;' value='" + OpenSpace + "'></td><td><input type='text' style='width:100px;' value='" + Exempt + "'></td></tr>";
			additionalInfoTabContent += "</table>";
			additionalInfoTabContent += "</fieldset>";
			
			var exemptionGroupTabContent = "<table>";
			exemptionGroupTabContent += "</table>";
			
			var assessmentInfoContent = "<div style='width:640px; height:480px;' class='governUI'>";
			assessmentInfoContent += "<div style='width:640px; height:30px;'>";
			assessmentInfoContent += "Tax Year: ";
			assessmentInfoContent += "<select id='taxYear' style='width:100px;'>";
			assessmentInfoContent += "<option value=" + this.assessmentYear + ">" + this.assessmentYear + "</option>";
			assessmentInfoContent += "<option value=2017>2017</option>";
			assessmentInfoContent += "<option value=2016>2016</option>";
			assessmentInfoContent += "<option value=2015>2015</option>";
			assessmentInfoContent += "<option value=2014>2014</option>";
			assessmentInfoContent += "</select>";
			assessmentInfoContent += "</div>";
			assessmentInfoContent += "<div data-dojo-type='dijit/layout/TabContainer' style='width:640px; height:450px;'>";
			assessmentInfoContent += "<div data-dojo-type='dijit/layout/ContentPane' title='Assessment' data-dojo-props='selected:true'>";
			assessmentInfoContent += assessmentTabContent;
			assessmentInfoContent += "</div>";
			assessmentInfoContent += "<div data-dojo-type='dijit/layout/ContentPane' title='Add. Info'>";
			assessmentInfoContent += additionalInfoTabContent;
			assessmentInfoContent += "</div>";
			assessmentInfoContent += "<div data-dojo-type='dijit/layout/ContentPane' title='Exemption Group'>";
			assessmentInfoContent += exemptionGroupTabContent;
			assessmentInfoContent += "</div>";
			assessmentInfoContent += "</div>";
			assessmentInfoContent += "</div>";
			return assessmentInfoContent;
		},
		
		_HandleUIAssessmentInfoButtons: function(){
			var taxYearListBox = dom.byId("taxYear");
			on(taxYearListBox, "change", lang.hitch(this, function(evt){
				this.assessmentYear = taxYearListBox.item(taxYearListBox.selectedIndex).text;
				console.log(this.assessmentYear);
				var getAssessmentComplete = this._GetAssessment(this.parcelPIDs[this.parcelCurrentIdx], this.assessmentYear);
					
				getAssessmentComplete.then(lang.hitch(this, function(assessmentText){
					console.log("The server returned: ", assessmentText);
					this.assessmentResults = assessmentText.getElementsByTagName("DocumentElement")[0];
					var assessmentInfoContent = this._UIAssessmentInfoContent();
					var assessmentInfoDialog = this.assessmentUIdialog;
					
					assessmentInfoDialog.set("title", "Property Assessment - PID: " + this.parcelPIDs[this.parcelCurrentIdx]);
					assessmentInfoDialog.set("content", assessmentInfoContent);
					this._HandleUIAssessmentInfoButtons();
				}));
				
			}));
		},
		
    });
    //if (has("extend-esri")) {
    //    lang.setObject("dijit.HomeButton", Widget, esriNS);
    //}
    return Widget;
});