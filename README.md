# Note about Angularjs #
I added a counter component based in angular 1.5 for this App. Please see the file App.js in the js folder.
The component reads from a Web Service which accesses to a DB (Microsoft SQL Server) getting the number of footprints and then show a running counter from 1 to the number of connections by increasing that number every 1 milisecond.

This component was added into the current project shown in this repo. However, as it is an internal project, you can see the public version of this project [here][mapcentre]. Unfortunately not such counter component is in the public one.


# README #

This project is going to be the newest map app for the City of St. John's, it also replaced the old one, which was done using Microsoft Silverlight.

### What is this repository for? ###

* The whole idea is to take advantage the cutting-edge technologies in matter of repositories.
* Current version is 1.7.14. (2017/03/01). I earnestly beg to follow the [Semantic Versioning](http://semver.org/spec/v2.0.0-rc.1.html) for new releases.
* You can see Mapcentre running [here](http://lisapp01/mapcentre/index.html).

### How do I get set up? ###

The App is built using a combination of a bunch of libraries. Please be aware any modification may cause undesirable changes. When you modify something, testing it at least in IE, chrome, Firefox and iPhone too before you do a commit here.

[mapcentre]: http://map.stjohns.ca