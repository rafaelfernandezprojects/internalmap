/*------------------------------------------------------+
 |  Writing more code here for minimizing index.html    |
 |  Javascript on CSJ ------ MAPCENTRE Application      |
 +-----------------------------------------------------*/
//Global here
var identifyMyGlobalTask;
var identifyMyGlobalParams;

/*------------------------+
 |       DEM FUNCTIONS    |
 +-----------------------*/
function identifyLayerInTOC(evt){ /*Function coming from index.html*/
    require([
        "esri/map", "esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters", "esri/layers/TileInfo", "dojo/promise/all"], function(Map, IdentifyTask, IdentifyParameters, TileInfo, all) {

        var myDeferredIM360;
        var myDeferredSewer;
        var myDeferredWater;
        var myDeferredTraffic;
        var numLayer = 0;

        if (map.getZoom() > 5) {

            //alert(toc.findTOCNode(planning, 0).collapsed);


            for(var j = 0; j < map.layerIds.length; j++) {
                var layer = map.getLayer(map.layerIds[j]);
                //alert(layer.id + ' ' + layer.url + ' ' + layer.visible);
                if (layer.visible == true) {
                    switch (layer.id) {
                        case "IM360":
                            numLayer = numLayer + 1;
                            var identifyIM360Task = null; //Reinitialization for each identify
                            var identifyIM360Params = null; //Reinitialization for each identify

                            //create identify tasks and setup parameters
                            identifyIM360Task = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/MC_SJ_2014_IM360_Map_Service/MapServer");

                            identifyIM360Params = new IdentifyParameters();
                            identifyIM360Params.tolerance = 7;
                            identifyIM360Params.returnGeometry = false;
                            identifyIM360Params.layerIds = [0];
                            identifyIM360Params.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
                            identifyIM360Params.width = map.width;
                            identifyIM360Params.height = map.height;

                            identifyIM360Params.geometry = evt.mapPoint;
                            identifyIM360Params.mapExtent = map.extent;
                            break;
                        case "sewer":
                            numLayer = numLayer + 2;
                            var identifySewerTask = null; //Reinitialization for each identify
                            var identifySewerParams = null; //Reinitialization for each identify

                            //create identify tasks and setup parameters
                            identifySewerTask = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/MC_STJ_Sewer_Map_Service/MapServer");

                            identifySewerParams = new IdentifyParameters();
                            identifySewerParams.tolerance = 7;
                            identifySewerParams.returnGeometry = false;
                            identifySewerParams.layerIds = [0,1,2,3,5];
                            identifySewerParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
                            identifySewerParams.width = map.width;
                            identifySewerParams.height = map.height;

                            identifySewerParams.geometry = evt.mapPoint;
                            identifySewerParams.mapExtent = map.extent;

                            break;
                        case "waterServices":
                            numLayer = numLayer + 4;

                            var identifyWaterTask = null; //Reinitialization for each identify
                            var identifyWaterParams = null; //Reinitialization for each identify

                            //create identify tasks and setup parameters
                            identifyWaterTask = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/MC_STJ_WaterServices_Map_Service/MapServer");

                            identifyWaterParams = new IdentifyParameters();
                            identifyWaterParams.tolerance = 7;
                            identifyWaterParams.returnGeometry = false;
                            identifyWaterParams.layerIds = [0,1];
                            identifyWaterParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
                            identifyWaterParams.width = map.width;
                            identifyWaterParams.height = map.height;

                            identifyWaterParams.geometry = evt.mapPoint;
                            identifyWaterParams.mapExtent = map.extent;
                            break;
                        case "trafficSigns":
                            numLayer = numLayer + 8;

                            var identifyTrafficSignsTask = null; //Reinitialization for each identify
                            var identifyTrafficSignsParams = null; //Reinitialization for each identify

                            //create identify tasks and setup parameters
                            identifyTrafficSignsTask = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/MC_STJ_Traffic_Signage_Map_Service/MapServer");

                            identifyTrafficSignsParams = new IdentifyParameters();
                            identifyTrafficSignsParams.tolerance = 7;
                            identifyTrafficSignsParams.returnGeometry = false;
                            identifyTrafficSignsParams.layerIds = [0];
                            identifyTrafficSignsParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
                            identifyTrafficSignsParams.width = map.width;
                            identifyTrafficSignsParams.height = map.height;

                            identifyTrafficSignsParams.geometry = evt.mapPoint;
                            identifyTrafficSignsParams.mapExtent = map.extent;
                            break;
                    }

                }

            }

            var content = "";
            var ly = "";
            var symbol = "";
            map.infoWindow.setTitle("Symbols information");
            map.infoWindow.hide();

            if(numLayer == 1) { //IM360
                myDeferredIM360 = identifyIM360Task.execute(identifyIM360Params);
                var promises = new all([myDeferredIM360]);
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            if(n.layerName == "IM360_2014") {
                                ly = "Layer: <b>IM360 2014</b>";
                                symbol = "Source: " + n.feature.attributes.SOURCE + "<br>Frame: <a href='" + n.feature.attributes.HTML + "' target='_blank'>" + n.feature.attributes.frame + "</a>";
                            }
                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 2) { //Sewer
                myDeferredSewer = identifySewerTask.execute(identifySewerParams);
                var promises = new all([myDeferredSewer]);
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            if(n.layerName == "Manhole") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Manhole";
                            }
                            if(n.layerName == "Storm Headwall") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Storm Headwall";
                            }
                            if(n.layerName == "Wetwell") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Wetwell";
                            }
                            if(n.layerName == "Node") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Node";
                            }
                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 3) { //IM360 and Sewer
                myDeferredIM360 = identifyIM360Task.execute(identifyIM360Params);
                myDeferredSewer = identifySewerTask.execute(identifySewerParams);
                var promises = new all([myDeferredIM360,myDeferredSewer]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            if(n.layerName == "IM360_2014") {
                                ly = "Layer: <b>IM360 2014</b>";
                                symbol = "Source: " + n.feature.attributes.SOURCE + "<br>Frame: <a href='" + n.feature.attributes.HTML + "' target='_blank'>" + n.feature.attributes.frame + "</a>";
                            }

                            if(n.layerName == "Manhole") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Manhole";
                            }
                            if(n.layerName == "Storm Headwall") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Storm Headwall";
                            }
                            if(n.layerName == "Wetwell") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Wetwell";
                            }
                            if(n.layerName == "Node") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Node";
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 4) { //Water Services
                myDeferredWater = identifyWaterTask.execute(identifyWaterParams);
                var promises = new all([myDeferredWater]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            if(n.layerName == "Hydrant") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Hydrant";
                            }
                            if(n.layerName == "Valve") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Valve";
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 5) { //IM360 and Water Services
                myDeferredIM360 = identifyIM360Task.execute(identifyIM360Params);
                myDeferredWater = identifyWaterTask.execute(identifyWaterParams);
                var promises = new all([myDeferredIM360,myDeferredWater]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            if(n.layerName == "IM360_2014") {
                                ly = "Layer: <b>IM360 2014</b>";
                                symbol = "Source: " + n.feature.attributes.SOURCE + "<br>Frame: <a href='" + n.feature.attributes.HTML + "' target='_blank'>" + n.feature.attributes.frame + "</a>";
                            }

                            if(n.layerName == "Hydrant") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Hydrant";
                            }
                            if(n.layerName == "Valve") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Valve";
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 6) { //Sewer and Water Services
                myDeferredSewer = identifySewerTask.execute(identifySewerParams);
                myDeferredWater = identifyWaterTask.execute(identifyWaterParams);
                var promises = new all([myDeferredSewer,myDeferredWater]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            if(n.layerName == "Manhole") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Manhole";
                            }
                            if(n.layerName == "Storm Headwall") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Storm Headwall";
                            }
                            if(n.layerName == "Wetwell") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Wetwell";
                            }
                            if(n.layerName == "Node") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Node";
                            }

                            if(n.layerName == "Hydrant") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Hydrant";
                            }
                            if(n.layerName == "Valve") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Valve";
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 7) { //IM360 and Sewer and Water Services
                myDeferredIM360 = identifyIM360Task.execute(identifyIM360Params);
                myDeferredSewer = identifySewerTask.execute(identifySewerParams);
                myDeferredWater = identifyWaterTask.execute(identifyWaterParams);
                var promises = new all([myDeferredIM360,myDeferredSewer,myDeferredWater]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            if(n.layerName == "IM360_2014") {
                                ly = "Layer: <b>IM360 2014</b>";
                                symbol = "Source: " + n.feature.attributes.SOURCE + "<br>Frame: <a href='" + n.feature.attributes.HTML + "' target='_blank'>" + n.feature.attributes.frame + "</a>";
                            }

                            if(n.layerName == "Manhole") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Manhole";
                            }
                            if(n.layerName == "Storm Headwall") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Storm Headwall";
                            }
                            if(n.layerName == "Wetwell") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Wetwell";
                            }
                            if(n.layerName == "Node") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Node";
                            }

                            if(n.layerName == "Hydrant") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Hydrant";
                            }
                            if(n.layerName == "Valve") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Valve";
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 8) { //Traffic Signs
                myDeferredTraffic = identifyTrafficSignsTask.execute(identifyTrafficSignsParams);
                var promises = new all([myDeferredTraffic]);
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            //alert(n.layerName);
                            if(n.layerName == "Signs-With Symbols") {
                                ly = "Layer: <b>Signs</b>";
                                symbol = "Symbol: " + n.feature.attributes.CLASS;
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;

                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }

            if(numLayer == 9) { //IM360 and Traffic Signs
                myDeferredIM360 = identifyIM360Task.execute(identifyIM360Params);
                myDeferredTraffic = identifyTrafficSignsTask.execute(identifyTrafficSignsParams);
                var promises = new all([myDeferredIM360,myDeferredTraffic]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            if(n.layerName == "IM360_2014") {
                                ly = "Layer: <b>IM360 2014</b>";
                                symbol = "Source: " + n.feature.attributes.SOURCE + "<br>Frame: <a href='" + n.feature.attributes.HTML + "' target='_blank'>" + n.feature.attributes.frame + "</a>";
                            }
                            if(n.layerName == "Signs-With Symbols") {
                                ly = "Layer: <b>Signs</b>";
                                symbol = "Symbol: " + n.feature.attributes.CLASS;
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }

            if(numLayer == 10) {
                myDeferredSewer = identifySewerTask.execute(identifySewerParams);
                myDeferredTraffic = identifyTrafficSignsTask.execute(identifyTrafficSignsParams);
                var promises = new all([myDeferredSewer,myDeferredTraffic]);

                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            //alert(n.layerName);
                            if(n.layerName == "Signs-With Symbols") {
                                ly = "Layer: <b>Signs</b>";
                                symbol = "Symbol: " + n.feature.attributes.CLASS;
                            }

                            if(n.layerName == "Manhole") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Manhole";
                            }
                            if(n.layerName == "Storm Headwall") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Storm Headwall";
                            }
                            if(n.layerName == "Wetwell") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Wetwell";
                            }
                            if(n.layerName == "Node") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Node";
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;

                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });

            }
            if(numLayer == 11) { //IM360 and Sewer and Traffic Signs
                myDeferredIM360 = identifyIM360Task.execute(identifyIM360Params);
                myDeferredSewer = identifySewerTask.execute(identifySewerParams);
                myDeferredTraffic = identifyTrafficSignsTask.execute(identifyTrafficSignsParams);
                var promises = new all([myDeferredIM360,myDeferredSewer,myDeferredTraffic]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            //IM360
                            if(n.layerName == "IM360_2014") {
                                ly = "Layer: <b>IM360 2014</b>";
                                symbol = "Source: " + n.feature.attributes.SOURCE + "<br>Frame: <a href='" + n.feature.attributes.HTML + "' target='_blank'>" + n.feature.attributes.frame + "</a>";
                            }
                            //Sewer
                            if(n.layerName == "Manhole") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Manhole";
                            }
                            if(n.layerName == "Storm Headwall") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Storm Headwall";
                            }
                            if(n.layerName == "Wetwell") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Wetwell";
                            }
                            if(n.layerName == "Node") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Node";
                            }
                            //Traffic signs
                            if(n.layerName == "Signs-With Symbols") {
                                ly = "Layer: <b>Signs</b>";
                                symbol = "Symbol: " + n.feature.attributes.CLASS;
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 12) { //Water services and Traffic Signs
                myDeferredWater = identifyWaterTask.execute(identifyWaterParams);
                myDeferredTraffic = identifyTrafficSignsTask.execute(identifyTrafficSignsParams);
                var promises = new all([myDeferredWater,myDeferredTraffic]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            //Water services
                            if(n.layerName == "Hydrant") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Hydrant";
                            }
                            if(n.layerName == "Valve") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Valve";
                            }
                            //Traffic signs
                            if(n.layerName == "Signs-With Symbols") {
                                ly = "Layer: <b>Signs</b>";
                                symbol = "Symbol: " + n.feature.attributes.CLASS;
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 13) { //IM360 and Water services and Traffic Signs
                myDeferredIM360 = identifyIM360Task.execute(identifyIM360Params);
                myDeferredWater = identifyWaterTask.execute(identifyWaterParams);
                myDeferredTraffic = identifyTrafficSignsTask.execute(identifyTrafficSignsParams);
                var promises = new all([myDeferredIM360,myDeferredWater,myDeferredTraffic]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            //IM360
                            if(n.layerName == "IM360_2014") {
                                ly = "Layer: <b>IM360 2014</b>";
                                symbol = "Source: " + n.feature.attributes.SOURCE + "<br>Frame: <a href='" + n.feature.attributes.HTML + "' target='_blank'>" + n.feature.attributes.frame + "</a>";
                            }
                            //Water services
                            if(n.layerName == "Hydrant") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Hydrant";
                            }
                            if(n.layerName == "Valve") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Valve";
                            }
                            //Traffic signs
                            if(n.layerName == "Signs-With Symbols") {
                                ly = "Layer: <b>Signs</b>";
                                symbol = "Symbol: " + n.feature.attributes.CLASS;
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 14) { //Sewer and Water services and Traffic Signs
                myDeferredSewer = identifySewerTask.execute(identifySewerParams);
                myDeferredWater = identifyWaterTask.execute(identifyWaterParams);
                myDeferredTraffic = identifyTrafficSignsTask.execute(identifyTrafficSignsParams);
                var promises = new all([myDeferredSewer,myDeferredWater,myDeferredTraffic]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            //Sewer
                            if(n.layerName == "Manhole") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Manhole";
                            }
                            if(n.layerName == "Storm Headwall") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Storm Headwall";
                            }
                            if(n.layerName == "Wetwell") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Wetwell";
                            }
                            if(n.layerName == "Node") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Node";
                            }
                            //Water services
                            if(n.layerName == "Hydrant") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Hydrant";
                            }
                            if(n.layerName == "Valve") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Valve";
                            }
                            //Traffic signs
                            if(n.layerName == "Signs-With Symbols") {
                                ly = "Layer: <b>Signs</b>";
                                symbol = "Symbol: " + n.feature.attributes.CLASS;
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }
            if(numLayer == 15) { //IM360 and Sewer and Water services and Traffic Signs
                myDeferredIM360 = identifyIM360Task.execute(identifyIM360Params);
                myDeferredSewer = identifySewerTask.execute(identifySewerParams);
                myDeferredWater = identifyWaterTask.execute(identifyWaterParams);
                myDeferredTraffic = identifyTrafficSignsTask.execute(identifyTrafficSignsParams);
                var promises = new all([myDeferredIM360,myDeferredSewer,myDeferredWater,myDeferredTraffic]);
                esri.show(loading); //it was defined in index.html (line 230)
                map.disableMapNavigation();
                map.hideZoomSlider();
                promises.then(function (results){
                    $.each(results, function (i, v) {
                        $.each(v, function (m, n) {
                            //IM360
                            if(n.layerName == "IM360_2014") {
                                ly = "Layer: <b>IM360 2014</b>";
                                symbol = "Source: " + n.feature.attributes.SOURCE + "<br>Frame: <a href='" + n.feature.attributes.HTML + "' target='_blank'>" + n.feature.attributes.frame + "</a>";
                            }
                            //Sewer
                            if(n.layerName == "Manhole") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Manhole";
                            }
                            if(n.layerName == "Storm Headwall") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Storm Headwall";
                            }
                            if(n.layerName == "Wetwell") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Wetwell";
                            }
                            if(n.layerName == "Node") {
                                ly = "Layer: <b>Sewer</b>";
                                symbol = "Symbol: Node";
                            }
                            //Water services
                            if(n.layerName == "Hydrant") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Hydrant";
                            }
                            if(n.layerName == "Valve") {
                                ly = "Layer: <b>Water services</b>";
                                symbol = "Symbol: Valve";
                            }
                            //Traffic signs
                            if(n.layerName == "Signs-With Symbols") {
                                ly = "Layer: <b>Signs</b>";
                                symbol = "Symbol: " + n.feature.attributes.CLASS;
                            }

                            if (content != "") content = content + "<br/>";
                            content = content + ly + "<br/>" + symbol;
                        });
                        esri.hide(loading);
                        map.enableMapNavigation();
                        map.showZoomSlider();
                        if (content != "") { //only if success
                            map.infoWindow.setContent(content);
                            map.infoWindow.show(evt.screenPoint);
                        }
                        else {
                            map.infoWindow.setContent("Not symbols found.");
                            map.infoWindow.show(evt.screenPoint);
                        }
                    });
                });
            }

        }
        else {
            //if(mode != "DEM" && mode != "measure" && mode != "GoogleStreetView" && mode != "pushpin") {
                $.confirm({
                    title: 'Symbols information',
                    content: 'To identify symbols, you need more zoom on the map.<br><b>Please, don\'t forget to activate some map layers</b><br>Would you like more zoom now?',
                    theme: 'white',
                    autoClose: 'cancel|8000',
                    confirm: function () {
                        map.centerAndZoom(evt.mapPoint, 8);
                    },
                    cancel: function () {

                    }
                });
            //}
        }//alert("To identify symbols, you need more zoom on the map");



    });
}

function handleResults(results) {

    $.each(results, function (i, v) {

        $.each(v, function (m, n) {
            alert(n.layerName);
        });


    });

}

