/*------------------------------------------------------+
 |  Writing more code here for minimizing index.html    |
 |  Javascript on CSJ ------ MAPCENTRE Application      |
 +-----------------------------------------------------*/
//Global here
var countHideCollapse = 0; //it ia used to avoid the minus icon before we see the dialog

/*-------------------------------+
 |       Video Help FUNCTIONS    |
 +------------------------------*/
if (document.getElementById('VideoIntro').addEventListener) {
    document.getElementById('VideoIntro').addEventListener("click", function() {
        printActivity("Video-Introduction");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_Introduction";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoIntro').attachEvent("onclick", function() {
        printActivity("Video-Introduction");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_Introduction";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoMapSwitcher').addEventListener) {
    document.getElementById('VideoMapSwitcher').addEventListener("click", function() {
        printActivity("Video-MapSwitcher");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapSwitcher";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoMapSwitcher').attachEvent("onclick", function() {
        printActivity("Video-MapSwitcher");
        var myVideo = "mapcentre_MapSwitcher";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoTemplates').addEventListener) {
    document.getElementById('VideoTemplates').addEventListener("click", function() {
        printActivity("Video-MapTemplates");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapTemplates";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoTemplates').attachEvent("onclick", function() {
        printActivity("Video-MapTemplates");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapTemplates";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoMapLayers').addEventListener) {
    document.getElementById('VideoMapLayers').addEventListener("click", function() {
        printActivity("Video-MapLayers");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapLayers";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoMapLayers').attachEvent("onclick", function() {
        printActivity("Video-MapLayers");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapLayers";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoPrintDialog').addEventListener) {
    document.getElementById('VideoPrintDialog').addEventListener("click", function() {
        printActivity("Video-MapPrint");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapPrint";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoPrintDialog').attachEvent("onclick", function() {
        printActivity("Video-MapPrint");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapPrint";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoSearch').addEventListener) {
    document.getElementById('VideoSearch').addEventListener("click", function() {
        printActivity("Video-MapSearch");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapSearch";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoSearch').attachEvent("onclick", function() {
        printActivity("Video-MapSearch");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapSearch";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoIdentify').addEventListener) {
    document.getElementById('VideoIdentify').addEventListener("click", function() {
        printActivity("Video-MapIdentify");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapIdentify";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoIdentify').attachEvent("onclick", function() {
        printActivity("Video-MapIdentify");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapIdentify";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoDraw').addEventListener) {
    document.getElementById('VideoDraw').addEventListener("click", function() {
        printActivity("Video-MapDraw");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapDraw";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoDraw').attachEvent("onclick", function() {
        printActivity("Video-MapDraw");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapDraw";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoParcels').addEventListener) {
    document.getElementById('VideoParcels').addEventListener("click", function() {
        var myVideo = "mapcentre_MapParcel";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoParcels').attachEvent("onclick", function() {
        var myVideo = "mapcentre_MapParcel";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoSightLines').addEventListener) {
    document.getElementById('VideoSightLines').addEventListener("click", function () {
        var myVideo = "mapcentre_MapSightLines";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoSightLines').attachEvent("onclick", function () {
        var myVideo = "mapcentre_MapSightLines";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoMeasure').addEventListener) {
    document.getElementById('VideoMeasure').addEventListener("click", function() {
        printActivity("Video-MapMeasure");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapMeasure";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoMeasure').attachEvent("onclick", function() {
        printActivity("Video-MapMeasure");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapMeasure";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoProfile').addEventListener) {
    document.getElementById('VideoProfile').addEventListener("click", function () {
        printActivity("Video-MapProfile");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapProfile";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoProfile').attachEvent("onclick", function () {
        printActivity("Video-MapProfile");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapProfile";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoGoTo').addEventListener) {
    document.getElementById('VideoGoTo').addEventListener("click", function() {
        printActivity("Video-MapGoto");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapGoto";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoGoTo').attachEvent("onclick", function() {
        printActivity("Video-MapGoto");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapGoto";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoStreetView').addEventListener) {
    document.getElementById('VideoStreetView').addEventListener("click", function() {
        printActivity("Video-MapStreetView");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapStreetView";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoStreetView').attachEvent("onclick", function() {
        printActivity("Video-MapStreetView");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapStreetView";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoForecast').addEventListener) {
    document.getElementById('VideoForecast').addEventListener("click", function() {
        printActivity("Video-MapForecast");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapForecast";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoForecast').attachEvent("onclick", function() {
        printActivity("Video-MapForecast");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapForecast";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}

if (document.getElementById('VideoHelpUs').addEventListener) {
    document.getElementById('VideoHelpUs').addEventListener("click", function() {
        printActivity("Video-MapContact");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapContact";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    }, false);
}
else {
    document.getElementById('VideoHelpUs').attachEvent("onclick", function() {
        printActivity("Video-MapContact");
        document.getElementById("map_layers").style.cursor = "default";
        var myVideo = "mapcentre_MapContact";
        window.open("Mapcentre_Video_Tutorials.html?Video=" + myVideo, "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
    });
}


//Collapsing the list in Mapcentre Video Help Dialog
$('.collapsibleList').on('show.bs.collapse', function (e) {
    $(e.target).prev().find('.glyphicon').toggleClass('glyphicon-minus glyphicon-plus');
}).on('hide.bs.collapse', function(e){
    countHideCollapse++;
    if (countHideCollapse > 4) $(e.target).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
});
