// Declare app level module which depends on filters, and services
var myApp = angular.module("myApp", [/* No Dependency Injection */]);

//timeout and scope are Angular's built-in services
//Adding injector
myApp.controller("demoController",['$scope','$timeout', function($scope,$timeout){

    //Adding initial value for counter
    $scope.counter = 0;
    var stopped;
    var myCount;

//Cancels a task associated with the promise. As a result of this, the //promise will be resolved with a rejection.
    $scope.countup = function() {
        stopped = $timeout(function() {
            //console.log($scope.counter);
            $scope.counter++;
            if($scope.counter > myCount) $scope.stop()
            else $scope.countup();
        }, 1); //1 millisecond for changing to the next number
    };

    $scope.stop = function(){
        $timeout.cancel(stopped);
    };

    $scope.reset = function(){
        $timeout.cancel(stopped);
        $scope.counter = 0;
    };

    $scope.getCount = function() {
        var checkBrowserAJAX01 = $.ajax({
            crossDomain: true,
            type: "GET",
            url: "http://listestsrv.stjohnstest.com/mapcentre_testing/htmlApptracking/mapcentrehtmltracking.asmx/countActivity",
            data: {toolActivity: "mapcentre"},
            timeout: 1000,
            success: function (result) {
                var myJson = $.xml2json(result);
                myCount = parseInt(myJson);
								$scope.counter = myCount - 500;
                $scope.countup();
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
                myCount = 2560;
                $scope.countup();
            }
        });
    };
}]);