/*------------------------------------------------------+
 |  Writing more code here for minimizing index.html    |
 |  Javascript on CSJ ------ MAPCENTRE Application      |
 +-----------------------------------------------------*/
//globals
var intersectionPoint;
var vertexInParcel = [];
var firstSegment;
var SecondSegment;
var myCollision = [];
var segmentsAffected = [];
var myParcel;

/*--------------------------------+
 |       DRAW TOOLBAR FUNCTIONS    |
 +--------------------------------*/
function click_SightLine() { /*Function coming from index.html*/
    require([
        "esri/map"], function(Map) {

        setTemplateOrtho();

        printActivity("SightLine");
        document.getElementById("map_layers").style.cursor = "default";

            $("#dialog-sight-line-tool").dialog({
            //height: 300,
            width: 310,
            dialogClass: "alertDialog",
            resizable: false,
            closeOnEscape: false, // disable escape event on dialog
            //buttons: [{ text: "Ok", click: function() { $( this ).dialog( "close" ); } }],
            close: function (event, ui) {
                map.getLayer("layerRetrievingParcels").clear();
                map.getLayer("layerDrawingParcels").clear();
                mode = "default";

            },
            modal: false
        })
            .dialogExtend({
                "minimizable" : true,
                "icons" : { "minimize" : "ui-icon-circle-minus" }
            });

            //Initialitation
            mode = "sightline";
            //Loop for each element in order to remove them
            var myNode = document.getElementById('divStepsInspectionServices');
            while (myNode.lastChild) {
                myNode.removeChild(myNode.lastChild);
            }
            $("#selectTypeSightLine").val("opCornerTriangle");
            $('#divStepsInspectionServices')
                .append("<label id='lblStepInspectionServices'>1.- Data point inside the property</label>");
    });
}

function setTemplateOrtho() {
    topo.hide();
	ortho2015.show();
    ortho2013.hide();
    ortho2010.hide();
    ortho2006.hide();
    ortho2003.hide();
    //DEM.hide();
    CityLabelsImagery.show();
    Property.show();
    Annotation.show();
    Address_Annotation.show();
    CityBounds.show();
    Elevation.hide();
    wards.hide();
    planning.hide();
    IM360.hide();
    watersheds.hide();
    sewer.hide();
    waterServices.hide();
    trafficSigns.hide();
    neighbourhood.hide();

    document.getElementById("elevationDIV").style.display = "none";
    mode = "";

    imgTopo.style.border = "1px solid black";
	imgOrtho2015.style.border = "4px solid black";
    imgOrtho2013.style.border = "1px solid black";
    imgOrtho2010.style.border = "1px solid black";
    imgOrtho2006.style.border = "1px solid black";
    imgOrtho2003.style.border = "1px solid black";
    //imgDEM.style.border = "1px solid black";
}

function getParcelCoordinatesByDatapoint(evt, myOption) {

    if (myOption === "opSightLine") { //We need here other different scenario
        $('input[name=sightlineScenario]').each(function () {
            //alert(this.value + ":" + this.checked);
            if (this.checked === true) {
                esri.show(loading);
                getParcelByDataPoint(evt, this.value);
            }
        });

        return;
    }

    //console.log(evt.mapPoint.x);
    
    vertexInParcel.length = 0;
    map.getLayer("layerRetrievingParcels").clear();
    map.getLayer("layerDrawingParcels").clear();

    var qt = new esri.tasks.QueryTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_QueryTaskPublic_MapService/MapServer/1");
    var q = new esri.tasks.Query();
    q.outSpatialReference = { "wkid": 32181 };
    q.returnGeometry = true;
    q.geometry = evt.mapPoint;
    q.spatialRelationship = esri.tasks.Query.SPATIAL_REL_INTERSECTS;
    q.geometryType = "esriGeometryPolygon";
    var myDeferredRetrieve = qt.execute(q);
    myDeferredRetrieve.then(esri.show(loading));

    qt.on("complete", function (evt2) {
        esri.hide(loading);
        if ((map.extent.xmax - map.extent.xmin) > 600) {
            $.alert({
                title: 'Too many features',
                content: 'You need more zoom on the map please',
                theme: 'white',
                autoClose: 'confirm|3000',
                confirm: function () {
                }
            });
            return;
        }
        gLayerRetrievingParcels.clear();
        var symbol = new esri.symbol.SimpleFillSymbol()
            .setColor(new dojo.Color([255, 255, 255, 0]))
            .setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 255, 255]), 3));

        dojo.some(evt2.featureSet.features, function (feature) {
            //gLayerRetrievingParcels.add(feature.setSymbol(symbol));
            myParcel = feature.setSymbol(symbol);
            gLayerRetrievingParcels.add(myParcel);
            var numVertex = parseInt(feature.geometry.rings[0].length);
            var myPolygon = feature.geometry.rings[0];
            for (var i = 0; i < numVertex; i++) {
                //console.log("inside first loop");
                //console.log(feature.geometry.rings[0][i][0]);
                vertexInParcel.push({ index: i, x: feature.geometry.rings[0][i][0], y: feature.geometry.rings[0][i][1] });
            }
            //Returns the centroid of the polygon
            //var myCentroid = feature.geometry.getCentroid();
            //Checks if our Polygon ring is clockwise
            var clockwise = feature.geometry.isClockwise(feature.geometry.rings[0]);
            
            drawTextInSegment(vertexInParcel);
            startNextStep("2", myOption, vertexInParcel, clockwise);
           
            return true; //break the loop avoiding stack polygons
        });
    });
}

function drawTextInSegment(arrayVertex) {
    require([
        "esri/map", "esri/graphic", "dojo/_base/connect", "esri/symbols/Font", "esri/symbols/TextSymbol", "esri/Color", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol"],
            function (Map, Graphic, connect, Font, TextSymbol, Color, SimpleMarkerSymbol, SimpleLineSymbol) {

            var myTextPoint_X = 0;
            var myTextPoint_Y = 0;
            for (var i = 0; i < arrayVertex.length - 1; i++) {
                var Dx = arrayVertex[i + 1].x - arrayVertex[i].x;
                var Dy = arrayVertex[i + 1].y - arrayVertex[i].y;
                var distance = Math.sqrt(Math.pow(Dx, 2) + Math.pow(Dy, 2));
                if (distance < 1.500) continue; //Filtering by distance. We don't want short segments
                myTextPoint_X = parseFloat(arrayVertex[i].x + (Dx / 2));
                myTextPoint_Y = parseFloat(arrayVertex[i].y + (Dy / 2));

                var inPoint = new esri.geometry.Point([myTextPoint_X, myTextPoint_Y], map.spatialReference);

                var font = new Font("15px", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL);
                var textSymbol = new TextSymbol(i + 1, font, new Color([255, 255, 255, 1]));
                textSymbol.horizontalAlignment = "center";
                textSymbol.verticalAlignment = "middle"; //Not supported in IE
                textSymbol.yoffset = -5;

                var labelPointGraphic = new Graphic(inPoint, textSymbol);

                var symbolCircle = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 15, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 0, 0]), 1), new Color([0, 0, 0, 1]));
                var SymbolGraphic = new Graphic(inPoint, symbolCircle);

                // add the label point graphic to the map
                gLayerRetrievingParcels.add(SymbolGraphic);
                gLayerRetrievingParcels.add(labelPointGraphic);
        

            }
        });
}



function startNextStep(step, myOption, vertex, clockwise) {
    var msg;
    //Loop for each element in order to remove them
    var myNode = document.getElementById('divStepsInspectionServices');
    while (myNode.lastChild) {
        myNode.removeChild(myNode.lastChild);
    }

    switch (myOption) {
        case "opCornerTriangle":
            switch (step) {
                case "2":
                    msg = step + ".- Please, indicate the segments that will intersect to make the corner";
                    $('#divStepsInspectionServices')
                        .append("<label id='lblStepInspectionServices'>" + msg + "</label>");

                    var options = '';
                    for (var i = 0; i < vertex.length - 1; i++) {
                        var tmp01 = i + 1;
                        options += "<option value='" + tmp01 + "'>" + tmp01 + "</option>";
                    }
                    
                    $('#divStepsInspectionServices')
                        .append("<select id='segment01' class='input-sm' style='width: 27%'></select>");
                    $('#segment01').html(options);
                    
                    $('#divStepsInspectionServices')
                        .append("<select id='segment02' class='input-sm' style='width: 27%; margin-left: 5px'></select>");
                    $('#segment02').html(options);
                    
                    $('#divStepsInspectionServices')
                        .append("<button id='btnCornerTriangle' type='button' class='btn btn-default' style='width: 25%; float: right'>Go</button>");

                    $("#btnCornerTriangle").click({ param1: vertex, param2: clockwise }, getIntersection);

                    break;
            }
            
    }
    
}

function getIntersection(event) {
    require(["esri/geometry/mathUtils", "esri/geometry/Point", "esri/SpatialReference", "esri/symbols/CartographicLineSymbol",
    "esri/map", "esri/graphic", "dojo/_base/connect", "esri/Color", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol", "esri/geometry/Polyline"],
    function (mathUtils, Point, SpatialReference, CartographicLineSymbol, Map, Graphic, connect, Color, SimpleMarkerSymbol, SimpleLineSymbol, Polyline) {

        gLayerRetrievingParcels.remove(GraphicInters);

        firstSegment = parseInt($('#segment01').val());
        SecondSegment = parseInt($('#segment02').val());
        if (firstSegment == SecondSegment) {
            alert("Segments are identicals");
            return;
        }
        
        var firstXori = new Point(parseFloat(event.data.param1[firstSegment - 1].x), parseFloat(event.data.param1[firstSegment - 1].y), new SpatialReference({ wkid: 32181 }));
        var firstXend = new Point(parseFloat(event.data.param1[firstSegment].x), parseFloat(event.data.param1[firstSegment].y), new SpatialReference({ wkid: 32181 }));
        var secondXori = new Point(parseFloat(event.data.param1[SecondSegment - 1].x), parseFloat(event.data.param1[SecondSegment - 1].y), new SpatialReference({ wkid: 32181 }));
        var secondXend = new Point(parseFloat(event.data.param1[SecondSegment].x), parseFloat(event.data.param1[SecondSegment].y), new SpatialReference({ wkid: 32181 }));

        
        intersectionPoint = esri.geometry.getLineIntersection(firstXori, firstXend, secondXori, secondXend);
        if (intersectionPoint) {
            var PropertyIsClockwise = event.data.param2;
            

            /* Test area */
            //var symbolLine = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DOT, new Color([255, 255, 0]), 3);
            //var singlePathPolyline1 = new Polyline([[event.data.param2.x, event.data.param2.y], [firstXori.x, firstXori.y]]);
            //var singlePathPolyline2 = new Polyline([[event.data.param2.x, event.data.param2.y], [secondXend.x, secondXend.y]]);
            //var GraphicLine1 = new Graphic(singlePathPolyline1, symbolLine);
            //var GraphicLine2 = new Graphic(singlePathPolyline2, symbolLine);
            //gLayerRetrievingParcels.add(GraphicLine1);
            //gLayerRetrievingParcels.add(GraphicLine2);

            var pathPolyline = new Polyline();
            //pathPolyline.addPath([[firstXori.x, firstXori.y], [secondXend.x, secondXend.y]]);
            segmentsAffected.length = 0;
            
            if (PropertyIsClockwise) {
                if (firstSegment < SecondSegment) {
                    for (var i = firstSegment; i <= SecondSegment; i++) {
                        var pOri = new Point(parseFloat(event.data.param1[i - 1].x), parseFloat(event.data.param1[i - 1].y), new SpatialReference({ wkid: 32181 }));
                        var pEnd = new Point(parseFloat(event.data.param1[i].x), parseFloat(event.data.param1[i].y), new SpatialReference({ wkid: 32181 }));
                        pathPolyline.addPath([pOri, pEnd]);
                        segmentsAffected.push({ xOri: pOri.x, yOri: pOri.y, xEnd: pEnd.x, yEnd: pEnd.y});
                    }
                }
                else {
                    for (var i = firstSegment; i < event.data.param1.length; i++) {
                        var pOri = new Point(parseFloat(event.data.param1[i - 1].x), parseFloat(event.data.param1[i - 1].y), new SpatialReference({ wkid: 32181 }));
                        var pEnd = new Point(parseFloat(event.data.param1[i].x), parseFloat(event.data.param1[i].y), new SpatialReference({ wkid: 32181 }));
                        pathPolyline.addPath([pOri, pEnd]);
                        segmentsAffected.push({ xOri: pOri.x, yOri: pOri.y, xEnd: pEnd.x, yEnd: pEnd.y });
                    }
                    for (var i = 1; i <= SecondSegment; i++) {
                        var pOri = new Point(parseFloat(event.data.param1[i - 1].x), parseFloat(event.data.param1[i - 1].y), new SpatialReference({ wkid: 32181 }));
                        var pEnd = new Point(parseFloat(event.data.param1[i].x), parseFloat(event.data.param1[i].y), new SpatialReference({ wkid: 32181 }));
                        pathPolyline.addPath([pOri, pEnd]);
                        segmentsAffected.push({ xOri: pOri.x, yOri: pOri.y, xEnd: pEnd.x, yEnd: pEnd.y });
                    }
                }
            }
            else {
                //TO DO
                alert("Property is counterclockwise. Please contact with LIS department");
            }
            //var symbolLine = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DOT, new Color([255, 255, 0]), 3);
            var symbolLine = new CartographicLineSymbol(CartographicLineSymbol.STYLE_SOLID, new Color([255, 0, 0, 0.5]), 10, CartographicLineSymbol.CAP_ROUND, CartographicLineSymbol.JOIN_ROUND, 5);
            var GraphicLine = new Graphic(pathPolyline, symbolLine);
            gLayerRetrievingParcels.add(GraphicLine);

            var symbolCircleInters = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 0, 255]), 1), new Color([255, 255, 255, 0.4]));
            var GraphicInters = new Graphic(intersectionPoint, symbolCircleInters);
            gLayerRetrievingParcels.add(GraphicInters); //Draw the intersection point
            
            /* END Test area */
            
            var lobibox = Lobibox.confirm({
                title: "Inspection services",
                msg: "Geometrically, do you agree to the intersection point (blue point) and the presumed sides of the triangle (red lines)?",
                draggable: true,
                closeOnEsc: false,
                callback: function ($this, type, ev) {
                    if (type === 'no') {
                        gLayerRetrievingParcels.remove(GraphicInters);
                        gLayerRetrievingParcels.remove(GraphicLine);
                    } else if (type === 'yes') {
                        getSpatialIntersectionWithStreets(intersectionPoint);
                    }
                }
            });
            var posicion_x = (screen.width / 2) - (lobibox.width / 2);
            lobibox.setPosition(posicion_x, 40);
        }
        

    });
    
}

function getSpatialIntersectionWithStreets(MyIntersection) {
    require(["esri/geometry/Point", "esri/geometry/Circle", "esri/map", "esri/graphic", "esri/Color", "esri/symbols/SimpleFillSymbol", "esri/symbols/SimpleLineSymbol",
        "esri/tasks/QueryTask", "esri/tasks/query", "esri/tasks/FindTask", "esri/tasks/FindParameters", "esri/tasks/IdentifyParameters", "esri/tasks/IdentifyTask", "esri/geometry/Extent"],
    function (Point, Circle, Map, Graphic, Color, SimpleFillSymbol, SimpleLineSymbol, QueryTask, Query, FindTask, FindParameters, IdentifyParameters, IdentifyTask, Extent) {

        var arrNamesAndClass = [];

        /*
        var circleSymb = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL,
              new SimpleLineSymbol(
                SimpleLineSymbol.STYLE_SHORTDASHDOTDOT,
                new Color([105, 105, 105]), 2), new Color([255, 255, 0, 0.25])
            );
        */
        var radius = 20;
        
        var myPoint = new esri.geometry.Point([MyIntersection.x, MyIntersection.y], map.spatialReference);

        /*
        var myCircle = new esri.geometry.Circle(myPoint, {
            "radius": radius
        });

        var graphic = new Graphic(myCircle, circleSymb);
        gLayerRetrievingParcels.add(graphic);
        */

        //Looking for Streets nearby
        var identifyTask = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_Query_Identify_TaskS_MapService/MapServer");
        var tolerance = radius / (map.extent.getWidth() / map.width);

        identifyParams = new IdentifyParameters();
        identifyParams.tolerance = tolerance;
        identifyParams.returnGeometry = true;
        identifyParams.layerIds = [2];
        identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
        identifyParams.width = map.width;
        identifyParams.height = map.height;
        identifyParams.geometry = myPoint;
        identifyParams.mapExtent = map.extent;

        identifyTask.execute(identifyParams, showResultsStreetAtt);
    });
}

function showResultsStreetAtt(results) {
    require(["esri/symbols/SimpleLineSymbol", "esri/Color", "esri/map", "esri/graphic"], function (SimpleLineSymbol, Color, Map, Graphic) {
        if (results.length > 0) {
            //Loop for each element in order to remove them
            var myNode = document.getElementById('divStepsInspectionServices');
            while (myNode.lastChild) {
                myNode.removeChild(myNode.lastChild);
            }
            //Creating new HTML elements
            var msg = "3.- According to street's classification, please choose a distance to draw the corner triangle.";
            $('#divStepsInspectionServices')
                .append("<label id='lblStepInspectionServices'>" + msg + "</label>");
            //Creating the table
            $('#divStepsInspectionServices')
                .append("<table style='width:100%' id='tblStreets'><tr style='background-color:#ccc;'><th>Street Name</th><th>Class</th><th>Color</th></tr></table>");

            var result, attribs;
            var data = [];
            var colorStreet = ['red', 'green', 'blue', 'yellow', 'grey', 'orange', 'black', 'fuchsia'];
            var colorStreetHex = ['#FF0000', '#63AB62', '#00BFFF', '#FFE600', '#A1A1A1', '#FF8000', '#000000', '#FF00AA'];
            var j = 0;
            dojo.forEach(results, function (result) {
                var attribs = result.feature.attributes;
                data.push({ StreetName: attribs.STREETNAME, Classification: attribs.STREETCLS });
                //Coloring each street
                var sls = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color.fromHex(colorStreetHex[j]), 3);
                //var sls = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255,0,0]), 3);
                var GraphicStreet = new Graphic(result.feature.geometry, sls);
                // add the label point graphic to the map
                gLayerRetrievingParcels.add(GraphicStreet);
                j++;
            });
            //Populating the table
            var html = '';
            var isArterial = false;
            for (var i = 0; i < data.length; i++) {
                html += '<tr><td>' + data[i].StreetName + '</td><td>' + data[i].Classification + '</td><td>' + colorStreet[i] + '</td></tr>';
                if (data[i].Classification == "ART-MA" || data[i].Classification == "ART-MI" || data[i].Classification == "COLLECTOR" || data[i].Classification == "FREEWAY") isArterial = true;
            }
            $('#tblStreets tr').first().after(html);
            //Adding a select control for choosing a distance
            var options = '';
            options = "<option value='8'>8 meters</option>";
            options += "<option value='15'>15 meters</option>";
       

            $('#divStepsInspectionServices')
                .append("<br><select id='triangleSide' class='input-sm' style='width: 45%'></select>");
            $('#triangleSide').html(options);
            //Select a convenient side
            if (isArterial) $("#triangleSide").val('15');
            $('#divStepsInspectionServices')
                .append("<button id='btnDrawTriangle' type='button' class='btn btn-default' style='width: 45%; float: right'>Draw the triangle</button>");
            //Adding a click event
            $("#btnDrawTriangle").click(drawTriangle);
        }
        else alert("We didn't find any street nearby 20 m.");
    
    });
}

function drawTriangle() {
    require(["esri/geometry/Point", "esri/SpatialReference", "esri/geometry/Polygon", "esri/symbols/Font", "esri/symbols/TextSymbol",
    "esri/map", "esri/graphic", "dojo/_base/connect", "esri/Color", "esri/symbols/SimpleFillSymbol", "esri/symbols/SimpleLineSymbol"],
    function (Point, SpatialReference, Polygon, Font, TextSymbol, Map, Graphic, connect, Color, SimpleFillSymbol, SimpleLineSymbol) {
    
        //Define the Circle to look for spatial intersection later
        var Cx = intersectionPoint.x;
        var Cy = intersectionPoint.y;
        var Cr = parseInt($("#triangleSide").val());
    
        myCollision.length = 0;

        for (var i=0;i<segmentsAffected.length;i++) {
            getIntersectionCircleLine2(segmentsAffected[i].xOri, segmentsAffected[i].yOri, segmentsAffected[i].xEnd, segmentsAffected[i].yEnd, Cx, Cy, Cr);
        }

        //getIntersectionCircleLine(x0, y0, x1, y1, Cx, Cy, Cr);
        //getIntersectionCircleLine(x2, y2, x3, y3, Cx, Cy, Cr);
        
        if (myCollision.length == 2) {
            var polygon = new esri.geometry.Polygon(new esri.SpatialReference({ wkid: 32181 }));
            polygon.addRing([[intersectionPoint.x, intersectionPoint.y], [myCollision[0].x, myCollision[0].y], [myCollision[1].x, myCollision[1].y], [intersectionPoint.x, intersectionPoint.y]]);

/*
            var mySymbol = new esri.symbol.SimpleFillSymbol()
                    .setColor(new dojo.Color([0, 0, 0, 0]))
                    .setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_BACKWARD_DIAGONAL, new dojo.Color([0, 0, 0]), 2));
*/

	    var mySymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_BACKWARD_DIAGONAL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 2),
			new Color([0,0,0,1])); /*----- Rafael 2016/09/21 ------*/
  	    	


            var polygonGraphic = new Graphic(polygon, mySymbol);
            //gLayerDrawingParcels.add(polygonGraphic);

            //Remove temporary graphics layers
            map.getLayer("layerRetrievingParcels").clear();
            map.getLayer("layerDrawingParcels").clear();
            
            gLayerDrawingParcels.add(myParcel);
            gLayerDrawingParcels.add(polygonGraphic);

            /*
            //Maybe it is interesting to add distances as a text too
            var quadrant = 0;
            var arrMiddlePoint = [];
            quadrant = getQuadrant(myCollision[0].x, myCollision[0].y, intersectionPoint.x, intersectionPoint.y);
            arrMiddlePoint = getMiddlePoint(myCollision[0].x, myCollision[0].y, intersectionPoint.x, intersectionPoint.y);
            var middlePoint = new Point(arrMiddlePoint[0].x, arrMiddlePoint[0].y, new SpatialReference({ wkid: 32181 }));

            var font = new Font("15px", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL);
            var textSymbolDistance = new TextSymbol({
                "color": [0, 0, 255, 255],
                "type": "esriTS",
                "angle": 45,
                //"xoffset": 10,
                //"yoffset": 10,
                "text": "my Text",
                "align": "middle",
                "decoration": "none",
                "rotated": false,
                "kerning": true,
                "font": {
                    "size": 15,
                    "weight": "normal",
                    "style": "normal",
                    "variant": "normal",
                    "family": "serif"
                }
            });

            var labelPointGraphicDistance = new Graphic(middlePoint, textSymbolDistance);
            gLayerDrawingParcels.add(labelPointGraphicDistance);
            */

            //Last Step
            //Loop for each element in order to remove them
            var myNode = document.getElementById('divStepsInspectionServices');
            while (myNode.lastChild) {
                myNode.removeChild(myNode.lastChild);
            }

            //Creating new HTML elements
            var msg = "4.- Maybe you would like to download it";
            $('#divStepsInspectionServices')
                .append("<label id='lblStepInspectionServices'>" + msg + "</label>");
            //Creating the button
            $('#divStepsInspectionServices')
                .append("<button id='btnDownloadTriangle' type='button' class='btn btn-default' style='width: 100%'>Print it!</button>");
            //Adding a click event
            $("#btnDownloadTriangle").click(printTriangle);
        }
        else {
            var lobibox = Lobibox.notify("error",{
                msg: "At least one side of the triangle is outside of the selected segments. Maybe the side of the property is shortest than the corner triangle distance?",
            });
        }
        
    });
}

function printTriangle() {
    click_Printing();
}

function getIntersectionCircleLine(x1, y1, x2, y2, Cx, Cy, Cr, arrCollision) {
    require(["esri/geometry/mathUtils", "esri/geometry/Point", "esri/SpatialReference",
    "esri/map", "esri/graphic", "dojo/_base/connect", "esri/Color", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol"],
    function (mathUtils, Point, SpatialReference, Map, Graphic, connect, Color, SimpleMarkerSymbol, SimpleLineSymbol) {


        // compute the euclidean distance between A and B
        var LAB = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));

        // compute the direction vector D from A to B
        var Dx = (x2 - x1) / LAB;
        var Dy = (y2 - y1) / LAB;

        // compute the value t of the closest point to the circle center (Cx, Cy)
        var t = Dx * (Cx - x1) + Dy * (Cy - y1);

        // This is the projection of C on the line from A to B.

        // compute the coordinates of the point E on line and closest to C
        var Ex = (t * Dx) + x1;
        var Ey = (t * Dy) + y1;

        // compute the euclidean distance from E to C
        var LEC = Math.sqrt(Math.pow((Ex - Cx), 2) + Math.pow((Ey - Cy), 2));

        // test if the line intersects the circle
        if (LEC < Cr) {
            // compute distance from t to circle intersection point
            var dt = Math.sqrt(Math.pow(Cr, 2) - Math.pow(LEC, 2));

            // compute first intersection point
            var Fx = (t - dt) * Dx + x1;
            var Fy = (t - dt) * Dy + y1;
            var F = new Point(Fx, Fy, new SpatialReference({ wkid: 32181 }));


            // compute second intersection point
            var Gx = (t + dt) * Dx + x1;
            var Gy = (t + dt) * Dy + y1;
            var G = new Point(Gx, Gy, new SpatialReference({ wkid: 32181 }));


            //var symbolInters = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CROSS, 12, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 0, 0, 255]), 2), new Color([0, 0, 0, 255]));
            //var GraphicPointsInters1 = new Graphic(F, symbolInters);
            //var GraphicPointsInters2 = new Graphic(G, symbolInters);


            if (isPointBelongSegment(x1, y1, x2, y2, Fx, Fy)) {
                //gLayerRetrievingParcels.add(GraphicPointsInters1);
                arrCollision.push({ x: Fx, y: Fy });
                
            }
            if (isPointBelongSegment(x1, y1, x2, y2, Gx, Gy)) {
                //gLayerRetrievingParcels.add(GraphicPointsInters2);
                arrCollision.push({ x: Gx, y: Gy });
                
            }
            return arrCollision;
        }
        


    });
}

function getIntersectionCircleLine2(x1, y1, x2, y2, Cx, Cy, Cr) {
    require(["esri/geometry/mathUtils", "esri/geometry/Point", "esri/SpatialReference",
    "esri/map", "esri/graphic", "dojo/_base/connect", "esri/Color", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol"],
    function (mathUtils, Point, SpatialReference, Map, Graphic, connect, Color, SimpleMarkerSymbol, SimpleLineSymbol) {
    
        
        // compute the euclidean distance between A and B
        var LAB = Math.sqrt( Math.pow((x2-x1),2)+Math.pow((y2-y1),2) );

        // compute the direction vector D from A to B
        var Dx = (x2-x1)/LAB;
        var Dy = (y2-y1)/LAB;

        // compute the value t of the closest point to the circle center (Cx, Cy)
        var t = Dx*(Cx-x1) + Dy*(Cy-y1); 

        // This is the projection of C on the line from A to B.

        // compute the coordinates of the point E on line and closest to C
        var Ex = (t*Dx)+x1;
        var Ey = (t*Dy)+y1;

        // compute the euclidean distance from E to C
        var LEC = Math.sqrt( Math.pow((Ex-Cx),2)+Math.pow((Ey-Cy),2) );

        // test if the line intersects the circle
        if( LEC < Cr )
        {
            // compute distance from t to circle intersection point
            var dt = Math.sqrt( Math.pow(Cr,2) - Math.pow(LEC,2));

            // compute first intersection point
            var Fx = (t - dt) * Dx + x1;
            var Fy = (t - dt) * Dy + y1;
            var F = new Point(Fx, Fy, new SpatialReference({ wkid: 32181 }));
            

            // compute second intersection point
            var Gx = (t + dt) * Dx + x1;
            var Gy = (t + dt) * Dy + y1;
            var G = new Point(Gx, Gy, new SpatialReference({ wkid: 32181 }));
            

            var symbolInters = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 1), new Color([255, 255, 255, 0]));
            var GraphicPointsInters1 = new Graphic(F, symbolInters);
            var GraphicPointsInters2 = new Graphic(G, symbolInters);
            if (isPointBelongSegment(x1, y1, x2, y2, Fx, Fy)) {
                gLayerRetrievingParcels.add(GraphicPointsInters1);
                myCollision.push({ x: Fx, y: Fy });
                //return;
            }
            if (isPointBelongSegment(x1, y1, x2, y2, Gx, Gy)) {
                gLayerRetrievingParcels.add(GraphicPointsInters2);
                myCollision.push({ x: Gx, y: Gy });
                //return;
            }
            return;
        }
        else if( LEC == Cr ) { // else test if the line is tangent to circle
            // tangent point to circle is E
            //alert("line is tangent to circle. Tanget point is: X= " + Ex + ", Y=" + Ey);
            var lobibox = Lobibox.notify("error", {
                msg: "We don't find any street close to the property", //Without final point
            });
        }
        else {
            // line doesn't touch circle
            var lobibox = Lobibox.notify("error", {
                msg: "We don't find any street close to the property.",
            });
        }

        
    });
}

function isPointBelongSegment(Lx1, Ly1, Lx2, Ly2, Px, Py) {
    var ab = Math.sqrt(Math.pow((Lx2 - Lx1), 2) + Math.pow((Ly2 - Ly1), 2));
    var ac = Math.sqrt(Math.pow((Lx1 - Px), 2) + Math.pow((Ly1 - Py), 2));
    var bc = Math.sqrt(Math.pow((Lx2 - Px), 2) + Math.pow((Ly2 - Py), 2));

    if ((ac + bc - ab) < Math.abs(0.1))return true;
}

function getQuadrant(xEnd, yEnd, xOri, yOri) {
    var dX = xEnd - xOri;
    var dY = yEnd - yOri;
    if (dY > 0 && dX > 0) return 1;
    if (dY > 0 && dX < 0) return 2;
    if (dY < 0 && dX > 0) return 4;
    if (dY < 0 && dX < 0) return 3;
    if (dY == 0 && dX > 0) return 5;
    if (dY == 0 && dX < 0) return 6;
    if (dY > 0 && dX == 0) return 7;
    if (dY < 0 && dX == 0) return 8;
    if (dY == 0 && dX == 0) return 9;
}

function getMiddlePoint(xEnd, yEnd, xOri, yOri) {
    var middlePoint = [];
    var x = parseFloat((xEnd + xOri) / 2);
    var y = parseFloat((yEnd + yOri) / 2);
    middlePoint.push({ x: x, y: y });
    return middlePoint;
}

/*--------------------------------------+
|       Scenario for Sight Lines        |
+---------------------------------------*/
function getParcelByDataPoint(evt, scenarioSelected) {
    require(["esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters", "esri/geometry/geometryEngine", "esri/graphic", "esri/Color", "esri/symbols/SimpleFillSymbol", "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleMarkerSymbol", "esri/geometry/Point"],
        function (IdentifyTask, IdentifyParameters, geometryEngine, Graphic, Color, SimpleFillSymbol, SimpleLineSymbol, SimpleMarkerSymbol, Point) {

        if ((map.extent.xmax - map.extent.xmin) > 600) {
            $.alert({
                title: 'Too many features',
                content: 'You need more zoom on the map please',
                theme: 'white',
                autoClose: 'confirm|3000',
                confirm: function () {
                }
            });
            return;
        }
        var arrPolygons = [];

        var identifyTask = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_QueryTaskPublic_MapService/MapServer");

        var tolerance = 7 / (map.extent.getWidth() / map.width);

        var identifyParams = new IdentifyParameters();
        identifyParams.tolerance = tolerance;
        identifyParams.returnGeometry = true;
        identifyParams.layerIds = [1];
        identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
        identifyParams.width = map.width;
        identifyParams.height = map.height;

        identifyParams.geometry = evt.mapPoint;
        identifyParams.mapExtent = map.extent;

        var myDeferredTask = identifyTask.execute(identifyParams, function (idResults) {
            $.each(idResults, function (i, v) { //Getting parcel IDs for each parcel found
                //alert(v.feature.attributes.P_ID);
                //var numVertex = parseInt(v.feature.geometry.rings[0].length);

                var dist = geometryEngine.distance(evt.mapPoint, v.feature.geometry, "meters");
                arrPolygons.push({ fea: v.feature , distanceToMapPoint: dist});
            });

            //We do need some results, otherwise I'll launch a notification
            if (arrPolygons.length > 0) {
                var checkDist = 9999;
                var j;
                for (var i = 0; i < arrPolygons.length; i++) { //Closest parcel
                    if (arrPolygons[i].distanceToMapPoint < checkDist) {
                        checkDist = arrPolygons[i].distanceToMapPoint;
                        j = i;
                        //alert(arrPolygons[i].distanceToMapPoint);
                    }
                }

                //Highlight the parcel
                gLayerDrawingParcels.clear();
                gLayerRetrievingParcels.clear();
                var symbol = new esri.symbol.SimpleFillSymbol()
                    .setColor(new dojo.Color([255, 255, 255, 0]))
                    .setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 255, 255]), 3));

                //this is our closest parcel
                var myPolygonSL = arrPolygons[j].fea;

                //console.log(myPolygonSL);

                var polygonGraphic = new Graphic(myPolygonSL.geometry, symbol);
                //gLayerRetrievingParcels.add(polygonGraphic);
                gLayerDrawingParcels.add(polygonGraphic);

                //Getting the vertex's polygon. I rather to do it here in order to maintain the next function clean for general purposes
                var vertexinPolygon = [];

                var numVertex = parseInt(arrPolygons[j].fea.geometry.rings[0].length);
                for (var i = 0; i < numVertex; i++) {
                    vertexinPolygon.push({ index: i, x: arrPolygons[j].fea.geometry.rings[0][i][0], y: arrPolygons[j].fea.geometry.rings[0][i][1] });
                }

                if (myPolygonSL.geometry.rings.length == 2) {
                    var numVertex2 = parseInt(arrPolygons[j].fea.geometry.rings[1].length);
                    for (var i = 0; i < numVertex2; i++) {
                        vertexinPolygon.push({ index: i, x: arrPolygons[j].fea.geometry.rings[1][i][0], y: arrPolygons[j].fea.geometry.rings[1][i][1] });
                    }
                }
                if (myPolygonSL.geometry.rings.length > 2) { //Geometry has 3 rings. We don't want this
                    var lobibox = Lobibox.notify("error", {
                        msg: "Parcel geometry is too complex. It has more than 2 rings",
                    });
                    gLayerRetrievingParcels.clear();
                    return;
                }

                //var clockwise = arrPolygons[j].fea.geometry.isClockwise(arrPolygons[j].fea.geometry.rings[0]);

                //Draw a perpendicular point. 
                var myIntersection = [];
                myIntersection.push(getPerpendicularIntersectionPointToPolygonByPoint(vertexinPolygon, evt.mapPoint));

                //console.log("Intersection is: " + myIntersection[0].x + "," + myIntersection[0].y + ", Is fall within:" + myIntersection[0].isInSegment + ", Distance:" + myIntersection[0].distance);
                var myIntersectionPoint = new Point(myIntersection[0].x, myIntersection[0].y, map.spatialReference);
                /*
                var symbolPointInters = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 7, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 0, 255]), 1), new Color([255, 255, 255, 0.4]));
                var GraphicInters = new Graphic(myIntersectionPoint, symbolPointInters);
                gLayerRetrievingParcels.add(GraphicInters); //Draw the intersection point
                */
                


                //Getting a Set Back distance according to user's choice
                var myRadio = $('input[name=sightlineScenario]');
                var mySelectedsightlineScenario = myRadio.filter(':checked').val();
                switch (mySelectedsightlineScenario) {
                    case "optCurb":
                        var distanceSetback = parseInt($('#curbDistance').val());
                        //Calculating a new point along a line from a point
                        var vx = myIntersection[0].x - evt.mapPoint.x;
                        var vy = myIntersection[0].y - evt.mapPoint.y;
                        var factor = distanceSetback / Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2));
                        vx *= factor;
                        vy *= factor;
                        var px = evt.mapPoint.x + vx;
                        var py = evt.mapPoint.y + vy;

                        break;
                    case "optEndSideWalk":
                        var distanceSetback = 3.5;
                        //Calculating a new point along a line from a point
                        var vx = myIntersection[0].x - evt.mapPoint.x;
                        var vy = myIntersection[0].y - evt.mapPoint.y;
                        var factor = distanceSetback / Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2));
                        vx *= factor;
                        vy *= factor;
                        var px = evt.mapPoint.x + vx;
                        var py = evt.mapPoint.y + vy;

                        break;
                    case "optProperty":
                        var distanceSetback = 2.5;
                        //Calculating a new point along a line from a point
                        var vx = myIntersection[0].x - evt.mapPoint.x;
                        var vy = myIntersection[0].y - evt.mapPoint.y;
                        var factor = distanceSetback / Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2));
                        vx *= factor;
                        vy *= factor;
                        var px = myIntersection[0].x + vx;
                        var py = myIntersection[0].y + vy;

                        break;
                }

                //Check if the new point is inside the parcel
                var isInside = getMapPointIsInsidePolygon(vertexinPolygon, evt.mapPoint); //Using the Jordan curve theorem
                if (isInside) { //We need to invert this point using a simmetry
                    var newSymmetricPoint = getSymmetricPoint(px, py, myIntersection[0].x, myIntersection[0].y);
                    var myKeyPoint = new Point(newSymmetricPoint[0], newSymmetricPoint[1], map.spatialReference);
                }
                else var myKeyPoint = new Point(px, py, map.spatialReference);
                
                /*
                var GraphicInters2 = new Graphic(myKeyPoint, symbolPointInters);
                gLayerRetrievingParcels.add(GraphicInters2); //Draw the intersection point
                */
                

                findCloseAndConnectedStreets(myPolygonSL, myKeyPoint, myIntersectionPoint);

            }
            else {
                gLayerRetrievingParcels.clear();
                var lobibox = Lobibox.notify("warning", {
                    msg: "Too far away from any property.",
                });
            }

        });
    });
}

function findCloseAndConnectedStreets(myPolygonSL, myKeyPoint, myIntersectionPoint) {
    require(["esri/SpatialReference", "esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters", "esri/geometry/geometryEngine", "esri/graphic", "esri/Color", "esri/symbols/SimpleFillSymbol", "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleMarkerSymbol", "esri/geometry/Point", "esri/geometry/Polyline", "dojo/promise/all", "esri/symbols/CartographicLineSymbol"],
        function (SpatialReference, IdentifyTask, IdentifyParameters, geometryEngine, Graphic, Color, SimpleFillSymbol, SimpleLineSymbol, SimpleMarkerSymbol, Point, Polyline, all, CartographicLineSymbol) {
            //Looking for Streets nearby
            var identifyTaskStreets = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_Query_Identify_TaskS_MapService/MapServer");
            var tolerance = 12 / (map.extent.getWidth() / map.width); //12 meters is enought for most purposes

            var identifyParamsStreets = new IdentifyParameters();
            identifyParamsStreets.tolerance = tolerance;
            identifyParamsStreets.returnGeometry = true;
            identifyParamsStreets.layerIds = [2];
            identifyParamsStreets.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
            identifyParamsStreets.width = map.width;
            identifyParamsStreets.height = map.height;
            identifyParamsStreets.geometry = myIntersectionPoint;
            identifyParamsStreets.mapExtent = map.extent;

            var myDeferredTask2 = identifyTaskStreets.execute(identifyParamsStreets, function (idResults) {
                var arrStreetsFound = [];
                
                var myIndexOnStreet;
                $.each(idResults, function (i, v) { //Getting parcel IDs for each parcel found
                    //var attribs = v.feature.attributes;
                    //alert(attribs.STREETNAME);
                    //data.push({ StreetName: attribs.STREETNAME, Classification: attribs.STREETCLS });

                    var dist = geometryEngine.distance(myIntersectionPoint, v.feature.geometry, "meters");
                    arrStreetsFound.push({ fea: v.feature, distanceToMapPoint: dist });
                });
                if (arrStreetsFound.length > 0) {
                    var checkDist = 9999;
                    var j;
                    for (var i = 0; i < arrStreetsFound.length; i++) {
                        if (arrStreetsFound[i].distanceToMapPoint < checkDist) {
                            checkDist = arrStreetsFound[i].distanceToMapPoint;
                            j = i;
                        }
                    }
                    //We got the closest street
                    var myClosestLineStreetSL = arrStreetsFound[j].fea;
                    var myOriginalClosestLineStreetSL = arrStreetsFound[j].fea;

                    //And draw it
                    /*
                    var sls = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255, 0, 0]), 3);
                    var GraphicStreetConnected = new Graphic(myClosestLineStreetSL.geometry, sls);
                    gLayerRetrievingParcels.add(GraphicStreetConnected);
                    */

                    //Get Parallel. Distance is according the street classification
                    var parallelDist;
                    var streetClas = myClosestLineStreetSL.attributes.STREETCLS;
                    if (streetClas == "ART-MA" || streetClas == "ART-MI" || streetClas == "COLLECTOR" || streetClas == "FREEWAY") parallelDist = 3.75;
                    else parallelDist = 3;

                    var myParallel01 = geometryEngine.offset(myClosestLineStreetSL.geometry, parallelDist, "meters", "round");
                    var myParallel02 = geometryEngine.offset(myClosestLineStreetSL.geometry, parallelDist * -1, "meters", "round");
                    
                    var distTemp01 = geometryEngine.distance(myPolygonSL.geometry, myParallel01, "meters");
                    var distTemp02 = geometryEngine.distance(myPolygonSL.geometry, myParallel02, "meters");

                    if (distTemp01 < distTemp02) myClosestLineStreetSL = myParallel01;
                    else myClosestLineStreetSL = myParallel02;


                    //var sls = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255, 0, 0]), 3);
                    //var GraphicStreetParallel = new Graphic(myParallel, sls);
                    //gLayerRetrievingParcels.add(GraphicStreetParallel);


                    //We need the intersection point with our street
                    //Getting the vertex's polyline. I rather to do it here in order to maintain the next function clean for general purposes
                    var vertexinPolyline = [];
                    //var numVertexInPolyline = parseInt(myClosestLineStreetSL.geometry.paths[0].length);
                    var numVertexInPolyline = parseInt(myClosestLineStreetSL.paths[0].length);

                    for (var i = 0; i < numVertexInPolyline; i++) {
                        //vertexinPolyline.push({ index: i, x: myClosestLineStreetSL.geometry.paths[0][i][0], y: myClosestLineStreetSL.geometry.paths[0][i][1] });
                        vertexinPolyline.push({ index: i, x: myClosestLineStreetSL.paths[0][i][0], y: myClosestLineStreetSL.paths[0][i][1] });
                    }
                    var myIntersectionOnStreet = [];
                    myIntersectionOnStreet.push(getPerpendicularIntersectionPointToPolylineByPoint(vertexinPolyline, myIntersectionPoint));

                    //There is a possibility our perpendicular doesn't fall on the street segment
                    if (typeof myIntersectionOnStreet[0] == 'undefined') {
                        var lobibox = Lobibox.notify("error", {
                            msg: "This is not a good street geometry to find the intersection point. Try another nearby location.",
                        });
                        return;
                    }

                    //Draw the new intersection point on the street. This point belongs to the street
                    var symbolPointInters = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 7, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 0, 255]), 1), new Color([255, 255, 255, 0.4]));
                    var myInterPoint = new Point(myIntersectionOnStreet[0].x, myIntersectionOnStreet[0].y, map.spatialReference);
                    /*
                    var GraphicInters3 = new Graphic(myInterPoint, symbolPointInters);
                    gLayerRetrievingParcels.add(GraphicInters3); //Draw the intersection point
                    */


                    //We need to find the distances from the intersection point to each endpoint of the street to check if it is too short.
                    //First thing to do is to find the index where the intersection point falls
                    //var myIndexOnStreet;
                    for (var j = 0; j < numVertexInPolyline - 1; j++) {
                        if (isPointBelongSegment(vertexinPolyline[j].x, vertexinPolyline[j].y, vertexinPolyline[j + 1].x, vertexinPolyline[j + 1].y, myIntersectionOnStreet[0].x, myIntersectionOnStreet[0].y)) {
                            //alert("The index is: " + vertexinPolyline[j].index);
                            myIndexOnStreet = vertexinPolyline[j].index;
                        }
                    }

                    if (typeof myIndexOnStreet == 'undefined') {
                        var lobibox = Lobibox.notify("error", {
                            msg: "We didn't find any street nearby. Probably we need to extend the tolerance",
                        });
                        return;
                    }
                    else {
                        /*----------------------------------------------------------------------------------+
                        |   What distance from the intersection to the FIRST node?. Toward the FIRST node.  |
                        +----------------------------------------------------------------------------------*/
                        var arrStreetsConnectedFirstNode = [];
                        var finishOnFirstNode = false;
                        var distancePointToNode_first = getDistance(vertexinPolyline[myIndexOnStreet].x, vertexinPolyline[myIndexOnStreet].y, myIntersectionOnStreet[0].x, myIntersectionOnStreet[0].y);

                        for (var m = myIndexOnStreet; m > 0; m--) {
                            //console.log("X:" + vertexinPolyline[m].x + "; Y:" + vertexinPolyline[m].y + "; distances:" + parseFloat(Math.sqrt((Math.pow((vertexinPolyline[m].x - vertexinPolyline[m - 1].x), 2)) + (Math.pow((vertexinPolyline[m].y - vertexinPolyline[m - 1].y), 2)))));
                            distancePointToNode_first += getDistance(vertexinPolyline[m].x, vertexinPolyline[m].y, vertexinPolyline[m - 1].x, vertexinPolyline[m - 1].y);
                        }

                        //In this case, we need to find the prolongation of the initial street
                        if ((distancePointToNode_first - 1) < parseInt($('#sightDistance').val())) {
                            //Looking for Streets nearby
                            var identifyTaskStreetsConnectedFirst = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_Query_Identify_TaskS_MapService/MapServer");
                            var toleranceForStreetsConnectedFirst = 3;

                            var identifyParamsStreetsConnectedFirst = new IdentifyParameters();
                            identifyParamsStreetsConnectedFirst.tolerance = toleranceForStreetsConnectedFirst;
                            identifyParamsStreetsConnectedFirst.returnGeometry = true;
                            identifyParamsStreetsConnectedFirst.layerIds = [2];
                            identifyParamsStreetsConnectedFirst.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
                            identifyParamsStreetsConnectedFirst.width = map.width;
                            identifyParamsStreetsConnectedFirst.height = map.height;
                            //var tmpPointStreetsConnectedFirst = new Point(vertexinPolyline[0].x, vertexinPolyline[0].y, map.spatialReference);
                            var tmpPointStreetsConnectedFirst = new Point(myOriginalClosestLineStreetSL.geometry.paths[0][0][0], myOriginalClosestLineStreetSL.geometry.paths[0][0][1], map.spatialReference);
                            
                            identifyParamsStreetsConnectedFirst.geometry = tmpPointStreetsConnectedFirst;
                            identifyParamsStreetsConnectedFirst.mapExtent = map.extent;

                            var myDeferredTaskStreetsConnectedFirst = identifyTaskStreetsConnectedFirst.execute(identifyParamsStreetsConnectedFirst, function (idResults) {
                                var firstNode = false;
                                $.each(idResults, function (i, v) {
                                    var attribs = v.feature.attributes;
                                    if (attribs.STREET_ID != myOriginalClosestLineStreetSL.attributes.STREET_ID) { //We don't have the same streets, so discern by STREET_ID
                                        //We need to know which node the original street is connected to
                                        //alert("num. of vertex: " + v.feature.geometry.paths[0].length);
                                        if (v.feature.geometry.paths[0][0][0].toFixed(2) == tmpPointStreetsConnectedFirst.x.toFixed(2) && v.feature.geometry.paths[0][0][1].toFixed(2) == tmpPointStreetsConnectedFirst.y.toFixed(2)) {
                                            //alert("connected with the first node in the next street");
                                            //We need to treat angles to know if it is a real extension. arguments [first point, central point, second point]
                                            var angleBetweenStreets = getAngleBetween3points(vertexinPolyline[1].x, vertexinPolyline[1].y, tmpPointStreetsConnectedFirst.x, tmpPointStreetsConnectedFirst.y, v.feature.geometry.paths[0][1][0], v.feature.geometry.paths[0][1][1]) * 180 / Math.PI;
                                            //alert(angleBetweenStreets);
                                            if (Math.abs(180 - angleBetweenStreets) < 30) {
                                                arrStreetsConnectedFirstNode.push({ fea: v.feature, indexConnected: 0 }); //Angular tolerance on streets
                                                firstNode = true;
                                            }

                                        }
                                        if (v.feature.geometry.paths[0][v.feature.geometry.paths[0].length - 1][0].toFixed(2) == tmpPointStreetsConnectedFirst.x.toFixed(2) && v.feature.geometry.paths[0][v.feature.geometry.paths[0].length - 1][1].toFixed(2) == tmpPointStreetsConnectedFirst.y.toFixed(2)) {
                                            //alert("connected with the last node in the next street");
                                            var angleBetweenStreets = getAngleBetween3points(vertexinPolyline[1].x, vertexinPolyline[1].y, tmpPointStreetsConnectedFirst.x, tmpPointStreetsConnectedFirst.y, v.feature.geometry.paths[0][v.feature.geometry.paths[0].length - 2][0], v.feature.geometry.paths[0][v.feature.geometry.paths[0].length - 2][1]) * 180 / Math.PI;
                                            //alert(angleBetweenStreets);
                                            if (Math.abs(180 - angleBetweenStreets) < 30) {
                                                arrStreetsConnectedFirstNode.push({ fea: v.feature, indexConnected: v.feature.geometry.paths[0].length - 1 }); //Angular tolerance on streets
                                                firstNode = true;
                                            }

                                        }
                                    }
                                });
                                if (firstNode) {
                                    //alert("Draw the connected streets to the first node");
                                    //Draw the connected streets
                                    /*
                                    var sls2 = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255, 255, 0]), 3);
                                    var GraphicStreetConnectedFirstNode = new Graphic(arrStreetsConnectedFirstNode[0].fea.geometry, sls2);
                                    gLayerRetrievingParcels.add(GraphicStreetConnectedFirstNode);
                                    */


                                    //Check distances again. We are assuming we have just one street connected at 180 degrees +-30
                                    for (var ma = 0; ma < arrStreetsConnectedFirstNode[0].fea.geometry.paths[0].length - 1; ma++) {
                                        distancePointToNode_first += getDistance(arrStreetsConnectedFirstNode[0].fea.geometry.paths[0][ma][0], arrStreetsConnectedFirstNode[0].fea.geometry.paths[0][ma][1], arrStreetsConnectedFirstNode[0].fea.geometry.paths[0][ma + 1][0], arrStreetsConnectedFirstNode[0].fea.geometry.paths[0][ma + 1][1]);
                                    }

                                    //If would need more connections because of total distance would be still short. It would be here the code TO DO
                                    if (distancePointToNode_first < parseInt($('#sightDistance').val())) {
                                        var lobibox = Lobibox.notify("error", {
                                            msg: "Distance counting on the first connected street is still shorter than the sight distance: " + distancePointToNode_first,
                                        });

                                    }
                                }
                                else {
                                    var lobibox = Lobibox.notify("warning", {
                                        msg: "We didn't find any valid connected street on the first node to continue, so the sight distance on one side will be just until the end of the street segment.",
                                    });
                                    finishOnFirstNode = true;
                                }
                            });


                        }

                        /*----------------------------------------------------------------------------------+
                        |   What distance from the intersection to the LAST node?. Toward the LAST node.    |
                        +----------------------------------------------------------------------------------*/
                        var arrStreetsConnectedLastNode = [];
                        var finishOnLastNode = false;
                        myIndexOnStreet = myIndexOnStreet + 1;
                        var distancePointToNode_last = getDistance(vertexinPolyline[myIndexOnStreet].x, vertexinPolyline[myIndexOnStreet].y, myIntersectionOnStreet[0].x, myIntersectionOnStreet[0].y);
                        //alert(distancePointToNode_last);
                        for (var m = myIndexOnStreet; m < vertexinPolyline.length - 1; m++) {
                            //console.log("X:" + vertexinPolyline[m].x + "; Y:" + vertexinPolyline[m].y + "; distances:" + parseFloat(Math.sqrt((Math.pow((vertexinPolyline[m].x - vertexinPolyline[m - 1].x), 2)) + (Math.pow((vertexinPolyline[m].y - vertexinPolyline[m - 1].y), 2)))));
                            distancePointToNode_last += getDistance(vertexinPolyline[m].x, vertexinPolyline[m].y, vertexinPolyline[m + 1].x, vertexinPolyline[m + 1].y);
                        }
                        //alert(distancePointToNode_last);


                        //In this case, we need to find the prolongation of the initial street
                        if ((distancePointToNode_last - 1) < parseInt($('#sightDistance').val())) {
                            //alert("Looking for Streets nearby to the last node");
                            //Looking for Streets nearby
                            var identifyTaskStreetsConnectedLast = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_Query_Identify_TaskS_MapService/MapServer");
                            var toleranceForStreetsConnectedLast = 3;

                            var identifyParamsStreetsConnectedLast = new IdentifyParameters();
                            identifyParamsStreetsConnectedLast.tolerance = toleranceForStreetsConnectedLast;
                            identifyParamsStreetsConnectedLast.returnGeometry = true;
                            identifyParamsStreetsConnectedLast.layerIds = [2];
                            identifyParamsStreetsConnectedLast.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
                            identifyParamsStreetsConnectedLast.width = map.width;
                            identifyParamsStreetsConnectedLast.height = map.height;
                            //var tmpPointStreetsConnectedLast = new Point(vertexinPolyline[vertexinPolyline.length - 1].x, vertexinPolyline[vertexinPolyline.length - 1].y, map.spatialReference);
                            var tmpPointStreetsConnectedLast = new Point(myOriginalClosestLineStreetSL.geometry.paths[0][myOriginalClosestLineStreetSL.geometry.paths[0].length - 1][0], myOriginalClosestLineStreetSL.geometry.paths[0][myOriginalClosestLineStreetSL.geometry.paths[0].length - 1][1], map.spatialReference);
                            identifyParamsStreetsConnectedLast.geometry = tmpPointStreetsConnectedLast;
                            identifyParamsStreetsConnectedLast.mapExtent = map.extent;

                            var myDeferredTaskStreetsConnectedLast = identifyTaskStreetsConnectedLast.execute(identifyParamsStreetsConnectedLast, function (idResults) {
                                var secondNode = false;
                                $.each(idResults, function (i, v) { //Getting parcel IDs for each parcel found
                                    var attribs = v.feature.attributes;
                                    if (attribs.STREET_ID != myOriginalClosestLineStreetSL.attributes.STREET_ID) { //We don't have the same streets, so discern by STREET_ID
                                        //We need to know which node the original street is connected to
                                        //alert("num. of vertex: " + v.feature.geometry.paths[0].length);
                                        if (v.feature.geometry.paths[0][0][0].toFixed(2) == tmpPointStreetsConnectedLast.x.toFixed(2) && v.feature.geometry.paths[0][0][1].toFixed(2) == tmpPointStreetsConnectedLast.y.toFixed(2)) {
                                            //alert("connected with the first node in the next street");
                                            //We need to treat angles to know if it is a real extension. arguments [first point, central point, second point]
                                            var angleBetweenStreets = getAngleBetween3points(vertexinPolyline[vertexinPolyline.length - 2].x, vertexinPolyline[vertexinPolyline.length - 2].y, tmpPointStreetsConnectedLast.x, tmpPointStreetsConnectedLast.y, v.feature.geometry.paths[0][1][0], v.feature.geometry.paths[0][1][1]) * 180 / Math.PI;
                                            //alert(angleBetweenStreets);
                                            if (Math.abs(180 - angleBetweenStreets) < 30) {
                                                arrStreetsConnectedLastNode.push({ fea: v.feature, indexConnected: 0 }); //Angular tolerance on streets
                                                secondNode = true;
                                            }
                                        }
                                        if (v.feature.geometry.paths[0][v.feature.geometry.paths[0].length - 1][0].toFixed(2) == tmpPointStreetsConnectedLast.x.toFixed(2) && v.feature.geometry.paths[0][v.feature.geometry.paths[0].length - 1][1].toFixed(2) == tmpPointStreetsConnectedLast.y.toFixed(2)) {
                                            //alert("connected with the last node in the next street");
                                            var angleBetweenStreets = getAngleBetween3points(vertexinPolyline[vertexinPolyline.length - 2].x, vertexinPolyline[vertexinPolyline.length - 2].y, tmpPointStreetsConnectedLast.x, tmpPointStreetsConnectedLast.y, v.feature.geometry.paths[0][v.feature.geometry.paths[0].length - 2][0], v.feature.geometry.paths[0][v.feature.geometry.paths[0].length - 2][1]) * 180 / Math.PI;
                                            //alert(angleBetweenStreets);
                                            if (Math.abs(180 - angleBetweenStreets) < 30) {
                                                arrStreetsConnectedLastNode.push({ fea: v.feature, indexConnected: v.feature.geometry.paths[0].length - 1 }); //Angular tolerance on streets
                                                secondNode = true;
                                            }

                                        }
                                    }
                                });

                                if (secondNode) {
                                    //alert("Draw the connected streets on the last node");
                                    //alert(arrStreetsConnectedLastNode.length);
                                    //Draw the connected streets
                                    /*
                                    var sls3 = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([0, 255, 0]), 3);
                                    var GraphicStreetConnectedLast = new Graphic(arrStreetsConnectedLastNode[0].fea.geometry, sls3);
                                    gLayerRetrievingParcels.add(GraphicStreetConnectedLast);
                                    */


                                    //Check distances again. We are assuming we have just one street connected at 180 degrees +-10
                                    for (var mb = 0; mb < arrStreetsConnectedLastNode[0].fea.geometry.paths[0].length - 1; mb++) {
                                        distancePointToNode_last += getDistance(arrStreetsConnectedLastNode[0].fea.geometry.paths[0][mb][0], arrStreetsConnectedLastNode[0].fea.geometry.paths[0][mb][1], arrStreetsConnectedLastNode[0].fea.geometry.paths[0][mb + 1][0], arrStreetsConnectedLastNode[0].fea.geometry.paths[0][mb + 1][1]);
                                    }

                                    //alert("New distance is: " + distancePointToNode_last);

                                    //If would need more connections because of total distance would be still short. It would be here the code TO DO
                                    if (distancePointToNode_last < parseInt($('#sightDistance').val())) {
                                        var lobibox = Lobibox.notify("error", {
                                            msg: "Distance counting on the second connected street is shorter than the sight distance: " + distancePointToNode_last,
                                        });
                                        return;
                                    }
                                }
                                else {
                                    var lobibox = Lobibox.notify("warning", {
                                        msg: "We didn't find any valid connected street on the last node to continue, so the sight distance on one side will be just until the end of the street segment.",
                                    });
                                    //
                                    finishOnLastNode = true;
                                }

                            });
                        }
                        /*------------------------------------------------------------------+
                        |       We need to wait here until every deferred task finishs      |
                        +------------------------------------------------------------------*/
                        var promisesSL = new all([myDeferredTaskStreetsConnectedFirst, myDeferredTaskStreetsConnectedLast]);
                        promisesSL.then(function (results) {
                            //Using our arrays of streets. Build a new polyline
                            var newStreetPolyline = new Polyline(new SpatialReference({ wkid: 32181 }));

                            //alert(arrStreetsConnectedFirstNode.length);
                            //alert(arrStreetsConnectedLastNode.length);
                            if (arrStreetsConnectedFirstNode.length > 0) {
                                var myParallel03 = geometryEngine.offset(arrStreetsConnectedFirstNode[0].fea.geometry, parallelDist, "meters", "round");
                                var myParallel04 = geometryEngine.offset(arrStreetsConnectedFirstNode[0].fea.geometry, parallelDist * -1, "meters", "round");
                                var distTemp03 = geometryEngine.distance(myPolygonSL.geometry, myParallel03, "meters");
                                var distTemp04 = geometryEngine.distance(myPolygonSL.geometry, myParallel04, "meters");
                                var connectedStreetFirstNode;
                                if (distTemp03 < distTemp04) connectedStreetFirstNode = myParallel03;
                                else connectedStreetFirstNode = myParallel04;
                            }

                            if (arrStreetsConnectedLastNode.length > 0) {
                                var myParallel05 = geometryEngine.offset(arrStreetsConnectedLastNode[0].fea.geometry, parallelDist, "meters", "round");
                                var myParallel06 = geometryEngine.offset(arrStreetsConnectedLastNode[0].fea.geometry, parallelDist * -1, "meters", "round");
                                var distTemp05 = geometryEngine.distance(myPolygonSL.geometry, myParallel05, "meters");
                                var distTemp06 = geometryEngine.distance(myPolygonSL.geometry, myParallel06, "meters");
                                var connectedStreetLastNode;
                                if (distTemp05 < distTemp06) connectedStreetLastNode = myParallel05;
                                else connectedStreetLastNode = myParallel06;
                            }

                            switch (arrStreetsConnectedFirstNode.length) {
                                case 0:
                                    switch (arrStreetsConnectedLastNode.length) {
                                        case 0:
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            break;
                                        case 1:
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            newStreetPolyline.addPath(connectedStreetLastNode.paths[0]);
                                            break;
                                        case 2, 3, 4, 5, 6:
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            newStreetPolyline.addPath(connectedStreetLastNode.paths[0]);
                                            alert("TO DO. Case 3");
                                            break;
                                    }
                                    break;
                                case 1:
                                    switch (arrStreetsConnectedLastNode.length) {
                                        case 0:
                                            newStreetPolyline.addPath(connectedStreetFirstNode.paths[0]);
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            break;
                                        case 1:
                                            newStreetPolyline.addPath(connectedStreetFirstNode.paths[0]);
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            newStreetPolyline.addPath(connectedStreetLastNode.paths[0]);
                                            break;
                                        case 2, 3, 4, 5, 6:
                                            newStreetPolyline.addPath(connectedStreetFirstNode.paths[0]);
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            alert("TO DO. Case 6");
                                            break;
                                    }
                                    break;
                                case 2, 3, 4, 5, 6:
                                    switch (arrStreetsConnectedLastNode.length) {
                                        case 0:
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            alert("TO DO. Case 7");
                                            break;
                                        case 1:
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            newStreetPolyline.addPath(connectedStreetLastNode.paths[0]);
                                            alert("TO DO. Case 8");
                                            break;
                                        case 2, 3, 4, 5, 6:
                                            newStreetPolyline.addPath(myClosestLineStreetSL.paths[0]);
                                            alert("TO DO. Case 9");
                                            break;
                                    }
                                    break;
                            }


                            //Draw the new street
                            //var symbolLineForStreet = new CartographicLineSymbol(CartographicLineSymbol.STYLE_SOLID, new Color([255, 0, 0, 0.2]), 10, CartographicLineSymbol.CAP_ROUND, CartographicLineSymbol.JOIN_ROUND, 5);
                            //var GraphicLineForStreet = new Graphic(newStreetPolyline, symbolLineForStreet);
                            //gLayerRetrievingParcels.add(GraphicLineForStreet);
                            //gLayerDrawingParcels.add(GraphicLineForStreet);

                            var arrCollision = [];
                            for (var t = 0; t < myClosestLineStreetSL.paths[0].length - 1; t++) {
                                arrCollision.push(getIntersectionCircleLine(myClosestLineStreetSL.paths[0][t][0], myClosestLineStreetSL.paths[0][t][1], myClosestLineStreetSL.paths[0][t + 1][0], myClosestLineStreetSL.paths[0][t + 1][1], myKeyPoint.x, myKeyPoint.y, parseInt($('#sightDistance').val()), arrCollision));
                            }

                            if (arrStreetsConnectedFirstNode.length > 0) { //The original street has some street connected to its first node
                                for (var t = 0; t < connectedStreetFirstNode.paths[0].length - 1; t++) {
                                    arrCollision.push(getIntersectionCircleLine(connectedStreetFirstNode.paths[0][t][0], connectedStreetFirstNode.paths[0][t][1], connectedStreetFirstNode.paths[0][t + 1][0], connectedStreetFirstNode.paths[0][t + 1][1], myKeyPoint.x, myKeyPoint.y, parseInt($('#sightDistance').val()), arrCollision));
                                }
                            }
                            else if (finishOnFirstNode) {
                                arrCollision.push({ x: myClosestLineStreetSL.paths[0][0][0], y: myClosestLineStreetSL.paths[0][0][1] });
                            }

                            if (arrStreetsConnectedLastNode.length > 0) {
                                for (var t = 0; t < connectedStreetLastNode.paths[0].length - 1; t++) {
                                    arrCollision.push(getIntersectionCircleLine(connectedStreetLastNode.paths[0][t][0], connectedStreetLastNode.paths[0][t][1], connectedStreetLastNode.paths[0][t + 1][0], connectedStreetLastNode.paths[0][t + 1][1], myKeyPoint.x, myKeyPoint.y, parseInt($('#sightDistance').val()), arrCollision));
                                }
                            }
                            else if (finishOnLastNode) {
                                arrCollision.push({ x: myClosestLineStreetSL.paths[0][myClosestLineStreetSL.paths[0].length - 1][0], y: myClosestLineStreetSL.paths[0][myClosestLineStreetSL.paths[0].length - 1][1] });
                            }

                            //console.log(arrCollision);
                            //get the results
                            var symbolForSightLines = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 255]), 2)
                            //Finally, Draw Sight Lines
                            
                            var GraphicLineForSightLines;
                            for (var tt = 0; tt < arrCollision.length; tt++) {
                                if (typeof arrCollision[tt] != 'undefined') {
                                    var newSightLinePolyline = new Polyline(new SpatialReference({ wkid: 32181 }));
                                    newSightLinePolyline.addPath([[myKeyPoint.x, myKeyPoint.y], [arrCollision[tt].x, arrCollision[tt].y]]);
                                    GraphicLineForSightLines = new Graphic(newSightLinePolyline, symbolForSightLines);
                                    gLayerRetrievingParcels.add(GraphicLineForSightLines);
                                    
                                    var symbolInters = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CROSS, 12, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 0, 0, 255]), 2), new Color([0, 0, 0, 255]));
                                    var myCross = new Point([arrCollision[tt].x, arrCollision[tt].y], new SpatialReference({ wkid: 32181 }));
                                    var GraphicCross = new Graphic(myCross, symbolInters);
                                    gLayerRetrievingParcels.add(GraphicCross);
                                    
                                    //gLayerDrawingParcels.add(GraphicLineForSightLines);
                                    //featureLayerForPrinting.add(GraphicLineForSightLines);
                                }
                            }
                            
                            //Finish the waiting icon
                            esri.hide(loading);

                            //Creating the button
                            $('#btnDownloadTriangle').remove();

                            $('#divStepsInspectionServices')
                                .append("<button id='btnDownloadTriangle' type='button' class='btn btn-default' style='width: 100%; margin-top:10px'>Print it!</button>");
                            //Adding a click event
                            $("#btnDownloadTriangle").click(printTriangle);

                        });
                    }
                }
                else {
                    var lobibox = Lobibox.notify("error", {
                        msg: "We didn't find any street nearby.",
                    });
                    gLayerRetrievingParcels.clear();
                    return;
                }
            });
        });
}

function getAngleBetween3points(Ax,Ay,Bx,By,Cx,Cy) {
    var AB = Math.sqrt(Math.pow(Bx - Ax, 2) + Math.pow(By - Ay, 2));
    var BC = Math.sqrt(Math.pow(Bx - Cx, 2) + Math.pow(By - Cy, 2));
    var AC = Math.sqrt(Math.pow(Cx - Ax, 2) + Math.pow(Cy - Ay, 2));
    return Math.acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB));
}

function getDistance(x1, y1, x2, y2) {
    return parseFloat(Math.sqrt((Math.pow((x1 - x2), 2)) + (Math.pow((y1 - y2), 2))));
}

function getSymmetricPoint(Ax, Ay, Mx, My) {
    var symmetricPoint = new Array();
    symmetricPoint[0] = 2 * Mx - Ax;
    symmetricPoint[1] = 2 * My - Ay;
    return symmetricPoint;
}

function getPerpendicularIntersectionPointToPolylineByPoint(vertexPolyline, intersectionPoint) {
    var tmpIntersectionPoints = [];
    var intersectionPoints = [];
    for (var i = 0; i < vertexPolyline.length - 1; i++) {
        var xOri = vertexPolyline[i].x;
        var yOri = vertexPolyline[i].y;
        var xEnd = vertexPolyline[i + 1].x;
        var yEnd = vertexPolyline[i + 1].y;
        var xPoint = intersectionPoint.x;
        var yPoint = intersectionPoint.y;

        tmpIntersectionPoints.push(getPerpendicularIntersectionPointToLineByPoint(xOri, yOri, xEnd, yEnd, xPoint, yPoint));

    }
    for (var j = 0; j < tmpIntersectionPoints.length; j++) {
        if (tmpIntersectionPoints[j][0].isInSegment === true)
            intersectionPoints.push({ x: tmpIntersectionPoints[j][0].x, y: tmpIntersectionPoints[j][0].y, isInSegment: tmpIntersectionPoints[j][0].isInSegment, distance: tmpIntersectionPoints[j][0].distance });
    }

    //Sorting using a key word
    Array.prototype.sortOn = function (key) {
        this.sort(function (a, b) {
            if (a[key] < b[key]) {
                return -1;
            } else if (a[key] > b[key]) {
                return 1;
            }
            return 0;
        });
    }

    intersectionPoints.sortOn('distance');

    return intersectionPoints[0];
}

function getPerpendicularIntersectionPointToPolygonByPoint(vertexPolygon, mapPoint) {
    var tmpIntersectionPoints = [];
    var intersectionPoints = [];
    for (var i = 0; i < vertexPolygon.length-1;i++) {
        var xOri = vertexPolygon[i].x;
        var yOri = vertexPolygon[i].y;
        var xEnd = vertexPolygon[i + 1].x;
        var yEnd = vertexPolygon[i + 1].y;
        var xPoint = mapPoint.x;
        var yPoint = mapPoint.y;
        
        tmpIntersectionPoints.push(getPerpendicularIntersectionPointToLineByPoint(xOri, yOri, xEnd, yEnd, xPoint, yPoint));

    }
    for (var j = 0; j < tmpIntersectionPoints.length; j++) {
        if (tmpIntersectionPoints[j][0].isInSegment === true)
            intersectionPoints.push({x: tmpIntersectionPoints[j][0].x, y: tmpIntersectionPoints[j][0].y, isInSegment: tmpIntersectionPoints[j][0].isInSegment ,distance: tmpIntersectionPoints[j][0].distance});
    }

    //Sorting using a key word
    Array.prototype.sortOn = function (key) {
        this.sort(function (a, b) {
            if (a[key] < b[key]) {
                return -1;
            } else if (a[key] > b[key]) {
                return 1;
            }
            return 0;
        });
    }

    intersectionPoints.sortOn('distance');

    return intersectionPoints[0];
}

function getPerpendicularIntersectionPointToLineByPoint(xOri, yOri, xEnd, yEnd, xPoint, yPoint) {
    var myIntersectionPoints = [];
    var LineMag = parseFloat(Math.sqrt((Math.pow((xEnd - xOri), 2)) + (Math.pow((yEnd - yOri), 2))));
    if(LineMag === 0) return myIntersectionPoints; //P1 and P2 are coincident

    var U = (((xPoint - xOri) * (xEnd - xOri)) + ((yPoint - yOri) * (yEnd - yOri))) / (LineMag * LineMag);

    var tmpX = xOri + U * ( xEnd - xOri );
    var tmpY = yOri + U * (yEnd - yOri);

    if (U < 0 || U > 1)
        var tmpFallWithin = false;   // closest point does not fall within the line segment
    else var tmpFallWithin = true;

    var tmpDistance = parseFloat(Math.sqrt((Math.pow((tmpX - xPoint), 2)) + (Math.pow((tmpY - yPoint), 2))));

    myIntersectionPoints.push({ x: tmpX, y: tmpY, isInSegment: tmpFallWithin, distance: tmpDistance });
    
    //console.log("Intersection is: " + myIntersectionPoints[0].x + "," + myIntersectionPoints[0].y + ", Is fall within:" + myIntersectionPoints[0].isInSegment + ", Distance:" + myIntersectionPoints[0].distance);

    return myIntersectionPoints;
}

function getMapPointIsInsidePolygon(vertexPolygon, mapPoint) {
    var inside = false;
    for (var i = 0, j = vertexPolygon.length - 1; i < vertexPolygon.length; j = i++) {
        var xi = vertexPolygon[i].x, yi = vertexPolygon[i].y;
        var xj = vertexPolygon[j].x, yj = vertexPolygon[j].y;

        var intersect = ((yi > mapPoint.y) != (yj > mapPoint.y))
            && (mapPoint.x < (xj - xi) * (mapPoint.y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }

    return inside;
}

function startNextStepSL(step) {
    var msg;
    //Loop for each element in order to remove them
    var myNode = document.getElementById('divStepsInspectionServices');
    while (myNode.lastChild) {
        myNode.removeChild(myNode.lastChild);
    }

    switch (myOption) {
        case "opSightLine":
            switch (step) {
                case "2":
                    msg = step + ".- Please, indicate the segments that will intersect to make the corner";
                    $('#divStepsInspectionServices')
                        .append("<label id='lblStepInspectionServices'>" + msg + "</label>");


                    $('#divStepsInspectionServices')
                        .append("<button id='btnSightLine' type='button' class='btn btn-default' style='width: 25%; float: right'>Go</button>");

                    $("#btnSightLine").click({ param1: vertex, param2: clockwise }, getIntersection);

                    break;
            }

    }

}


//Changing input coordinates according its type
$("#selectTypeSightLine").change(function () {
    mode = "sightline";
    gLayerRetrievingParcels.clear();
    //Loop for each element in order to remove them
    $('#divStepsInspectionServices').each(function () {
        $('#divStepsInspectionServices').children($(this).id).remove();
    });

    var opt = this.value;

    switch (opt) {
        case "opCornerTriangle":
            $('#divStepsInspectionServices')
                .append("<label id='lblStepInspectionServices'>1.- Data point inside the property</label>");
            break;

        case "opSightLine":
            $('#divStepsInspectionServices')
                .append("<label id='lblStepInspectionServices'>1.- Please, according the image and following your own criteria, choose the best scenario possible for your next data point</label>");

            var divTableRow1 = $("<div id='divTable1SightLine'></div>");
            $('#divStepsInspectionServices').append(divTableRow1);
            divTableRow1.css('display', 'table-row');
            
            var divTableCellLeft = $("<div id='divTableCellLeftSightLine'></div>");
            $('#divTable1SightLine').append(divTableCellLeft);
            divTableCellLeft.css('display', 'table-cell');

            var divTableCellRight = $("<div id='divTableCellRightSightLine'></div>");
            $('#divTable1SightLine').append(divTableCellRight);
            divTableCellRight.css('width', '50%');
            divTableCellRight.css('display', 'table-cell');
            divTableCellRight.css('padding-left', '15px');

            var divTableRow2 = $("<div id='divTableRow2SightLine'></div>");
            $('#divStepsInspectionServices').append(divTableRow2);
            divTableRow2.css('display', 'table-row');

            var divTableRow2CellLeft = $("<div id='divTableRow2CellLeftSightLine'></div>");
            $('#divTableRow2SightLine').append(divTableRow2CellLeft);
            divTableRow2CellLeft.css('display', 'table-cell');
            //$('#divTableRow2CellLeftSightLine').append("fdgdfgdfg");

            var divTableRow2CellRight = $("<div id='divTableRow2CellRightSightLine'></div>");
            $('#divTableRow2SightLine').append(divTableRow2CellRight);
            divTableRow2CellRight.css('width', '50%');
            divTableRow2CellRight.css('display', 'table-cell');
            divTableRow2CellRight.css('padding-left', '15px');
            //divTableRow2CellRight.css('float', 'right');
            //$('#divTableRow2CellRightSightLine').append("1234");
            
            
            var myInputs = $("<input type='radio' name='sightlineScenario' value='optCurb'> Curb<br> \
                              <input type='radio' name='sightlineScenario' value='optEndSideWalk'> End of the sidewalk<br> \
                              <input type='radio' name='sightlineScenario' value='optProperty'> Line on the property<br>");
            $('#divTableCellLeftSightLine').append(myInputs);
            
            $('input[name=sightlineScenario]').click(function () {
                var opt2 = this.value;
                
                //Loop for each element in order to remove them. Right and botton area
                var myNode = document.getElementById('divTableCellRightSightLine');
                while (myNode.lastChild) {
                    myNode.removeChild(myNode.lastChild);
                }

                var myNode2 = document.getElementById('divTableRow2CellLeftSightLine');
                while (myNode2.lastChild) {
                    myNode2.removeChild(myNode2.lastChild);
                }

                var myNode3 = document.getElementById('divTableRow2CellRightSightLine');
                while (myNode3.lastChild) {
                    myNode3.removeChild(myNode3.lastChild);
                }
                

                divTableCellRight.css('border-left', 'solid thin');

                switch (opt2) {
                    case "optCurb":
                        var elem0 = $("<p>Please, choose a distance from the face of curb and the sight distance too</p>");
                        $('#divTableCellRightSightLine').append(elem0);

                        var options = '';
                        options = "<option value='4'>4 meters</option>";
                        options += "<option value='6'>6 meters</option>";

                        var elem1 = "<label for='curbDistance'>Curb distance</label><select id='curbDistance' class='input-sm'></select>";
                        $('#divTableRow2CellLeftSightLine').append(elem1);
                        $('#curbDistance').html(options);

                        var options2 = '';
                        options2 = "<option value='45'>45 meters</option>";
                        options2 += "<option value='65'>65 meters</option>";

                        var elem2 = "<label for='sightDistance'>Sight distance</label><select id='sightDistance' class='input-sm'></select>";
                        $('#divTableRow2CellRightSightLine').append(elem2);
                        $('#sightDistance').html(options2);
                        
                        break;
                    case "optEndSideWalk":
                        var elem0 = $("<p>It'll take <b>1 m.</b> behind the sidewalk. Please just choose the sight distance</p>");
                        $('#divTableCellRightSightLine').append(elem0);
                        
                        var options2 = '';
                        options2 = "<option value='45'>45 meters</option>";
                        options2 += "<option value='65'>65 meters</option>";

                        var elem2 = "<label for='sightDistance'>Sight distance</label><select id='sightDistance' class='input-sm'></select>";
                        $('#divTableRow2CellRightSightLine').append(elem2);
                        $('#sightDistance').html(options2);
                        
                        break;
                    case "optProperty":
                        var elem0 = $("<p>It'll take <b>2,5 m.</b> from the property line. Please just choose the sight distance</p>");
                        $('#divTableCellRightSightLine').append(elem0);

                        var options2 = '';
                        options2 = "<option value='45'>45 meters</option>";
                        options2 += "<option value='65'>65 meters</option>";

                        var elem2 = "<label for='sightDistance'>Sight distance</label><select id='sightDistance' class='input-sm'></select>";
                        $('#divTableRow2CellRightSightLine').append(elem2);
                        $('#sightDistance').html(options2);
                        
                        break;
                }
                
            });

            break;
    }
});
