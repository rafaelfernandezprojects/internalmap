/*------------------------------------------------------+
 |  Writing more code here for minimizing index.html    |
 |  Javascript on CSJ ------ MAPCENTRE Application      |
 +-----------------------------------------------------*/
//Global here

/*-------------------------------------+
 |       Tracking Activity FUNCTION    |
 +------------------------------------*/
function printActivity(tool){
    var myUUID = sessionStorage.getItem("uuid");
    var myActiviy = myUUID + "_" + tool;
    $.ajax({
        url: "http://listestsrv.stjohnstest.com/mapcentre_testing/HtmlAppTracking/MapCentreHTMLTracking.asmx/trackActivity",
        type: "POST",
        data: {toolActivity: myActiviy},
        timeout: 1000,
        error: function (a, b, c) {
            console.log("Unable to complete AJAX function (trackActivity).");
        }
    });
}

