/*------------------------------------------------------+
 |  Writing more code here for minimizing index.html    |
 |  Javascript on CSJ ------ MAPCENTRE Application      |
 +-----------------------------------------------------*/
//Global here

function click_liTemplate(TypeTemplate){
    require([
            "esri/map", "esri/geometry/Point", "dojo/domReady!"],
    function(Map, Point) {

        var myLi = document.getElementById(TypeTemplate);
        var myRadio = $(myLi).children("input[type='radio']:first");
        myRadio.prop("checked", true);

        //var myOpt = $.trim($(this).text()); //Removing blanks
        var inPoint = new esri.geometry.Point([325784, 5268883] , map.spatialReference);

        switch (TypeTemplate)
        {
            case "topomap_zoning":
                topo.show();
                ortho2015.hide();
                ortho2013.hide();
                ortho2010.hide();
                ortho2006.hide();
                ortho2003.hide();
                DEM.hide();
                CityLabelsImagery.hide();
                Property.hide();
                Annotation.hide();
                Address_Annotation.hide();
                CityBounds.show();
                Elevation.hide();
                wards.hide();
                planning.show();
                IM360.hide();
                watersheds.hide();
                sewer.hide();
                waterServices.hide();
                trafficSigns.hide();
                neighbourhood.hide();

                document.getElementById("elevationDIV").style.display = "none";
                toolManager.set("currentTool", "");
                mode = "";

                //map.centerAndZoom(inPoint, 5);
                break;
            case "topomap_waterServices":
                topo.show();
                ortho2015.hide();
                ortho2013.hide();
                ortho2010.hide();
                ortho2006.hide();
                ortho2003.hide();
                DEM.hide();
                CityLabelsImagery.hide();
                Property.hide();
                Annotation.hide();
                Address_Annotation.hide();
                CityBounds.show();
                Elevation.hide();
                wards.hide();
                planning.hide();
                IM360.hide();
                watersheds.hide();
                sewer.show();
                waterServices.show();
                trafficSigns.hide();
                neighbourhood.hide();

                document.getElementById("elevationDIV").style.display = "none";
                toolManager.set("currentTool", "");
                mode = "";

                //map.centerAndZoom(inPoint, 5);

                imgTopo.style.border = "4px solid black";
                imgOrtho2015.style.border = "1px solid black";
                imgOrtho2013.style.border = "1px solid black";
                imgOrtho2010.style.border = "1px solid black";
                imgOrtho2006.style.border = "1px solid black";
                imgOrtho2003.style.border = "1px solid black";
                //imgDEM.style.border = "1px solid black";

                break;
            case "topomap_DEM_Elevation":
                topo.show();
                topo.refresh();
                ortho2015.hide();
                ortho2013.hide();
                ortho2010.hide();
                ortho2006.hide();
                ortho2003.hide();
                DEM.show();
                CityLabelsImagery.hide();
                Property.hide();
                Annotation.hide();
                Address_Annotation.hide();
                CityBounds.show();
                Elevation.show();
                wards.hide();
                planning.hide();
                IM360.hide();
                watersheds.hide();
                sewer.hide();
                waterServices.hide();
                trafficSigns.hide();
                neighbourhood.hide();

                toolManager.set("currentTool", "DEM");
                mode = "DEM";
                showElevationValues();

                //inPoint = new esri.geometry.Point([324947, 5268502] , map.spatialReference);
                //map.centerAndZoom(inPoint, 2);

                imgTopo.style.border = "1px solid black";
                imgOrtho2015.style.border = "1px solid black";
                imgOrtho2013.style.border = "1px solid black";
                imgOrtho2010.style.border = "1px solid black";
                imgOrtho2006.style.border = "1px solid black";
                imgOrtho2003.style.border = "1px solid black";
                //imgDEM.style.border = "4px solid black";

                break;
            case "ortho2015_Property":
                topo.hide();
                ortho2015.show();
                ortho2013.hide();
                ortho2010.hide();
                ortho2006.hide();
                ortho2003.hide();
                DEM.hide();
                CityLabelsImagery.show();
                Property.show();
                Annotation.show();
                Address_Annotation.show();
                CityBounds.show();
                Elevation.hide();
                wards.hide();
                planning.hide();
                IM360.hide();
                watersheds.hide();
                sewer.hide();
                waterServices.hide();
                trafficSigns.hide();
                neighbourhood.hide();

                //map.centerAndZoom(inPoint, 7);

                document.getElementById("elevationDIV").style.display = "none";
                toolManager.set("currentTool", "");
                mode = "";

                imgTopo.style.border = "1px solid black";
                imgOrtho2015.style.border = "4px solid black";
                imgOrtho2013.style.border = "1px solid black";
                imgOrtho2010.style.border = "1px solid black";
                imgOrtho2006.style.border = "1px solid black";
                imgOrtho2003.style.border = "1px solid black";
                //imgDEM.style.border = "1px solid black";

                break;

        }
        var myWidthScreen = $(window).width();
        myWidthScreen = myWidthScreen + "px";
        $("#map").width(myWidthScreen);
    });
}
/*
$('#myTemplates').on('click','li',function() {
    require([
            "esri/map", "esri/geometry/Point", "dojo/domReady!"],
    function(Map, Point) {
        //$(this).children("input[type=radio]").click();
        var myRadio = $(this).children("input[type='radio']:first");
        myRadio.prop("checked", true);

        var myOpt = $.trim($(this).text()); //Removing blanks
        var inPoint = new esri.geometry.Point([326147, 5269209] , map.spatialReference);

        switch (myOpt)
        {
            case "Topo map + zoning":

                topo.show();
                planning.show();
                map.centerAndZoom(inPoint, 7);
                break;
            case "Topo map + water services":

                break;
        }
    });
});
    */
/*
$('#myTemplates').on('click','li',function() {
    var myRadio = $(this).children("input[type='radio']:first");
    myRadio.prop("checked", true);

    var myOpt = $.trim($(this).text()); //Removing blanks
    var inPoint = new esri.geometry.Point([326147, 5269209] , map.spatialReference);

    switch (myOpt)
    {
        case "Topo map + zoning":
            topo.show();
            planning.show();
            map.centerAndZoom(inPoint, 7);
            break;
        case "Topo map + water services":

            break;
    }
});
    */