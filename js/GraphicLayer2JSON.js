

function GLayer2JSON (myGL, myWKID) {
    var myJSON;
    var numFeat = 0;
    var WKID;
    //Starting a new file
    myJSON = "{'displayFieldName':'','fieldAliases':{'FID':'FID','Id':'Id'},'geometryType':'esriGeometryPolygon','spatialReference':{'wkid':'" + myWKID + "'},'fields':[{'name':'FID','type':'esriFieldTypeOID','alias':'FID'},{'name':'Id','type':'esriFieldTypeInteger','alias':'Id'}],'features':[";

    //Coordinates
    $.each( myGL.graphics, function( key, value ) {
        numFeat++;
        WKID = value.geometry.spatialReference.wkid;
        //For each polygon. START
        myJSON = myJSON + "{'attributes':{'FID':0,'Id':0},'geometry':{'rings':[[";

        var puntos = value.geometry.rings[0];
        puntos.forEach(
            function (value) {
                myJSON = myJSON + "[" + value + "],";
            }
        );
        //For each polygon. END
        //remove comma in the last point
        myJSON = myJSON.substring(0, myJSON.length - 1);
        myJSON = myJSON + "]]}},"; //END POLYGON
    });

    //remove comma in the last feature
    myJSON = myJSON.substring(0, myJSON.length - 1);

    //FINISH FEATURES
    myJSON = myJSON + "]}";

    if(numFeat == 0) return -1;
    if(WKID != myWKID) return WKID;

    //Recording and Save
    var currentdate = new Date($.now());
    var datetime = currentdate.getFullYear() + "/"
        + (currentdate.getMonth()+1)  + "/"
        + currentdate.getDate() + " @ "
        + currentdate.getHours() + "-"
        + currentdate.getMinutes() + "-"
        + currentdate.getSeconds();
    var blob = new Blob([myJSON], {type: "text/plain;charset=utf-8"});
    saveAs(blob, "Parcel_" + datetime + ".json");

    return 0;
}