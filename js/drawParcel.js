/*------------------------------------------------------+
 |  Writing more code here for minimizing index.html    |
 |  Javascript on CSJ ------ MAPCENTRE Application      |
 +-----------------------------------------------------*/
var alreadyPolar = false;
var myPointGraphic = [];
var polygon;
var indexForUndo = [];

/*--------------------------------+
 |       DRAW TOOLBAR FUNCTIONS    |
 +--------------------------------*/
function click_DrawParcel(){ /*Function coming from index.html*/
    require([
        "esri/map"], function(Map) {

        printActivity("DrawParcel");
        document.getElementById("map_layers").style.cursor = "default";

        $('#newrelease').hide();

        $("#dialog-draw-parcel-tool").dialog({
            //height: 300,
            width: 310,
            dialogClass: "alertDialog",
            resizable: false,
            closeOnEscape: false, // disable escape event on dialog
            //buttons: [{ text: "Ok", click: function() { $( this ).dialog( "close" ); } }],
            close: function (event, ui) {
                toolbarDrawParcel.deactivate();
                //toolManager.set("currentTool", "neutral");
                mode = "neutral";
                map.setMapCursor("default");
                map.getLayer("layerDrawingParcels").clear();
                map.getLayer("layerRetrievingParcels").clear();

                //re-initialize the dialog for the next use
                reInitializeDrawDialog();
            },
            modal: false
        })
            .dialogExtend({
                "minimizable" : true,
                "icons" : { "minimize" : "ui-icon-circle-minus" }
            });
        //Clear parcels layer when the dialog appears
        //map.getLayer("layerParcels").clear();
        toolManager.set("currentTool", "drawParcel");
        mode = "drawParcel";

    });
}

function reInitializeDrawDialog() {
    $("#toggleDemo").collapse('hide'); //Collapsing the "Draw using survey" button
    $('#selectTypeCoordinates2').prop('disabled', false);
    $('#selectTypeSystem').prop('disabled', false);
    $("#lblCoordinates2").text("Enter coordinates for the first point:");
    $('#curve').remove();
    $('#drawMyLabel').remove();
    $('#goRadius').remove();
    $('#goArc').remove();
    $('#clockwise').remove();
    $('#tmpStyle01').remove();
    $('#latNS').remove();
    $('#goD').remove();
    $('#labelD').remove();
    $('#goM').remove();
    $('#labelM').remove();
    $('#goS').remove();
    $('#labelS').remove();
    $('#lonEW').remove();
    $('#goDist').remove();


    var element = document.getElementById('selectTypeCoordinates2');
    element.value = "opMetric";

    var myBadgeValue = $("#btnDrawParcel").find("span.badge");
    myBadgeValue.text("1");
    pts = [];
    myPointGraphic = [];
    indexForUndo = [];

    alreadyPolar = false;
    $('#goX_DP').val(null);
    $('#goX_DP').show();
    $('#goY_DP').val(null);
    $('#goY_DP').show();
}

function addGraphic(evt) {
    toolbarDrawParcel.deactivate();

    var symbol = new esri.symbol.SimpleFillSymbol()
        .setColor(new dojo.Color([51, 255, 51, 0.25]))
        .setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,0]), 3));

    var myGraphicParcel = new esri.Graphic(evt.geometry, symbol);
    gLayerDrawingParcels.add(myGraphicParcel);
}


function addRetrieve(evt) {

    toolbarRetrieveParcel.deactivate();

    //var qt = new esri.tasks.QueryTask("http://map.stjohns.ca/ArcGIS/rest/services/MapCentre/STJ_MC_SearchTools_MapService/MapServer/2");
    var qt = new esri.tasks.QueryTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_SearchToolsQueries_MapService/MapServer/2");
    var q = new esri.tasks.Query();
    q.outSpatialReference = {"wkid":32181};
    q.returnGeometry = true;
    q.geometry = evt.geometry;
    q.spatialRelationship = esri.tasks.Query.SPATIAL_REL_INTERSECTS;
    q.geometryType = "esriGeometryPolygon";
    var myDeferredRetrieve = qt.execute(q);
    myDeferredRetrieve.then(esri.show(loading));

    qt.on("complete", function(evt) {
        esri.hide(loading)
        if ((map.extent.xmax - map.extent.xmin) > 600) {
            $.alert({
                title: 'Too many features',
                content: 'You need more zoom on the map please',
                theme: 'white',
                autoClose: 'confirm|3000',
                confirm: function(){
                }
            });
            return;
        }

        var symbol = new esri.symbol.SimpleFillSymbol()
            .setColor(new dojo.Color([51, 255, 255, 0.3]))
            .setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,255]), 2));

        dojo.forEach(evt.featureSet.features,function(feature){
            gLayerRetrievingParcels.add(feature.setSymbol(symbol));
            //map.graphics.add(feature.setSymbol(symbol));
        });
    });
}

// Create symbols for the graphics
pointSymbol = createPointSymbol();
lineSymbol = createLineSymbol();
polygonSymbol = createPolygonSymbol();

function createPointSymbol() {
    require([
        "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol", "esri/Color"], function(SimpleMarkerSymbol, SimpleLineSymbol, Color) {

        return new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 7,
        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([255, 0, 0]), 1),
        new Color([255, 0, 0, 0.75]));
    });
}

function createLineSymbol() {
    require([
        "esri/symbols/SimpleLineSymbol", "esri/Color"], function(SimpleLineSymbol, Color) {
        return new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
        new Color([255, 0, 0, 0.75]),
        2);
    });
}

function createPolygonSymbol() {
    require([
        "esri/symbols/SimpleFillSymbol", "esri/symbols/SimpleLineSymbol", "esri/Color"], function(SimpleFillSymbol, SimpleLineSymbol, Color) {
        return new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([255, 0, 0, 0.75]), 1), new Color([255, 0, 0, 0.25]));
    });
}

// Add polygon graphic

function addPolygon(pt, mustFinish, typeSymbol) {
    require([
        "esri/map", "esri/graphic", "esri/geometry/Polygon", "esri/symbols/SimpleFillSymbol", "esri/symbols/SimpleLineSymbol", "esri/Color"], function(Map, Graphic, Polygon, SimpleFillSymbol, SimpleLineSymbol, Color) {
        if(typeof pts == 'undefined' || pts == null) pts = [];

        //Finding duplicates values
        var isDupli = "false";
        for(var i=0; i <= pts.length-1; i++)
        {
            if(pts[i].x == pt.x && pts[i].y == pt.y)
            {
                if(!mustFinish)
                {
                    $.alert({
                        title: 'LIS parcels',
                        content: 'Duplicate point. Vertex not drawn',
                        theme: 'white',
                        autoClose: 'confirm|3000',
                        confirm: function(){
                        }
                    });
                    isDupli = "true";
                    return;
                }
            }
        }

        //It is good. So adding the point to the Array
        if(isDupli == "false" && !mustFinish) {
            pts.push(pt);
        }

        if (mustFinish && pts.length < 3) {
            $.alert({
                title: 'LIS parcels',
                content: 'You need more than 2 vertex in order to close the parcel',
                theme: 'white',
                autoClose: 'confirm|3000',
                confirm: function(){
                }
            });
            return;
        }


        // Add a temporary point
        if (!mustFinish) addTempPoint(pt, typeSymbol);

        // Create the polygon and graphic. I mean, creating the interactive polygon filling
        if (pts.length > 1) {
            if (mustFinish && pts.length > 2) pts.push(pts[0]);  // Close the ring

            polygon = new Polygon(pt.spatialReference);
            polygon.addRing(pts);
            if(typeof polygonGraphic == 'undefined' || polygonGraphic == null) {
                var mySymbol = new esri.symbol.SimpleFillSymbol()
                    .setColor(new dojo.Color([51, 255, 51, 0.25]))
                    .setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,0]), 3));

                polygonGraphic = new Graphic(polygon,mySymbol);
                gLayerDrawingParcels.add(polygonGraphic);
                //map.graphics.add(polygonGraphic);
            }
            else
                polygonGraphic.setGeometry(polygon);
        }

        //Close the polygon and initialize values
        if (mustFinish && pts.length > 2) {
            try{
                polygonGraphic = null;
                //removing our vertex. We don't need them anymore when we draw the polygon
                for(var i=0;i<myPointGraphic.length;i++)
                    gLayerDrawingParcels.remove(myPointGraphic[i]);
                //initialize arrays
                myPointGraphic = null;
                pts = null;
                //Clean inputs
                if($("#selectTypeSystem").val() == "opAbsolute") {
                    $("#goX_DP").val("");
                    $("#goY_DP").val("");
                }
                if($("#selectTypeSystem").val() == "opPolar"){
                    $("#goD").val("");
                    $("#goM").val("");
                    $("#goS").val("");
                    $("#goDist").val("");
                }
                reInitializeDrawDialog();
                //Restore the badge value when closing the Parcel
                //var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                //myBadgeValue.text("1");
                //Enable Select Coordinate System after closing the parcel
                //$('#selectTypeCoordinates2').prop('disabled', false);
                //$('#selectTypeSystem').prop('disabled', false);
            } catch (e) {  alert(e); }
        }
    });
}


function addTempPoint(pt, typeSymbol) {
    require([
        "esri/map", "esri/geometry/Multipoint", "esri/geometry/Point", "esri/graphic", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol", "esri/Color"], function(Map, Multipoint, Point, Graphic, SimpleMarkerSymbol, SimpleLineSymbol, Color) {

        //Creating the symbols for drawing vertex
        var mySymbolSquare = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 8,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([0, 0, 0]), 1),
            new Color([175, 175, 175, 1]));

        var mySymbolCircle = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 4);
        mySymbolCircle.setColor(new esri.Color([0,0,0,1]));


        var myPoint = new Point(pt, pt.spatialReference);
        if(typeSymbol == "circleSymbol") var myPointGraphicTMP = new Graphic(myPoint);
        else var myPointGraphicTMP = new Graphic(myPoint,mySymbolSquare);
        myPointGraphic.push(myPointGraphicTMP);
        gLayerDrawingParcels.add(myPointGraphic[myPointGraphic.length-1]);


        //Increment the badge value on the Draw a parcel form
        //var myBadgeValue = $("#btnDrawParcel").find("span.badge");
        //var count = parseInt(myBadgeValue.text());
        //myBadgeValue.text(count+1);

        $("#lblCoordinates2").text("Enter coordinates for the next point:");

        //Changing to Polar mode on the second point if we are in opPolar
        if($("#selectTypeSystem").val() == "opPolar" && !alreadyPolar) { //We need to create this controls only the first time
            //$('#goX_DP').remove();
            //$('#goY_DP').remove();
            $('#goX_DP').hide();
            $('#goY_DP').hide();
            //And then, we create the news one
            $('#inputZoneDrawParcel')
                .append("<label id='drawMyLabel'> <input id='curve' name='curve' type='checkbox' onchange='myCurveFunction()'> Curve</label>");
            $('#inputZoneDrawParcel')
                .append("<input id='goRadius' type='text' name='Radius' placeholder='Radius' style='width:75px; margin-left: 12px' required disabled>");
            $('#inputZoneDrawParcel')
                .append("<input id='goArc' type='text' name='Arc' placeholder='Arc' style='width:75px;margin-left: 5px' required disabled>");
            $('#inputZoneDrawParcel')
                .append("<select id='clockwise' name='clockwise' class='select2'><option value='Counterclockwise'><b>&#x21ba;</b></option><option value='Clockwise'>&#x21bb;</option></select>");
            $('#inputZoneDrawParcel')
                .append("<div id='tmpStyle01' style='margin-top: 5px'>");
            $('#inputZoneDrawParcel')
                .append("<select id='latNS' name='latNS' class='select2'><option value='N'>N</option><option value='S'>S</option></select>");
            $('#inputZoneDrawParcel')
                .append("<input class='form-control myInputGeo' id='goD' type='text' name='D' placeholder='D' required style='margin-left: 10px'><label id='labelD'><b>&#176; </b></label>");
            $('#inputZoneDrawParcel')
                .append("<input class='form-control myInputGeo' id='goM' type='text' name='M' placeholder='M' required><label id='labelM'><b>&#039; </b></label>");
            $('#inputZoneDrawParcel')
                .append("<input class='form-control myInputGeo' id='goS' type='text' name='S' placeholder='S' required><label id='labelS'><b>&#034;</b></label>");
            $('#inputZoneDrawParcel')
                .append("<select id='lonEW' name='lonEW' class='select2' style='margin-left: 10px'><option value='E'>E</option><option value='W'>W</option></select></div>");
            $('#inputZoneDrawParcel')
                .append("</div>");
            if($("#selectTypeCoordinates2").val() == "opProjected") $('#inputZoneDrawParcel').append("<input class='form-control' id='goDist' type='text' name='dist' placeholder='Distance (meters)' required>");
            if($("#selectTypeCoordinates2").val() == "opMetric") $('#inputZoneDrawParcel').append("<input class='form-control' id='goDist' type='text' name='dist' placeholder='Distance (ft)' required>");


            //No more new controls
            alreadyPolar = true;
        }
        //Disable Selects controls
        $('#selectTypeCoordinates2').prop('disabled', 'disabled');
        $('#selectTypeSystem').prop('disabled', 'disabled');
    });
}

function myCurveFunction() {
    if($("#curve").is(':checked')) {
        $('#goRadius').prop('disabled', false);
        $('#goArc').prop('disabled', false);
        $('#goDist').prop('placeholder', 'Chord');
    }
    else {
        $('#goRadius').val('');
        $('#goArc').val('');
        $('#goRadius').prop('disabled', true);
        $('#goArc').prop('disabled', true);
        if($("#selectTypeCoordinates2").val() == "opProjected") $('#goDist').prop('placeholder', 'Distance (meters)');
        if($("#selectTypeCoordinates2").val() == "opMetric") $('#goDist').prop('placeholder', 'Distance (ft)');

    }
}

function drawCurve(coorSys) {
    var degrees = parseInt($('#goD').val());
    var minutes = parseInt($('#goM').val());
    var seconds = parseInt($('#goS').val());
    var distance = parseFloat($('#goDist').val());
    var radius = parseFloat($('#goRadius').val());
    var arcRadians = parseFloat($('#goArc').val());
    var arcDegrees = arcRadians * 360 / (2*radius*Math.PI);

    if(isNaN(degrees) || isNaN(minutes) || isNaN(seconds) || isNaN(distance) || isNaN(radius) || isNaN(arcRadians)){
        alert("Please, fill up all fields");
        return;
    }

    //Convert from DMS to decimal
    var fracc = ((minutes * 60) + seconds)/3600;
    var degreeDecimal = degrees + fracc;
    if($('#latNS').val() == "N" && $('#lonEW').val() == "E") {
        var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
        var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
    }
    if($('#latNS').val() == "N" && $('#lonEW').val() == "W") {
        var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
        var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
    }
    if($('#latNS').val() == "S" && $('#lonEW').val() == "E") {
        var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
        var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
    }
    if($('#latNS').val() == "S" && $('#lonEW').val() == "W") {
        var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
        var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
    }

    console.log("Start point:" + pts[pts.length-1].x + "; " + pts[pts.length-1].y);

    //var myVar01 = new Flatten(320565.92, 5267620.21, 320574.95, 5267626.334, "counterclockwise", 11.081, 17, 0.002);
    var myVar01 = new Flatten(pts[pts.length-1].x, pts[pts.length-1].y, X, Y, $('#clockwise').val(), arcRadians, radius, 0.01);
    var mySomething = myVar01.flattenArc();
    var pointsUsed = mySomething.length;
    for (var i=0; i<pointsUsed-1;i++) {
        console.log(mySomething[i].x + "; " + mySomething[i].y);
        var inPoint = new esri.geometry.Point([mySomething[i].x,mySomething[i].y] , map.spatialReference);
        addPolygon(inPoint,false,"circleSymbol");
    }

    //Draw the last point of my curve
    var inPoint = new esri.geometry.Point([mySomething[mySomething.length-1].x,mySomething[mySomething.length-1].y] , map.spatialReference);
    addPolygon(inPoint,false,"rect");
    //Very important to UNDO
    var myBadgeValue = $("#btnDrawParcel").find("span.badge");
    var count = parseInt(myBadgeValue.text());
    indexForUndo.push({index:count, pointsUsed:pointsUsed});
}


$(function(){

    if (document.getElementById('btnDirectDraw').addEventListener) {
        document.getElementById('btnDirectDraw').addEventListener("click", function() {
            toolbarDrawParcel.activate(esri.toolbars.Draw.POLYGON);

            $("#toggleDemo").collapse('hide'); //Collapsing the "Draw using survey" button
        }, false);
    }
    else {
        document.getElementById('btnDirectDraw').attachEvent("onclick", function() {
            toolbarDrawParcel.activate(esri.toolbars.Draw.POLYGON);

            $("#toggleDemo").collapse('hide'); //Collapsing the "Draw using survey" button
        });
    }

    if (document.getElementById('btnDrawUsingSurvey').addEventListener) {
        document.getElementById('btnDrawUsingSurvey').addEventListener("click", function() {
            toolbarDrawParcel.deactivate();
        }, false);
    }
    else {
        document.getElementById('btnDrawUsingSurvey').attachEvent("onclick", function() {
            toolbarDrawParcel.deactivate();
        });
    }

    if (document.getElementById('btnDrawParcel').addEventListener) {
        document.getElementById('btnDrawParcel').addEventListener("click", function() {
            toolManager.set("currentTool", "drawParcelUsingSurvey");
            mode = "drawParcelUsingSurvey";

            var myOpt = $("#selectTypeCoordinates2").val();
            switch (myOpt)
            {
                case "opProjected":
                    if(!alreadyPolar)
                    {
                        var X = parseFloat($("#goX_DP").val());
                        var Y = parseFloat($("#goY_DP").val());
                        //Controlling empty fields
                        if(isNaN(X) || isNaN(Y)) {
                            alert("Please, fill up all fields");
                            return;
                        }

                        //Test Extent
                        if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                        {
                            $("#goX_DP").val("");
                            $("#goY_DP").val("");
                            alert("Coordinates out of range");
                            return;
                        }
                        var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                        map.centerAndZoom(inPoint, 10);
                        addPolygon(inPoint,false,"rect");
                        //Increment the badge value on the Draw a parcel form
                        var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                        var count = parseInt(myBadgeValue.text());
                        myBadgeValue.text(count+1);
                        //Very important to UNDO
                        indexForUndo.push({index:count, pointsUsed:1});
                    }
                    else {
                        if($("#curve").is(':checked')) { //We need to calculate the complete path for that arc
                            drawCurve("Projected");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                        }
                        else {
                            var degrees = parseInt($('#goD').val());
                            var minutes = parseInt($('#goM').val());
                            var seconds = parseInt($('#goS').val());
                            var distance = parseFloat($('#goDist').val());
                            //Controlling empty fields
                            if(isNaN(degrees) || isNaN(minutes) || isNaN(seconds) || isNaN(distance)) {
                                alert("Please, fill up all fields");
                                return;
                            }

                            //Convert from DMS to decimal
                            var fracc = ((minutes * 60) + seconds)/3600;
                            var degreeDecimal = degrees + fracc;
                            if($('#latNS').val() == "N" && $('#lonEW').val() == "E") {
                                var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "N" && $('#lonEW').val() == "W") {
                                var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "S" && $('#lonEW').val() == "E") {
                                var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "S" && $('#lonEW').val() == "W") {
                                var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }

                            //Test Extent
                            if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                            {
                                $("#goD").val("");
                                $("#goM").val("");
                                $("#goS").val("");
                                $("#goDist").val("");
                                alert("Coordinates out of range");
                                return;
                            }
                            var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                            map.centerAndZoom(inPoint, 10);
                            addPolygon(inPoint,false,"rect");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                            //Very important to UNDO
                            indexForUndo.push({index:count, pointsUsed:1});
                        }
                    }
                    break;

                case "opMetric":
                    if(!alreadyPolar)
                    {
                        //is numeric
                        if($.isNumeric($("#goX_DP").val()) != false && $.isNumeric($("#goY_DP").val()) != false){
                            var X = (parseFloat($("#goX_DP").val()) * 0.3048) + 72.9;
                            var Y = (parseFloat($("#goY_DP").val()) * 0.3048) + 212.9;

                            //Test Extent
                            if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                            {
                                $("#goX_DP").val("");
                                $("#goY_DP").val("");
                                alert("Coordinates out of range");
                                return;
                            }
                            var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                            map.centerAndZoom(inPoint, 10);
                            addPolygon(inPoint,false,"rect");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                            //Very important to UNDO
                            indexForUndo.push({index:count, pointsUsed:1});
                        }
                    }
                    else {
                        if($("#curve").is(':checked')) { //We need to calculate the complete path for that arc
                            drawCurve("Metric");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                        }
                        else {
                            var degrees = parseInt($('#goD').val());
                            var minutes = parseInt($('#goM').val());
                            var seconds = parseInt($('#goS').val());
                            var distance = parseFloat($('#goDist').val()) * 0.30480; //Distance in meters
                            //Convert from DMS to decimal
                            var fracc = ((minutes * 60) + seconds)/3600;
                            var degreeDecimal = degrees + fracc;
                            if($('#latNS').val() == "N" && $('#lonEW').val() == "E") {
                                var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "N" && $('#lonEW').val() == "W") {
                                var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "S" && $('#lonEW').val() == "E") {
                                var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "S" && $('#lonEW').val() == "W") {
                                var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            //Test Extent
                            if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                            {
                                $("#goD").val("");
                                $("#goM").val("");
                                $("#goS").val("");
                                $("#goDist").val("");
                                alert("Coordinates out of range");
                                return;
                            }
                            var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                            map.centerAndZoom(inPoint, 10);
                            addPolygon(inPoint,false,"rect");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                            //Very important to UNDO
                            indexForUndo.push({index:count, pointsUsed:1});
                        }
                    }
                    break;
            }
        }, false);
    }
    else {
        document.getElementById('btnDrawParcel').attachEvent("onclick", function() {
            toolManager.set("currentTool", "drawParcelUsingSurvey");
            mode = "drawParcelUsingSurvey";

            var myOpt = $("#selectTypeCoordinates2").val();
            switch (myOpt)
            {
                case "opProjected":
                    if(!alreadyPolar)
                    {
                        var X = parseFloat($("#goX_DP").val());
                        var Y = parseFloat($("#goY_DP").val());
                        //Controlling empty fields
                        if(isNaN(X) || isNaN(Y)) {
                            alert("Please, fill up all fields");
                            return;
                        }

                        //Test Extent
                        if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                        {
                            $("#goX_DP").val("");
                            $("#goY_DP").val("");
                            alert("Coordinates out of range");
                            return;
                        }
                        var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                        map.centerAndZoom(inPoint, 10);
                        addPolygon(inPoint,false,"rect");
                        //Increment the badge value on the Draw a parcel form
                        var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                        var count = parseInt(myBadgeValue.text());
                        myBadgeValue.text(count+1);
                        //Very important to UNDO
                        indexForUndo.push({index:count, pointsUsed:1});
                    }
                    else {
                        if($("#curve").is(':checked')) { //We need to calculate the complete path for that arc
                            drawCurve("Projected");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                        }
                        else {
                            var degrees = parseInt($('#goD').val());
                            var minutes = parseInt($('#goM').val());
                            var seconds = parseInt($('#goS').val());
                            var distance = parseFloat($('#goDist').val());
                            //Controlling empty fields
                            if(isNaN(degrees) || isNaN(minutes) || isNaN(seconds) || isNaN(distance)) {
                                alert("Please, fill up all fields");
                                return;
                            }

                            //Convert from DMS to decimal
                            var fracc = ((minutes * 60) + seconds)/3600;
                            var degreeDecimal = degrees + fracc;
                            if($('#latNS').val() == "N" && $('#lonEW').val() == "E") {
                                var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "N" && $('#lonEW').val() == "W") {
                                var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "S" && $('#lonEW').val() == "E") {
                                var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "S" && $('#lonEW').val() == "W") {
                                var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }

                            //Test Extent
                            if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                            {
                                $("#goD").val("");
                                $("#goM").val("");
                                $("#goS").val("");
                                $("#goDist").val("");
                                alert("Coordinates out of range");
                                return;
                            }
                            var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                            map.centerAndZoom(inPoint, 10);
                            addPolygon(inPoint,false,"rect");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                            //Very important to UNDO
                            indexForUndo.push({index:count, pointsUsed:1});
                        }
                    }
                    break;

                case "opMetric":
                    if(!alreadyPolar)
                    {
                        //is numeric
                        if($.isNumeric($("#goX_DP").val()) != false && $.isNumeric($("#goY_DP").val()) != false){
                            var X = (parseFloat($("#goX_DP").val()) * 0.3048) + 72.9;
                            var Y = (parseFloat($("#goY_DP").val()) * 0.3048) + 212.9;

                            //Test Extent
                            if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                            {
                                $("#goX_DP").val("");
                                $("#goY_DP").val("");
                                alert("Coordinates out of range");
                                return;
                            }
                            var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                            map.centerAndZoom(inPoint, 10);
                            addPolygon(inPoint,false,"rect");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                            //Very important to UNDO
                            indexForUndo.push({index:count, pointsUsed:1});
                        }
                    }
                    else {
                        if($("#curve").is(':checked')) { //We need to calculate the complete path for that arc
                            drawCurve("Metric");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                        }
                        else {
                            var degrees = parseInt($('#goD').val());
                            var minutes = parseInt($('#goM').val());
                            var seconds = parseInt($('#goS').val());
                            var distance = parseFloat($('#goDist').val()) * 0.30480; //Distance in meters
                            //Convert from DMS to decimal
                            var fracc = ((minutes * 60) + seconds)/3600;
                            var degreeDecimal = degrees + fracc;
                            if($('#latNS').val() == "N" && $('#lonEW').val() == "E") {
                                var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "N" && $('#lonEW').val() == "W") {
                                var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y + (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "S" && $('#lonEW').val() == "E") {
                                var X = pts[pts.length-1].x + (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            if($('#latNS').val() == "S" && $('#lonEW').val() == "W") {
                                var X = pts[pts.length-1].x - (Math.sin(degreeDecimal * Math.PI / 180.0) * distance);
                                var Y = pts[pts.length-1].y - (Math.cos(degreeDecimal * Math.PI / 180.0) * distance);
                            }
                            //Test Extent
                            if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                            {
                                $("#goD").val("");
                                $("#goM").val("");
                                $("#goS").val("");
                                $("#goDist").val("");
                                alert("Coordinates out of range");
                                return;
                            }
                            var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                            map.centerAndZoom(inPoint, 10);
                            addPolygon(inPoint,false,"rect");
                            //Increment the badge value on the Draw a parcel form
                            var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                            var count = parseInt(myBadgeValue.text());
                            myBadgeValue.text(count+1);
                            //Very important to UNDO
                            indexForUndo.push({index:count, pointsUsed:1});
                        }
                    }
                    break;
            }
        });
    }


    if (document.getElementById('btnDrawParcelClose').addEventListener) {
        document.getElementById('btnDrawParcelClose').addEventListener("click", function() {
            if(typeof pts != 'undefined' && pts.length > 0) {
                addPolygon(pts[0],true,"rect");
            }
        }, false);
    }
    else {
        document.getElementById('btnDrawParcelClose').attachEvent("onclick", function() {
            if(typeof pts != 'undefined' && pts.length > 0) {
                addPolygon(pts[0],true,"rect");
            }
        });
    }

    if (document.getElementById('btnRetrieveParcel').addEventListener) {
        document.getElementById('btnRetrieveParcel').addEventListener("click", function() {
            toolbarRetrieveParcel.activate(esri.toolbars.Draw.FREEHAND_POLYGON);
        }, false);
    }
    else {
        document.getElementById('btnRetrieveParcel').attachEvent("onclick", function() {
            toolbarRetrieveParcel.activate(esri.toolbars.Draw.FREEHAND_POLYGON);
        });
    }

    if (document.getElementById('btnDownloadParcel').addEventListener) {
        document.getElementById('btnDownloadParcel').addEventListener("click", function() {
            toolbarDrawParcel.deactivate();
            $.ajax({
                url: "js/GraphicLayer2JSON.js",
                success: function(response) {
                    var ret = GLayer2JSON(gLayerDrawingParcels, 32181);
                    if(ret == 0) {
                        $.alert({
                            title: 'LIS parcels',
                            content: 'Your parcel is being downloaded',
                            theme: 'white',
                            autoClose: 'confirm|3000',
                            confirm: function(){
                            }
                        });
                    }
                    else if(ret == -1) {
                        $.alert({
                            title: 'LIS parcels',
                            content: 'You did not draw any parcel yet',
                            theme: 'white',
                            autoClose: 'confirm|3000',
                            confirm: function(){
                            }
                        });
                    }
                    else {
                        $.alert({
                            title: 'LIS parcels',
                            content: 'Your wkids do not match. Your Layer is WKID:' + ret,
                            theme: 'white',
                            confirm: function(){
                            }
                        });
                    }


                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error in GLayer2JSON. " + xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
                }
            });
        }, false);
    }
    else {
        document.getElementById('btnDownloadParcel').attachEvent("onclick", function() {
            toolbarDrawParcel.deactivate();
            $.ajax({
                url: "js/GraphicLayer2JSON.js",
                success: function(response) {
                    var ret = GLayer2JSON(gLayerDrawingParcels, 32181);
                    if(ret == 0) {
                        $.alert({
                            title: 'LIS parcels',
                            content: 'Your parcel is being downloaded',
                            theme: 'white',
                            autoClose: 'confirm|3000',
                            confirm: function(){
                            }
                        });
                    }
                    else if(ret == -1) {
                        $.alert({
                            title: 'LIS parcels',
                            content: 'You did not draw any parcel yet',
                            theme: 'white',
                            autoClose: 'confirm|3000',
                            confirm: function(){
                            }
                        });
                    }
                    else {
                        $.alert({
                            title: 'LIS parcels',
                            content: 'Your wkids do not match. Your Layer is WKID:' + ret,
                            theme: 'white',
                            confirm: function(){
                            }
                        });
                    }


                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error in GLayer2JSON. " + xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
                }
            });
        });
    }

    if (document.getElementById('btnUndo').addEventListener) {
        document.getElementById('btnUndo').addEventListener("click", function() {
            if(myPointGraphic.length < 1) $.alert({
                title: 'Undo operation',
                content: 'Nothing was drawn',
                theme: 'white',
                confirm: function(){
                }
            });
            if(myPointGraphic.length > 0) {
                //Removing segment
                for(var i=0;i<indexForUndo.length;i++) {
                    var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                    var count = parseInt(myBadgeValue.text()) - 1;
                    //console.log("Badge Value:" + count + "; index:" + indexForUndo[i].index + "; pointsUsed:" + indexForUndo[i].pointsUsed);
                    if(count == indexForUndo[i].index) {
                        for (var j=0;j<indexForUndo[i].pointsUsed;j++) {
                            polygon.removePoint(polygon.rings.length - 1, pts.length - 1);
                            polygonGraphic.setGeometry(polygon);
                            pts.length = pts.length - 1;
                            //Removing node
                            gLayerDrawingParcels.remove(myPointGraphic[myPointGraphic.length-1]);
                            myPointGraphic.length = myPointGraphic.length-1;
                        }
                    }
                }
                indexForUndo.length = indexForUndo.length - 1;

                //Updating Badge Value
                var myBadgeValue = $("#btnDrawParcel").find("span.badge");
                var count = parseInt(myBadgeValue.text());
                myBadgeValue.text(count-1);
                if (parseInt(myBadgeValue.text()) == 1) {
                    reInitializeDrawDialog();
                    polygonGraphic = null;
                    map.getLayer("layerDrawingParcels").graphics.length = map.getLayer("layerDrawingParcels").graphics.length - 1;
                    console.log("pts lenght is:" + pts.length + "; gLayerDrawingParcels lenght is (features):" + map.getLayer("layerDrawingParcels").graphics.length + "; myPointGraphic.length is:" + myPointGraphic.length);
                }
            }
        }, false);
    }
    else {
        document.getElementById('btnUndo').attachEvent("onclick", function() {
            //TO DO
        });
    }

    /* Change the glyphicon-plus for glyphicon-minus when it is expanded. It is used on DrawParcel tool and Mapcentre Video Help*/
    $('#dialog-draw-parcel-tool .collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });

});