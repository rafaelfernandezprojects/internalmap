
var swipeWidget;
var mapClipForDrawText;
var drawWidthValue;
var drawStyleValue;
var graphicPushpin;



function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
}

/* Restoring initial values */
function checkStillAlive() {
    
    if (swipeWidget != null) {
        //swipeWidget.destroy();
        swipeWidget.disable();
        topo.hide();
        ortho2015.show();
        ortho2013.hide();
        ortho2010.hide();
        ortho2006.hide();
        ortho2003.hide();
        DEM.hide();
        CityLabelsImagery.hide();
        Property.hide();
        Annotation.hide();
        Address_Annotation.show();
        CityBounds.show();
        Elevation.hide();
        wards.hide();
        planning.hide();
        IM360.hide();
        watersheds.hide();
        sewer.hide();
        waterServices.hide();
        trafficSigns.hide();
        neighbourhood.hide();


        document.getElementById("elevationDIV").style.display = "none";
        toolManager.set("currentTool", "");
        mode = "";

        imgTopo.style.border = "1px solid black";
        imgOrtho2015.style.border = "4px solid black";
        imgOrtho2013.style.border = "1px solid black";
        imgOrtho2010.style.border = "1px solid black";
        imgOrtho2006.style.border = "1px solid black";
        imgOrtho2003.style.border = "1px solid black";
        //imgDEM.style.border = "1px solid black";
    }

}

/* I need to know which layer chose the user */
function click_btnSwipe(){
    require([
        "esri/map", "esri/dijit/LayerSwipe"], function (Map, LayerSwipe) {

        var sel = $("#comboCompare option:selected").val();

        if (map.getScale() < 1000) map.setScale(1000);

        if (sel != null) {
            if (sel == "2013"){
                topo.hide();
                ortho2015.show();
                ortho2013.show();
                ortho2010.hide();
                ortho2006.hide();
                ortho2003.hide();
                DEM.hide();
                CityLabelsImagery.show();
                Property.hide();
                Annotation.show();
                Address_Annotation.show();
                CityBounds.show();
                Elevation.hide();
                wards.hide();
                planning.hide();
                IM360.hide();
                watersheds.hide();
                sewer.hide();
                waterServices.hide();
                trafficSigns.hide();
                neighbourhood.hide();


                document.getElementById("elevationDIV").style.display = "none";
                toolManager.set("currentTool", "");
                mode = "";

                if (typeof swipeWidget != "undefined") {
                    swipeWidget.layers = [ortho2013];
                    swipeWidget.enable();
                }
                else {
                    swipeWidget = new esri.dijit.LayerSwipe({
                        type: "horizontal",  //Try switching to "scope" or "horizontal"
                        map: map,
                        enabled: true,
                        layers: [ortho2013]
                    }, "swipeDiv");
                }
            }
            if (sel == "2010"){
                topo.hide();
                ortho2015.show();
                ortho2013.hide();
                ortho2010.show();
                ortho2006.hide();
                ortho2003.hide();
                DEM.hide();
                CityLabelsImagery.show();
                Property.hide();
                Annotation.show();
                Address_Annotation.show();
                CityBounds.show();
                Elevation.hide();
                wards.hide();
                planning.hide();
                IM360.hide();
                watersheds.hide();
                sewer.hide();
                waterServices.hide();
                trafficSigns.hide();
                neighbourhood.hide();


                document.getElementById("elevationDIV").style.display = "none";
                toolManager.set("currentTool", "");
                mode = "";

                if (typeof swipeWidget != "undefined") {
                    swipeWidget.layers = [ortho2010];
                    swipeWidget.enable();
                }
                else {
                    swipeWidget = new esri.dijit.LayerSwipe({
                        type: "horizontal",  //Try switching to "scope" or "horizontal"
                        map: map,
                        enabled: true,
                        layers: [ortho2010]
                    }, "swipeDiv");
                }
            }
            if (sel == "2006"){
                topo.hide();
                ortho2015.show();
                ortho2013.hide();
                ortho2010.hide();
                ortho2006.show();
                ortho2003.hide();
                DEM.hide();
                CityLabelsImagery.show();
                Property.hide();
                Annotation.show();
                Address_Annotation.show();
                CityBounds.show();
                Elevation.hide();
                wards.hide();
                planning.hide();
                IM360.hide();
                watersheds.hide();
                sewer.hide();
                waterServices.hide();
                trafficSigns.hide();
                neighbourhood.hide();


                document.getElementById("elevationDIV").style.display = "none";
                toolManager.set("currentTool", "");
                mode = "";

                if (typeof swipeWidget != "undefined") {
                    swipeWidget.layers = [ortho2006];
                    swipeWidget.enable();
                }
                else {
                    swipeWidget = new esri.dijit.LayerSwipe({
                        type: "horizontal",  //Try switching to "scope" or "horizontal"
                        map: map,
                        enabled: true,
                        layers: [ortho2006]
                    }, "swipeDiv");
                }
            }
            if (sel == "2003") {
                
                topo.hide();
                ortho2015.show();
                ortho2013.hide();
                ortho2010.hide();
                ortho2006.hide();
                ortho2003.show();
                DEM.hide();
                CityLabelsImagery.show();
                Property.hide();
                Annotation.show();
                Address_Annotation.show();
                CityBounds.show();
                Elevation.hide();
                wards.hide();
                planning.hide();
                IM360.hide();
                watersheds.hide();
                sewer.hide();
                waterServices.hide();
                trafficSigns.hide();
                neighbourhood.hide();


                document.getElementById("elevationDIV").style.display = "none";
                toolManager.set("currentTool", "");
                mode = "";

                //var mySwipe = document.getElementById("swipeDiv");
                //if (!mySwipe) $('#map').append("<div id='swipeDiv'></div>");

                if (typeof swipeWidget != "undefined") {
                    swipeWidget.layers = [ortho2003];
                    swipeWidget.enable();
                }
                else {
                    swipeWidget = new esri.dijit.LayerSwipe({
                        type: "horizontal",  //Try switching to "scope" or "horizontal"
                        map: map,
                        enabled: true,
                        layers: [ortho2003]
                    }, "swipeDiv");
                }
            }

            /* Only we want a border on 2013 */
            imgTopo.style.border = "1px solid black";
            imgOrtho2015.style.border = "4px solid black";
            imgOrtho2013.style.border = "1px solid black";
            imgOrtho2010.style.border = "1px solid black";
            imgOrtho2006.style.border = "1px solid black";
            imgOrtho2003.style.border = "1px solid black";
            //imgDEM.style.border = "1px solid black";

            //console.log(swipeWidget.enabled);
            //console.log(swipeWidget.loaded);
            //console.log(swipeWidget.visible);
            if (swipeWidget.loaded != true) swipeWidget.startup(); //Check if the widget was already loaded before
            
        }
        $('#myCompare').modal('hide');
    });
}

function click_informationServices(){
    checkStillAlive();
    $('#findAddress').modal('show');
    map.graphics.clear();
    toolManager.set("currentTool", "neutral");
    mode = "neutral";
    map.setMapCursor("default");

    printActivity("Search");
    //document.getElementById("map_layers").style.cursor = "default";
}

function click_MapSwitcher(){
    checkStillAlive();
    $('#myModal').modal('show');
    toolManager.set("currentTool", "");
    mode = "";
    map.setMapCursor("default");

    topo.hide();
    ortho2015.show();
    ortho2013.hide();
    ortho2010.hide();
    ortho2006.hide();
    ortho2003.hide();
    DEM.hide();
    CityLabelsImagery.hide();
    Property.hide();
    Annotation.hide();
    Address_Annotation.show();
    CityBounds.show();
    Elevation.hide();
    wards.hide();
    planning.hide();
    IM360.hide();
    watersheds.hide();
    sewer.hide();
    waterServices.hide();
    trafficSigns.hide();
    neighbourhood.hide();

    document.getElementById("elevationDIV").style.display = "none";
    imgTopo.style.border = "1px solid black";
    imgOrtho2015.style.border = "4px solid black";
    imgOrtho2013.style.border = "1px solid black";
    imgOrtho2010.style.border = "1px solid black";
    imgOrtho2006.style.border = "1px solid black";
    imgOrtho2003.style.border = "1px solid black";
    //imgDEM.style.border = "1px solid black";

    printActivity("MapSwitcher");
    document.getElementById("map_layers").style.cursor = "default";
}

function click_Templates(){
    checkStillAlive();
    $('#myTemplates').modal('show');
    //Deactivate any radio button before showing the template dialog
    $('#containerTemplates li input').prop("checked", false);
    toolManager.set("currentTool", "neutral");
    mode = "neutral";
    map.setMapCursor("default");

    printActivity("Templates");
    document.getElementById("map_layers").style.cursor = "default";
}

function click_Weather(){
    printActivity("Weather");
    document.getElementById("map_layers").style.cursor = "default";
    checkStillAlive();
    $('#forecast').modal('show');
    toolManager.set("currentTool", "neutral");
    mode = "neutral";
    map.setMapCursor("default");
}

function click_MapLayers(){
    checkStillAlive();
    //$('#MapLayersDialog').modal('show');
    var MyLeftPanel = dijit.byId("leftPane");
    if(MyLeftPanel) {
        dijit.byId('mapcontent').addChild(MyLeftPanel);
        //MyLeftPanel.destroy();
    }
    toolManager.set("currentTool", "neutral");
    mode = "neutral";
    map.setMapCursor("default");

    printActivity("MapLayers");
    document.getElementById("map_layers").style.cursor = "default";
}

function click_Contact(){
    checkStillAlive();
    $('#myContact').modal('show');

    toolManager.set("currentTool", "neutral");
    mode = "neutral";
    map.setMapCursor("default");

    printActivity("Contact");
    document.getElementById("map_layers").style.cursor = "default";
}

function click_Measurement(){

    require([
        "esri/map", "esri/dijit/Measurement", "esri/units", "esri/symbols/SimpleLineSymbol", "dojo/dom"], function(Map, Measurement, Units, SimpleLineSymbol, dom) {

        checkStillAlive();

        printActivity("Measurement");
        document.getElementById("map_layers").style.cursor = "default";

        /*if ($("#titlePane").css("visibility") != "hidden") {
            document.getElementById("titlePane").style.visibility = "hidden";
            playAnimation(map_graphics_layer);
            if(measurement){
                measurement.clearResult();

                measurement.destroy();

                measurement.hide();
            }
            mode = "neutral";
            map.setMapCursor("default");
        }
        else{
            document.getElementById("titlePane").style.visibility = "visible";*/
            toolManager.set("currentTool", "measure");
            mode = "measure";
            map.setMapCursor("crosshair");

            stopAnimation(map_graphics_layer);

            var sls = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0, 1]), 3);

            $("#dialog-measure-tool").dialog({
                //height: 300,
                width: 310,
                dialogClass: "alertDialog",
                resizable: false,
                closeOnEscape: false, // disable escape event on dialog
                //buttons: [{ text: "Ok", click: function() { $( this ).dialog( "close" ); } }],
                close: function (event, ui) {
                    toolManager.set("currentTool", "neutral");
                    mode = "neutral";
                    map.setMapCursor("default");
                    toolbar.deactivate();
                    measurement.clearResult();
                    var measureTool = measurement.getTool();
                    
                    if (measureTool) {
                        var toolName = measureTool.toolName; 
                        //alert(toolName);
                        measurement.setTool(toolName, false);
                    }
                    
                    playAnimation(map_graphics_layer);
                },
                modal: false
            })
            //alert($("#dialog-measure-tool").zIndex());
            //alert($("#measurementDiv").zIndex());
            
            if(measurement){
                /*var measurementDiv = dom.byId("measurementDiv");
                alert(measurementDiv);
                //measurement.clearResult();
                measurement = null;

                measurement = new esri.dijit.Measurement({
                    map: map,
                    lineSymbol : sls,
                    advancedLocationUnits: false,
                    defaultAreaUnit: esri.Units.SQUARE_METERS,
                    defaultLengthUnit: esri.Units.METERS
                }, dojo.create('measurementDiv'));

                dojo.place(measurement.domNode,dojo.byId('titlePane_pane'));

                measurement.startup();
                measurement.hideTool('location');*/
            }
            else {
                //var measurementDiv = dom.byId("measurementDiv");
                //alert(measurementDiv);

                measurement = new Measurement({
                    map: map,
                    lineSymbol : sls,
                    advancedLocationUnits: false,
                    defaultAreaUnit: esri.Units.SQUARE_METERS,
                    defaultLengthUnit: esri.Units.METERS
                }, dom.byId("measurementDiv"));

                measurement.startup();
                measurement.hideTool('location');


                /*measurement.on("measure-end", function(evt){
                    //measurement.setTool(evt.activeTool, false);
                });*/
                /*
                dojo.connect(measurement, "onMeasure", function (toolName, geometry) {
                    if (toolName == "distance") {
                        //var resultValue = measurement.resultValue.domNode.innerHTML;
                        //var stateCoords = "XY: " + Math.round(geometry.x).toString() + ", " + Math.round(geometry.y).toString();

                        measurement.resultValue.domNode.innerHTML = "aaa";
                        //dojo.byId("distance").innerHTML = "sdfsdf";
                    }
                });
                */

            }


        //}
    });

}







/*--------------------------------+
|       DRAW TOOLBAR FUNCTIONS    |
+--------------------------------*/
function click_Draw(){
    require([
        "esri/map", "esri/toolbars/draw", "dojo/_base/connect", "esri/graphic", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleFillSymbol", "dojo/dom", "dojo/on", "dojo/domReady!"], function(Map, Draw, connect, Graphic, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol, dom, on) {
        //checkStillAlive();

        printActivity("Draw");
        document.getElementById("map_layers").style.cursor = "default";

        $("#dialog-draw-tool").dialog({
            //height: 300,
            width: 310,
            dialogClass: "alertDialog",
            resizable: false,
            closeOnEscape: false, // disable escape event on dialog
            //buttons: [{ text: "Ok", click: function() { $( this ).dialog( "close" ); } }],
            close: function (event, ui) {
                toolManager.set("currentTool", "neutral");
                mode = "neutral";
                map.setMapCursor("default");
                //playAnimation(map_graphics_layer);
                map.disableSnapping();
                toolbar.deactivate();
                connect.disconnect(mapClipForDrawText);
                map.enableMapNavigation();
                map.showZoomSlider();
                $("#drawTools tr td").removeClass('active-cell');
            },
            modal: false
        })
            .dialogExtend({
                "minimizable" : true,
                "icons" : { "minimize" : "ui-icon-circle-minus" }
            });
        //Clear parcels layer when the dialog appears
        map.getLayer("layerParcels").clear();
        toolManager.set("currentTool", "drawTools");
        mode = "drawTools";



        on(dom.byId("drawTools"), "click", function(evt) {
            var myTarget = evt.target.id;

            if (myTarget == "") return;
            if ( myTarget === "dialog-draw-tool") {
                return;
            }

            if ( myTarget != "text" && myTarget != "ClearGraphics" && myTarget != "deleteSelected") {
                //toolbar = new Draw(map);
                var tool = myTarget.toLowerCase();
                toolbar.activate(tool);
                map.disableMapNavigation();
                map.hideZoomSlider();
            }
            if ( myTarget === "text") {
                if($("#Drawtext").val() == "")
                {
                    alert("Please, write a valid text value");
                    toolbar.deactivate();
                    $("#drawTools tr td").removeClass('active-cell');
                    return;
                }
                toolbar.deactivate();
                mapClipForDrawText = connect.connect(map, "onClick", myClickHandlerDrawText);
            }
            if ( myTarget === "deleteSelected") {
                toolbar.deactivate();
                $("#drawTools tr td").removeClass('active-cell'); //The command finishes and the cell becomes inactive again
            }
            if ( myTarget === "ClearGraphics") {
                toolbar.deactivate();

                var numberFeatures = map.getLayer("layerToolbar").graphics.length + map.getLayer("layerToolbarText").graphics.length;

                //Dialog to confirm removing features
                if(numberFeatures > 0) {
                    $( "#dialog-confirm-cleargraphics" ).dialog({
                        resizable: false,
                        height:200,
                        modal: true,
                        buttons: {
                            "Delete all features": function() {
                                map.getLayer("layerToolbar").clear();
                                map.getLayer("layerToolbarText").clear();
                                $( this ).dialog( "close" );
                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    });
                }

                $("#drawTools tr td").removeClass('active-cell'); //The command finishes and the cell becomes inactive again
            }

            toolManager.set("currentTool", "draw");
            mode = "draw";
            map.setMapCursor("crosshair");

        });

        //Click on Cell to remove the grey background
        $('#drawTools td').on('click', function (e) {
            $("#drawTools tr td").removeClass('active-cell');
            $(this).addClass('active-cell');
        });

        $('#rightWidthDraw').on('click', function (e) {

            var tmpID;
            if(e.target.id == "") tmpID = e.target.parentNode.id;
            else tmpID = e.target.id;

            switch(tmpID) {
                case "width01":
                    drawWidthValue = 1;
                    break;
                case "width02":
                    drawWidthValue = 3;
                    break;
                case "width03":
                    drawWidthValue = 4;
                    break;
                case "width04":
                    drawWidthValue = 6;
                    break;
                default:
                    drawWidthValue = 2;
            }
            //$(e.target).addClass('active');
            //$(e.target).siblings().removeClass('active');
        });

        $('#rightStyleDraw').on('click', function (e) {
            if(e.target.id == "") drawStyleValue = e.target.parentNode.id;
            else drawStyleValue = e.target.id;

            //alert(e.target.parentNode.parentNode.children[0].id);
            //$(e.target.parentNode.parentNode.children[0]).removeClass('active');
            //$(e.target).addClass('active');

            //$(e.target).siblings().removeClass('active');
            //$(e.target).addClass('active');

        });


        /*
        on(dom.byId("dialog-draw-tool"), "click", function(evt) {
            if (evt.target.id == "") return;
            if ( evt.target.id === "dialog-draw-tool") {
                return;
            }

            if ( evt.target.id != "text" && evt.target.id != "ClearGraphics" && evt.target.id != "deleteSelected") {
                //toolbar = new Draw(map);
                var tool = evt.target.id.toLowerCase();
                toolbar.activate(tool);
                map.disableMapNavigation();
                map.hideZoomSlider();
            }
            if ( evt.target.id === "text") {
                toolbar.deactivate();
                mapClipForDrawText = connect.connect(map, "onClick", myClickHandlerDrawText);
            }
            if ( evt.target.id === "deleteSelected") {
                toolbar.deactivate();
            }
            if ( evt.target.id === "ClearGraphics") {
                toolbar.deactivate();
                map.getLayer("layerToolbar").clear();
                map.getLayer("layerToolbarText").clear();
            }

            mode = "draw";
            map.setMapCursor("crosshair");
        });
        */
    });
}

// Writing Text in our Draw tools
function myClickHandlerDrawText(evt) {
    require([
        "esri/map", "esri/graphic", "dojo/_base/connect", "esri/symbols/Font", "esri/symbols/TextSymbol", "esri/Color"], function(Map, Graphic, connect, Font, TextSymbol, Color) {

        if($("#Drawtext").val() == "")
        {
            alert("Please, write a valid text value");
            return;
        }

        var r = hexToRgb($("#color1").val()).r;
        var g = hexToRgb($("#color1").val()).g;
        var b = hexToRgb($("#color1").val()).b;

        var font = new Font("20px", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_BOLDER);
        //var textSymbol = new TextSymbol("X: " + evt.mapPoint.x + ", Y: " + evt.mapPoint.y, font, new Color([r, g, b]));
        var textSymbol = new TextSymbol($("#Drawtext").val(), font, new Color([r, g, b]));

        var labelPointGraphic = new Graphic(evt.mapPoint, textSymbol);

        // add the label point graphic to the map
        gLayerToolbarText.add(labelPointGraphic);

        connect.disconnect(mapClipForDrawText);
        $("#drawTools tr td").removeClass('active-cell');
        toolManager.set("currentTool", "neutral");
        mode = "neutral";
        map.setMapCursor("default");
    });
}
//Text indicating the type of draw
function OvertypeOfDraw(e){
    $('#myTypeOfDraw').html("Draw a " + e.id);
    if(e.id === "ClearGraphics") $('#myTypeOfDraw').html("Clear Graphics");
    if(e.id === "deleteSelected") $('#myTypeOfDraw').html("Remove selected");
    if(e.id === "arrow") $('#myTypeOfDraw').html("Draw an " + e.id);
    if(e.id === "btnVideoHelpDrawTools") $('#myTypeOfDraw').html("Open the video help");
}
function OuttypeOfDraw(){
    $('#myTypeOfDraw').html("");
}
/*---------------------------------------------+
 |       END - DRAW TOOLBAR FUNCTIONS - END    |
 +--------------------------------------------*/



/*----------------------------------------------+
 |       Go to Coordinates TOOLBAR FUNCTIONS    |
 +---------------------------------------------*/
function click_GoToCoordinates(){
    printActivity("GoTo");
    //document.getElementById("map_layers").style.cursor = "default";
    $("#dialog-gotocoordinates-tool").dialog({
        //height: 300,
        width: 310,
        dialogClass: "alertDialog",
        resizable: false,
        closeOnEscape: false, // disable escape event on dialog
        //buttons: [{ text: "Ok", click: function() { $( this ).dialog( "close" ); } }],
        close: function (event, ui) {
            toolManager.set("currentTool", "neutral");
            mode = "neutral";
            map.setMapCursor("default");
            //Clear the layer when the button is closed
            map.getLayer("layerPushpinGoToCoordinates").clear();
        },
        modal: false
    })
        .dialogExtend({
            "minimizable" : true,
            "icons" : { "minimize" : "ui-icon-circle-minus" }
        });
}

/*----------------------------------------------+
|       Google StreetView TOOLBAR FUNCTION      |
+----------------------------------------------*/
function click_GoogleStreetView(){
    printActivity("GoogleStreetView");
    toolManager.set("currentTool", "GoogleStreetView");
    mode = "GoogleStreetView";
    //map.setMapCursor("url('assests/images/googlestreetview.cur') 17 9, auto"); //Be careful with the offset
    document.getElementById("map_layers").style.cursor = "url('assests/images/googlestreetview.cur'), auto";
}


function click_Printing(){
    checkStillAlive();
    $('#myPrint').modal('show');

    toolManager.set("currentTool", "neutral");
    mode = "neutral";
    map.setMapCursor("default");

    printActivity("Print");
    document.getElementById("map_layers").style.cursor = "default";
}

function click_Help() {
    checkStillAlive();
    $('#myHelp').modal('show');

    toolManager.set("currentTool", "neutral");
    mode = "neutral";
    map.setMapCursor("default");

    printActivity("Help");
    document.getElementById("map_layers").style.cursor = "default";
}

function ocultaPrint(){
    //document.getElementById('myPrint').remove();
    document.getElementById("myPrint").style.visibility = "hidden";
}

function ocultaPanel(){

   var MyLeftPanel = dijit.byId("leftPane");

     if(MyLeftPanel) {
        dijit.byId('mapcontent').removeChild(MyLeftPanel);
        //dijit.byId('map').set('style', {width: '100%'});


        //alert(map.width);
        //$('#map').css('width', '100%');
        //MyLeftPanel.destroy();

    }
    map.resize(true);
    map.reposition();
    
}


function loadBasemap(typeBasemap){
    switch (typeBasemap)
    {
        case "Topo":
            topo.show();
            ortho2015.hide();
            ortho2013.hide();
            ortho2010.hide();
            ortho2006.hide();
            ortho2003.hide();
            DEM.hide();
            CityLabelsImagery.hide();
            Property.hide();
            Annotation.hide();
            Address_Annotation.show();
            CityBounds.hide();
            Elevation.hide();
            wards.hide();
            planning.hide();
            IM360.hide();
            watersheds.hide();
            sewer.hide();
            waterServices.hide();
            trafficSigns.hide();
            neighbourhood.hide();


            imgTopo.style.border = "4px solid black";
            imgOrtho2015.style.border = "1px solid black";
            imgOrtho2013.style.border = "1px solid black";
            imgOrtho2010.style.border = "1px solid black";
            imgOrtho2006.style.border = "1px solid black";
            imgOrtho2003.style.border = "1px solid black";
            //imgDEM.style.border = "1px solid black";
            document.getElementById("elevationDIV").style.display = "none";
            toolManager.set("currentTool", "");
            mode = "";
            break;
        case "ortho2015":
            topo.hide();
            ortho2015.show();
            ortho2013.hide();
            ortho2010.hide();
            ortho2006.hide();
            ortho2003.hide();
            DEM.hide();
            CityLabelsImagery.show();
            Property.hide();
            Annotation.show();
            Address_Annotation.show();
            CityBounds.show();
            Elevation.hide();
            wards.hide();
            planning.hide();
            IM360.hide();
            watersheds.hide();
            sewer.hide();
            waterServices.hide();
            trafficSigns.hide();
            neighbourhood.hide();


            imgTopo.style.border = "1px solid black";
            imgOrtho2015.style.border = "4px solid black";
            imgOrtho2013.style.border = "1px solid black";
            imgOrtho2010.style.border = "1px solid black";
            imgOrtho2006.style.border = "1px solid black";
            imgOrtho2003.style.border = "1px solid black";
            //imgDEM.style.border = "1px solid black";
            document.getElementById("elevationDIV").style.display = "none";
            toolManager.set("currentTool", "");
            mode = "";
            break;
        case "ortho2013":
            topo.hide();
            ortho2015.hide();
            ortho2013.show();
            ortho2010.hide();
            ortho2006.hide();
            ortho2003.hide();
            DEM.hide();
            CityLabelsImagery.show();
            Property.hide();
            Annotation.show();
            Address_Annotation.show();
            CityBounds.show();
            Elevation.hide();
            wards.hide();
            planning.hide();
            IM360.hide();
            watersheds.hide();
            sewer.hide();
            waterServices.hide();
            trafficSigns.hide();
            neighbourhood.hide();


            imgTopo.style.border = "1px solid black";
            imgOrtho2015.style.border = "1px solid black";
            imgOrtho2013.style.border = "4px solid black";
            imgOrtho2010.style.border = "1px solid black";
            imgOrtho2006.style.border = "1px solid black";
            imgOrtho2003.style.border = "1px solid black";
            //imgDEM.style.border = "1px solid black";
            document.getElementById("elevationDIV").style.display = "none";
            toolManager.set("currentTool", "");
            mode = "";
            break;
        case "ortho2010":
            topo.hide();
            ortho2015.hide();
            ortho2013.hide();
            ortho2010.show();
            ortho2006.hide();
            ortho2003.hide();
            DEM.hide();
            CityLabelsImagery.show();
            Property.hide();
            Annotation.show();
            Address_Annotation.show();
            CityBounds.show();
            Elevation.hide();
            wards.hide();
            planning.hide();
            IM360.hide();
            watersheds.hide();
            sewer.hide();
            waterServices.hide();
            trafficSigns.hide();
            neighbourhood.hide();


            imgTopo.style.border = "1px solid black";
            imgOrtho2015.style.border = "1px solid black";
            imgOrtho2013.style.border = "1px solid black";
            imgOrtho2010.style.border = "4px solid black";
            imgOrtho2006.style.border = "1px solid black";
            imgOrtho2003.style.border = "1px solid black";
            //imgDEM.style.border = "1px solid black";
            document.getElementById("elevationDIV").style.display = "none";
            toolManager.set("currentTool", "");
            mode = "";
            break;
        case "ortho2006":
            topo.hide();
            ortho2015.hide();
            ortho2013.hide();
            ortho2010.hide();
            ortho2006.show();
            ortho2003.hide();
            DEM.hide();
            CityLabelsImagery.show();
            Property.hide();
            Annotation.show();
            Address_Annotation.show();
            CityBounds.show();
            Elevation.hide();
            wards.hide();
            planning.hide();
            IM360.hide();
            watersheds.hide();
            sewer.hide();
            waterServices.hide();
            trafficSigns.hide();
            neighbourhood.hide();


            imgTopo.style.border = "1px solid black";
            imgOrtho2015.style.border = "1px solid black";
            imgOrtho2013.style.border = "1px solid black";
            imgOrtho2010.style.border = "1px solid black";
            imgOrtho2006.style.border = "4px solid black";
            imgOrtho2003.style.border = "1px solid black";
            //imgDEM.style.border = "1px solid black";
            document.getElementById("elevationDIV").style.display = "none";
            toolManager.set("currentTool", "");
            mode = "";
            break;
        case "ortho2003":
            topo.hide();
            ortho2015.hide();
            ortho2013.hide();
            ortho2010.hide();
            ortho2006.hide();
            ortho2003.show();
            DEM.hide();
            CityLabelsImagery.show();
            Property.hide();
            Annotation.show();
            Address_Annotation.show();
            CityBounds.show();
            Elevation.hide();
            wards.hide();
            planning.hide();
            IM360.hide();
            watersheds.hide();
            sewer.hide();
            waterServices.hide();
            trafficSigns.hide();
            neighbourhood.hide();


            imgTopo.style.border = "1px solid black";
            imgOrtho2015.style.border = "1px solid black";
            imgOrtho2013.style.border = "1px solid black";
            imgOrtho2010.style.border = "1px solid black";
            imgOrtho2006.style.border = "1px solid black";
            imgOrtho2003.style.border = "4px solid black";
            //imgDEM.style.border = "1px solid black";
            document.getElementById("elevationDIV").style.display = "none";
            toolManager.set("currentTool", "");
            mode = "";
            break;
        case "DEM":
            osmLayer.show();
            topo.hide();
            ortho2015.hide();
            ortho2013.hide();
            ortho2010.hide();
            ortho2006.hide();
            ortho2003.hide();
            DEM.hide();
            CityLabelsImagery.hide();
            Property.hide();
            Annotation.hide();
            Address_Annotation.hide();
            CityBounds.show();
            Elevation.hide();
            wards.hide();
            planning.hide();
            IM360.hide();
            watersheds.hide();
            sewer.hide();
            waterServices.hide();
            trafficSigns.hide();
            neighbourhood.hide();


            imgTopo.style.border = "1px solid black";
            imgOrtho2015.style.border = "1px solid black";
            imgOrtho2013.style.border = "1px solid black";
            imgOrtho2010.style.border = "1px solid black";
            imgOrtho2006.style.border = "1px solid black";
            imgOrtho2003.style.border = "1px solid black";
            //imgDEM.style.border = "4px solid black";
            //We want to show elevation values too
            toolManager.set("currentTool", "DEM");
            mode = "DEM";
            showElevationValues();
            break;
        case "osmLayer":
            alert("OSM");
            topo.hide();
            ortho2015.hide();
            ortho2013.hide();
            ortho2010.hide();
            ortho2006.hide();
            ortho2003.hide();
            osmLayer.show();
            imgTopo.style.border = "1px solid black";
            imgOrtho2015.style.border = "1px solid black";
            imgOrtho2013.style.border = "1px solid black";
            imgOrtho2010.style.border = "1px solid black";
            imgOrtho2006.style.border = "1px solid black";
            imgOrtho2003.style.border = "1px solid black";
            //imgDEM.style.border = "4px solid black";
            break;

        default:
            alert('Default case');
            break;
    }
}

function click_btnCompare(){
    $('#myModal').modal('hide');

    topo.hide();
    ortho2015.show();
    ortho2013.hide();
    ortho2010.hide();
    ortho2006.hide();
    ortho2003.hide();
    DEM.hide();
    CityLabelsImagery.show();
    Property.hide();
    Annotation.show();
    Address_Annotation.show();
    CityBounds.show();
    Elevation.hide();
    wards.hide();
    planning.hide();
    IM360.hide();
    watersheds.hide();
    sewer.hide();
    sewer.hide();
    waterServices.hide();
    trafficSigns.hide();
    neighbourhood.hide();


    document.getElementById("elevationDIV").style.display = "none";
    toolManager.set("currentTool", "");
    mode = "";

    imgTopo.style.border = "1px solid black";
    imgOrtho2013.style.border = "4px solid black";
    imgOrtho2013.style.border = "1px solid black";
    imgOrtho2010.style.border = "1px solid black";
    imgOrtho2006.style.border = "1px solid black";
    imgOrtho2003.style.border = "1px solid black";
    //imgDEM.style.border = "1px solid black";
}

/*----------------------------------+
|           Authentication          |
+----------------------------------*/
function click_CityTools(){
    printActivity("CityTools");
    document.getElementById("map_layers").style.cursor = "default";
    checkStillAlive();
    if (sessionStorage.getItem('value')){
        $("#dialog-city-tools").dialog({
            //height: 500,
            //width: 200,
            dialogClass: "alertDialog",
            resizable: false,
            //buttons: [{ text: "Ok", click: function() { $( this ).dialog( "close" ); } }],
            close: function (event, ui) {
                //closeDialog(number)
                $('#filteringACR').hide(); //If CityTools is closed
            },
            modal: false
        });
    }
    else $('#myLogin').modal('show');

    toolManager.set("currentTool", "neutral");
    mode = "neutral";
    map.setMapCursor("default");
}

function click_btnLogin(){
    $('#myLogin').modal('hide');
    var myUser = $('#myUser').val();
    var myPwd = $('#myPwd').val();

    $.support.cors = true;
    var jqxhr = $.ajax({
        //url: "http://gistestsrv2/test/ACR_connection.asmx/test",
        //url: "http://localhost/WS_ACR_Getting_PWD/WS_ACR_Getting_PWD.asmx/CheckPWD",
        url: "http://gistestsrv2/WS_ACR_Getting_PWD/WS_ACR_Getting_PWD.asmx/CheckPWD",
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: "{'user':'" + myUser + "', 'pwd':'" + myPwd + "'}",
        crossDomain: true,
        success: function(response) {
            $.each( response, function( key, value ) {
                if(value){
                    sessionStorage.setItem("value", value);
                    $("#dialog-city-tools").dialog({
                        //height: 500,
                        //width: 200,
                        dialogClass: "alertDialog",
                        resizable: false,
                        //buttons: [{ text: "Ok", click: function() { $( this ).dialog( "close" ); } }],
                        close: function (event, ui) {
                            //closeDialog(number)
                            $('#filteringACR').hide(); //If CityTools is closed
                        },
                        modal: false
                    });
                    //$("#caseType").select2();
                }
                else alert("Your login attempt was not successful")
            });
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) {
            alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
        }
    });
}

/*---------------------------------------------------------+
 |           Show or hide the FilteringACR Dialog          |
 +--------------------------------------------------------*/
function click_statusFilteringACR(){
    if ($('#filteringACR').is(":hidden")){

        $('#filteringACR').show();

        //Populate the list Case Type
        //$("#caseType").select2({
            //data:[{id:0,text:'enhancement'},{id:1,text:'bug'},{id:2,text:'duplicate'},{id:3,text:'invalid'},{id:4,text:'wontfix'}]
        //});

        $.support.cors = true;
        case_type = [];

        /*

        var selDep = document.getElementById('selectCategory');


        var jqxhr = $.ajax({
            url: "http://localhost/WS_ACR_GetComplaints/WebServiceGetComplaints.asmx/GetCategories",
            //url: "http://gistestsrv2/WS_ACR_GetComplaints/WebServiceGetComplaints.asmx/GetCategories",
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            beforeSend: function() {
                $('#selectCategory').html("<img src='./assests/images/loading.gif'/>");
            },
            success: function(response) {
                //Writing a default value
                var optDep2 = document.createElement('option');
                optDep2.selected = "true";
                //optDep2.disabled = "true";
                optDep2.innerHTML = "All Categories";
                optDep2.value = "All Categories";
                selDep.appendChild(optDep2);

                $.each( response, function ( i, val ) {
                    //alert(val.length);
                    $.each(val, function( key, value ) {
                        var optDep = document.createElement('option');
                        optDep.innerHTML = value;
                        optDep.value = value;
                        selDep.appendChild(optDep);
                    });
                });
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
            }
        });
        //Waiting for finishing the before AJAX called
        jqxhr.always(function() {
            var jqxhr2 = $.ajax({
                url: "http://localhost/WS_ACR_GetComplaints/WebServiceGetComplaints.asmx/GetNumberRecordsInFilteredSearch",
                //url: "http://gistestsrv2/WS_ACR_GetComplaints/WebServiceGetComplaints.asmx/GetNumberRecordsInFilteredSearch",
                type: 'POST',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: "{'ward':'" + $('#selectWard').val() + "', 'year':'" + $('#selectYear').val() + "', 'category':'" + $('#selectCategory').val() + "'}",
                crossDomain: true,
                beforeSend: function() {
                    $('#myTextCounter').html("<img src='./assests/images/loading.gif'/>");
                },
                success: function(response) {
                    $.each( response, function ( i, val ) {
                        $('#myTextCounter').html(val + " records");

                    });
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
                }
            });
        });
        */

        var jqxhr3 = $.ajax({
            //url: "http://localhost/WS_ACR_GetComplaints/WebServiceGetComplaints.asmx/GetCase_types",
            url: "http://gistestsrv2/WS_ACR_GetComplaints/WebServiceGetComplaints.asmx/GetCase_types",
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            beforeSend: function() {
                $('#myTextCounter').html("<img src='./assests/images/loading16.gif'/>");
            },
            success: function(response) {
                $.each( response, function ( i, val ) {
                    //alert(val.length);
                    $.each(val, function( key, value ) {
                        case_type.push({id:key,text:value});
                    });
                    case_type.sort(SortByText); //Sorting the results
                });
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
            }
        });
        jqxhr3.always(function() {
            //Populate the list Case Types
            $("#caseType").select2({
                placeholder: "All Case types ...",
                allowClear: true,
                closeOnSelect: true,
                multiple: true,
                data: case_type
            });

            var test = $('#caseType');
            var CaseTypeSelections = ($(test).select2('data'));
            var myList = [];
            $.each( CaseTypeSelections, function ( i, val ) {
                myList.push(val.text);
            });

            $.ajax({
                //url: "http://localhost/WS_ACR_GetComplaints/WebServiceGetComplaints.asmx/GetNumberRecordsInFilteredSearch",
                url: "http://gistestsrv2/WS_ACR_GetComplaints/WebServiceGetComplaints.asmx/GetNumberRecordsInFilteredSearch",
                type: 'POST',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: "{'ward':'" + $('#selectWard').val() + "', 'year':'" + $('#selectYear').val() + "', 'cases':'" + JSON.stringify(myList) + "'}",
                //data: "{'ward':'" + $('#selectWard').val() + "', 'year':'" + $('#selectYear').val() + "', 'cases':'Snow Clearing General Inquiry - Complaint'}",
                crossDomain: true,
                beforeSend: function () {
                    $('#myTextCounter').html("<img src='./assests/images/loading16.gif'/>");
                },
                success: function (response) {
                    $.each(response, function (i, val) {
                        if(val == "1")
                            $('#myTextCounter').html(val + " record found");
                        else $('#myTextCounter').html(val + " records found");
                        $('#myMapCounter').html("0 on Map");

                    });
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
                }
            });



        });

    }
    else $('#filteringACR').hide();

}

function SortByText(a, b){
    var aName = a.text.toLowerCase();
    var bName = b.text.toLowerCase();
    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

/*--------------------------------------------------+
|           Validating a date (yyyy-mm-dd)          |
+--------------------------------------------------*/
function isRightDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;

    var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[3];
    dtDay= dtArray[5];
    dtYear = dtArray[1];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay> 31)
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
        return false;
    else if (dtMonth == 2)
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap))
            return false;
    }
    return true;
}


function stopAnimation(element)
{
    $(element).css("-webkit-animation", "none");
    $(element).css("-moz-animation", "none");
    $(element).css("-ms-animation", "none");
    $(element).css("animation", "none");
}

function playAnimation(element)
{
    $(element).css("-webkit-animation-duration", "3s");
    $(element).css("-webkit-animation-iteration-count", "infinite");
    $(element).css("-webkit-animation-name", "pulse");

    $(element).css("-moz-animation-duration", "3s");
    $(element).css("-moz-animation-iteration-count", "infinite");
    $(element).css("-moz-animation-name", "pulse");

    $(element).css("-ms-animation-duration", "3s");
    $(element).css("-ms-animation-iteration-count", "infinite");
    $(element).css("-ms-animation-name", "pulse");

    $(element).css("animation-duration", "3s");
    $(element).css("animation-iteration-count", "infinite");
    $(element).css("animation-name", "pulse");
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function openNewWindowForVideoHelp() {
    myWindow = window.open('Mapcentre_Video_Help_Draw_tools.html','_blank','height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no');


}

$("#myGoToCoordinatesForm").submit(function() {
    require([
            "esri/map", "esri/toolbars/draw", "dojo/_base/connect", "esri/graphic", "esri/symbols/SimpleMarkerSymbol",
            "esri/symbols/SimpleLineSymbol", "esri/symbols/SimpleFillSymbol", "dojo/dom", "dojo/on", "esri/geometry/Point", "dojo/domReady!"],
        function(Map, Draw, connect, Graphic, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol, dom, on, Point) {

            //Previously Clear the Layer
            map.getLayer("layerPushpinGoToCoordinates").clear();

            var myOpt = $("#selectTypeCoordinates").val();
            switch (myOpt)
            {
                case "opProj":
                    //is numeric
                    if($.isNumeric($("#goX").val()) != false && $.isNumeric($("#goY").val()) != false){
                        var X = parseFloat($("#goX").val());
                        var Y = parseFloat($("#goY").val());
                        //Test Extent
                        if (X < 288990.972 || X > 347578.663 || Y < 5239463.940 || Y > 5280116.623)
                        {
                            $("#goX").val("");
                            $("#goY").val("");
                            alert("Coordinates out of range");
                            return;
                        }
                        var inPoint = new esri.geometry.Point([X,Y] , map.spatialReference);

                        if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x4")
                        {
                            map.centerAndZoom(inPoint, 4);
                        }
                        if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x6")
                        {
                            map.centerAndZoom(inPoint, 7);
                        }
                        if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x8")
                        {
                            map.centerAndZoom(inPoint, 10);
                        }
                        DrawSymbol(inPoint);
                    }

                    break;

                case "opGeo":
                    //is numeric
                    if($.isNumeric($("#goLat").val()) != false && $.isNumeric($("#goLon").val()) != false){
                        var X = parseFloat($("#goLon").val());
                        var Y = parseFloat($("#goLat").val());
                        //Test Extent
                        if (X < -53.209032 || X > -52.430442 || Y < 47.294312 || Y > 47.658772)
                        {
                            $("#goLon").val("");
                            $("#goLat").val("");
                            alert("Coordinates out of range");
                            return;
                        }
                        //Transformation here
                        var pt = transformCoordinates("transformFromGeographicToProjected", Y, X);
                        var inPoint = new esri.geometry.Point([pt.x, pt.y] , map.spatialReference);

                        if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x4")
                        {
                            map.centerAndZoom(inPoint, 4);
                        }
                        if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x6")
                        {
                            map.centerAndZoom(inPoint, 7);
                        }
                        if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x8")
                        {
                            map.centerAndZoom(inPoint, 10);
                        }
                        DrawSymbol(inPoint);
                    }
                    break;

                case "opGoogle":
                    var myGCoordinates = $("#goGoogle").val().split(",");
                    if(myGCoordinates.length != 2 || $.isNumeric(myGCoordinates[0]) != true || $.isNumeric(myGCoordinates[1]) != true) {
                        $("#goGoogle").val("");
                        alert("They are not a valid Google coordinates");
                        return;
                    }
                    else if(parseFloat(myGCoordinates[1]) < -53.209032 || parseFloat(myGCoordinates[1]) > -52.430442 || parseFloat(myGCoordinates[0]) < 47.294312 || parseFloat(myGCoordinates[0]) > 47.658772) {
                        $("#goGoogle").val("");
                        alert("Coordinates out of range");
                        return;
                    }
                    var X = parseFloat(myGCoordinates[1]);
                    var Y = parseFloat(myGCoordinates[0]);

                    //Transformation here
                    var pt = transformCoordinates("transformFromGeographicToProjected", Y, X);
                    var inPoint = new esri.geometry.Point([pt.x, pt.y] , map.spatialReference);

                    if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x4")
                    {
                        map.centerAndZoom(inPoint, 4);
                    }
                    if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x6")
                    {
                        map.centerAndZoom(inPoint, 7);
                    }
                    if($('input[name=optZoom]:checked', '#myGoToCoordinatesForm').val() == "x8")
                    {
                        map.centerAndZoom(inPoint, 10);
                    }
                    DrawSymbol(inPoint);

                    break;
            }
        });

    return false; // avoid to execute the actual submit of the form.
});

function DrawSymbol(myPoint) {
    require([
            "esri/map", "dojo/_base/connect", "esri/graphic", "esri/symbols/PictureMarkerSymbol",
            "dojox/gfx/move", "dojo/domReady!"],
        function(Map, connect, Graphic, PictureMarkerSymbol, move) {

            var infoSymbol = new esri.symbol.PictureMarkerSymbol({
                "angle":0,
                "xoffset":0,
                "yoffset":7,
                "type":"esriPMS",
                "url":"http://static.arcgis.com/images/Symbols/Basic/PurpleShinyPin.png",
                "contentType":"image/png",
                "width":24,
                "height":24});

            graphicPushpin = new Graphic(myPoint, infoSymbol);
            gLayerPushpinGoToCoordinates.add(graphicPushpin);

            //Update te inputs. when we use the pushpin, before the movement
            switch ($( "#selectTypeCoordinates").val())
            {
                case "opProj":
                    $("#goX").val((myPoint.x).toFixed(2));
                    $("#goY").val((myPoint.y).toFixed(2));
                    break;
                case "opGeo":
                    var pt = transformCoordinates("transformFromProjectedToGeographic", myPoint.x, myPoint.y);
                    $("#goLat").val((pt.y).toFixed(7));
                    $("#goLon").val((pt.x).toFixed(7));
                    break;

                case "opGoogle":
                    var pt = transformCoordinates("transformFromProjectedToGoogle", myPoint.x, myPoint.y);
                    var myGoogleCoord = (pt.y).toFixed(7) + "," + (pt.x).toFixed(7);
                    $("#goGoogle").val(myGoogleCoord);
                    break;
            }

             // http://docs.dojocampus.org/dojox/gfx#coordinates-and-transformations
             var dojoShape = graphicPushpin.getDojoShape();
             var moveable = new dojox.gfx.Moveable(dojoShape);

             var moveStopToken = dojo.connect(moveable, "onMoveStop", function(mover) {
                 // Get the transformation that was applied to
                 // the shape since the last move
                 var tx = dojoShape.getTransform();

                 var startPoint = graphicPushpin.geometry;
                 var endPoint = map.toMap(map.toScreen(startPoint).offset(tx.dx, tx.dy+8));

                 //We can move the pushpin and its coordinates will go here
                 switch ($( "#selectTypeCoordinates").val())
                 {
                     case "opProj":
                         $("#goX").val((endPoint.x).toFixed(2));
                         $("#goY").val((endPoint.y).toFixed(2));
                         break;
                     case "opGeo":
                         var pt = transformCoordinates("transformFromProjectedToGeographic", endPoint.x, endPoint.y);
                         $("#goLat").val((pt.y).toFixed(7));
                         $("#goLon").val((pt.x).toFixed(7));
                         break;

                     case "opGoogle":
                         var pt = transformCoordinates("transformFromProjectedToGoogle", endPoint.x, endPoint.y);
                         var myGoogleCoord = (pt.y).toFixed(7) + "," + (pt.x).toFixed(7);
                         $("#goGoogle").val(myGoogleCoord);
                         break;
                 }

                 // clear out the transformation
                 dojoShape.setTransform(null);

                 // update the graphic geometry
                 graphicPushpin.setGeometry(endPoint);
             });

        });
}

function DrawSymbolGoogleStreetView(myPoint) {
    //console.log((myPoint.x).toFixed(3) + "; " + (myPoint.y).toFixed(3));
    pt2 = transformCoordinates("transformFromProjectedToGeographic", (myPoint.x).toFixed(3), (myPoint.y).toFixed(3));
    //alert((pt2.x).toFixed(7));
    //alert((pt2.y).toFixed(7));

    window.open("Mapcentre_Google_StreetView.html?Lat=" + (pt2.y).toFixed(7) + "&Lon=" + (pt2.x).toFixed(7), "_blank", "height=640,width=840, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no");
}

function transformCoordinates(typeTransf, arg1, arg2) {
    var pt;
    switch (typeTransf)
    {
        case "transformFromGeographicToProjected":
            var x = parseFloat(arg2);
            var y = parseFloat(arg1);

            var source = new Proj4js.Proj("EPSG:4326");    //source coordinates will be in Longitude/Latitude
            //Proj4js.defs["EPSG:4326"] = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
            Proj4js.defs["EPSG:32181"] = "+proj=tmerc +lat_0=0 +lon_0=-53 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs";
            var dest = new Proj4js.Proj("EPSG:32181");
            pt = new Proj4js.Point(x,y);
            Proj4js.transform(source, dest, pt);
            break;

        case "transformFromGoogleToProjected":
            var coords = arg1.split(",");
            var x = parseFloat(coords[1]);
            var y = parseFloat(coords[0]);

            var source = new Proj4js.Proj("EPSG:4326");    //source coordinates will be in Longitude/Latitude
            Proj4js.defs["EPSG:32181"] = "+proj=tmerc +lat_0=0 +lon_0=-53 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs";
            var dest = new Proj4js.Proj("EPSG:32181");
            pt = new Proj4js.Point(x,y);
            Proj4js.transform(source, dest, pt);

            break;

        case "transformFromProjectedToGeographic":
            var x = parseFloat(arg1);
            var y = parseFloat(arg2);

            Proj4js.defs["EPSG:32181"] = "+proj=tmerc +lat_0=0 +lon_0=-53 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs";
            Proj4js.defs["EPSG:4326"] = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
            var source = new Proj4js.Proj("EPSG:32181");

            var dest = new Proj4js.Proj("EPSG:4326");
            pt = new Proj4js.Point(x, y);
            Proj4js.transform(source, dest, pt);
            break;

        case "transformFromGoogleToGeographic":
            var coords = arg1.split(",");
            var x = parseFloat(coords[1]);
            var y = parseFloat(coords[0]);

            pt = new esri.geometry.Point([x,y],new esri.SpatialReference({ wkid:4326 }));
            break;

        case "transformFromProjectedToGoogle":
            var x = parseFloat(arg1);
            var y = parseFloat(arg2);

            Proj4js.defs["EPSG:32181"] = "+proj=tmerc +lat_0=0 +lon_0=-53 +k=0.9999 +x_0=304800 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs";
            Proj4js.defs["EPSG:4326"] = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
            var source = new Proj4js.Proj("EPSG:32181");
            var dest = new Proj4js.Proj("EPSG:4326");
            pt = new Proj4js.Point(x, y);
            Proj4js.transform(source, dest, pt);
            break;

        case "transformFromGeographicToGoogle":
            var x = parseFloat(arg2);
            var y = parseFloat(arg1);

            pt = new esri.geometry.Point([x,y],new esri.SpatialReference({ wkid:4326 }));
            break;
    }

    return pt;
}

function checkBrowser(){
    //Calling to the web service to check the browser
    var checkBrowserAJAX01 = $.ajax({
        crossDomain: true,
        type: "POST",
        url: "http://listestsrv.stjohnstest.com/BrowserCapabilities/BrowserCapabilities.asmx/isValidBrowser",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: function() {
            document.getElementById('browserWarningOrSuccess').className += ' alert-success';
            $('#browserWarningOrSuccess')
                .append("<img src='./assests/images/loading16.gif'/>");
        },
        success: function(result) {
            $.each( result, function ( i, val ) {
                if(val > 0)
                {
                    document.getElementById('browserWarningOrSuccess').className += ' alert-success';
                    $("#browserWarningOrSuccess").children("img:first").remove(); //Removing loading image
                    $('#browserWarningOrSuccess')
                        .append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>")
                        .append("<strong>Well done!</strong> Your browser is updated. <strong>Enjoy Mapcentre!</strong>");
                    $('#placeForBrowserVersions').remove(); //We don't need this DIV anymore since it is TRUE
                }
                else
                {
                    $.ajax({
                        crossDomain: true,
                        type: "POST",
                        url: "http://listestsrv.stjohnstest.com/BrowserCapabilities/BrowserCapabilities.asmx/getBrowser",
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        success: function(result) {
                            $.each( result, function ( i, val ) {
                                var myResult = String(val);
                                var arr = myResult.split(",");

                                document.getElementById('browserWarningOrSuccess').className += ' alert-warning';
                                $('#browserWarningOrSuccess')
                                    .append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>")
                                    .append("<strong>Warning!</strong> To properly use Mapcentre, you need to update your browser.<br>")
                                    .append("Your current browser is: <strong>" + arr[0] + "</strong>, Version: " + arr[1]);

                                //I need to add an image for supported mobile browsers
                                //$('#placeForBrowserVersions')
                                    //.append("<img src='assests/images/Browsers.png' height='52px' width='auto'>");

                                var $div = $("<img src='./assests/images/Browsers.png' height='58px' width='180' alt='Supported browsers'>");
                                $("#placeForBrowserVersions").append($div);
                            });
                        },
                        error: function (xmlHttpRequest, textStatus, errorThrown) {
                            alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
                        }
                    });
                }
                $( "#browserWarningOrSuccess" ).attr("style", "visibility: visible");
            });
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) {
            alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
        }
    });

}

/*--------------------------------------------------------------+
|   Writing more code here for reducing the load in index.html  |
+--------------------------------------------------------------*/
$(function(){
    //Changing input coordinates according its type
    $( "#selectTypeCoordinates" ).change(function() {
        //First, we need to know where are the coordinates from
        var pt;
        var storeCoord = [];
        storeCoord.push($("#goX").val());
        storeCoord.push($("#goY").val());
        storeCoord.push($("#goLat").val());
        storeCoord.push($("#goLon").val());
        storeCoord.push($("#goGoogle").val());
        var opt = this.value;

        //Loop for each element in order to remove them
        $('#inputZone :input').each(function () {
            storeCoord.push(this.id);
            $('#inputZone').children($(this).id).remove();
        });

        switch (opt)
        {
            case "opProj":
                //And new, we create the new one
                $('#inputZone')
                    .append("<input class='form-control' id='goX' type='text' name='x' placeholder='X' style='width: 49%' required>");
                $('#inputZone')
                    .append("<input class='form-control' id='goY' type='text' name='y' placeholder='Y' style='width: 49%; float: right' required>");

                if(storeCoord[2] != "" || storeCoord[3] != "") {
                    if (storeCoord[5] === "goLat") {
                        pt = transformCoordinates("transformFromGeographicToProjected", storeCoord[2], storeCoord[3]);
                        $("#goX").val((pt.x).toFixed(2));
                        $("#goY").val((pt.y).toFixed(2));
                    }
                }
                if(storeCoord[4] != "") {
                    if(storeCoord[5] === "goGoogle") {
                        pt = transformCoordinates("transformFromGoogleToProjected", storeCoord[4], null);
                        $("#goX").val((pt.x).toFixed(2));
                        $("#goY").val((pt.y).toFixed(2));
                    }
                }

                break;

            case "opGeo":
                //And new, we create the new one
                $('#inputZone')
                    .append("<input class='form-control' id='goLat' type='text' name='lat' placeholder='Lat' style='width: 49%' required>");
                $('#inputZone')
                    .append("<input class='form-control' id='goLon' type='text' name='lon' placeholder='Lon' style='width: 49%; float: right' required>");

                if(storeCoord[0] != "" || storeCoord[1] != "") {
                    if (storeCoord[5] === "goX") {
                        pt = transformCoordinates("transformFromProjectedToGeographic", storeCoord[0], storeCoord[1]);
                        $("#goLat").val((pt.y).toFixed(7));
                        $("#goLon").val((pt.x).toFixed(7));
                    }
                }
                if(storeCoord[4] != "") {
                    if (storeCoord[5] === "goGoogle") {
                        pt = transformCoordinates("transformFromGoogleToGeographic", storeCoord[4], null);
                        $("#goLat").val((pt.y).toFixed(7));
                        $("#goLon").val((pt.x).toFixed(7));
                    }
                }
                break;

            case "opGoogle":
                //And new, we create the new one
                $('#inputZone')
                    .append("<input class='form-control' id='goGoogle' type='text' name='google' placeholder='Copy it from Google' style='width: 100%' required>");

                if(storeCoord[0] != "" || storeCoord[1] != "") {
                    if (storeCoord[5] === "goX") {
                        pt = transformCoordinates("transformFromProjectedToGoogle", storeCoord[0], storeCoord[1]);
                        var myGoogleCoord = (pt.y).toFixed(7) + "," + (pt.x).toFixed(7);
                        $("#goGoogle").val(myGoogleCoord);
                    }
                }
                if(storeCoord[2] != "" || storeCoord[3] != "") {
                    if (storeCoord[5] === "goLat") {
                        pt = transformCoordinates("transformFromGeographicToGoogle", storeCoord[2], storeCoord[3]);
                        var myGoogleCoord = (pt.y).toFixed(7) + "," + (pt.x).toFixed(7);
                        $("#goGoogle").val(myGoogleCoord);
                    }
                }
                break;
        }


    });

    if (document.getElementById('btnDrawPushpin').addEventListener) {
        document.getElementById('btnDrawPushpin').addEventListener("click", function() {
            toolManager.set("currentTool", "pushpin");
            mode = "pushpin";
            map.getLayer('layerPushpinGoToCoordinates').clear();
            //map.setMapCursor("url('assests/images/PurpleShinyPin.cur') 24 40, auto"); //Be careful with the offset
            //console.log("mode pushpin");
            document.getElementById("map_layers").style.cursor = "url('assests/images/Cursor1.cur'), auto";
        }, false);
    }
    else {
        document.getElementById('btnDrawPushpin').attachEvent("onclick", function() {
            toolManager.set("currentTool", "pushpin");
            mode = "pushpin";
            map.getLayer('layerPushpinGoToCoordinates').clear();
            //map.setMapCursor("url('assests/images/PurpleShinyPin.cur') 24 40, auto"); //Be careful with the offset
            document.getElementById("map_layers").style.cursor = "url('assests/images/Cursor1.cur'), auto";
        });
    }

    if (document.getElementById('checkDisclaimer').addEventListener) {
        document.getElementById('checkDisclaimer').addEventListener("click", function() {
            if($("#checkDisclaimer").is(':checked'))
                $('#diclaimerAgree').prop('disabled', false);
            else
                $('#diclaimerAgree').prop('disabled', true);
        }, false);
    }
    else {
        document.getElementById('checkDisclaimer').attachEvent("onclick", function() {
            if($("#checkDisclaimer").is(':checked'))
                $('#diclaimerAgree').prop('disabled', false);
            else
                $('#diclaimerAgree').prop('disabled', true);
        });
    }


    var myObject = document.getElementById('myShowAboutMapcentre');
    myObject.onshow=function(){

        $('#myContact').hide();

        angular.element(document.getElementById('myCounter')).scope().getCount();
        //angular.element(document.getElementById('myCounter')).scope().countup();
    };
    myObject.onhide=function(){
        $('#myContact').show();
        angular.element(document.getElementById('myCounter')).scope().reset();
    };


    if (document.getElementById('mybtn').addEventListener) {
        document.getElementById('mybtn').addEventListener("click", function() {
            $.ajax({
                type: "POST",
                //url: "email.php", //if using PHP
                url: "http://listestsrv.stjohnstest.com/webserviceemail/webserviceemail.asmx/sendEmail",
                //data: { 'val':$("#myContactForm").serializeJSON() }, //if using PHP
                data: "{'name':'" + $('#contactName').val() + "', 'email':'" + $('#email').val() + "', 'msg':'" + $('#contactMsg').val() + "'}",
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    $('#waitingForSendingEmail').show();
                    $('#waitingForSendingEmail').html("<img src='./assests/images/loading16.gif'/>");
                },
                success: function(response)
                {
                    //alert(response.d)

                    //alert(data); // show response from the php script.
                    //$('#myContactForm').hide();
                    $('#waitingForSendingEmail').hide()
                    if(response.d == 1001) {

                        $('#success').html("<div class='alert alert-success'>");

                        $('#success > .alert-success')
                            .append("<strong>Your message has been sent.<br>Thanks for your feedback</strong>");
                        $('#success > .alert-success')
                            .append('</div>');

                        $('#myContactForm').trigger("reset");
                        $('#myContactForm').hide();
                    }
                    else {
                        //alert(data);
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");

                        $('#success > .alert-danger').append("<strong>Sorry, it seems that the mail server is not responding...</strong>");
                        $('#success > .alert-danger').append('</div>');
                        //clear all fields
                        $('#myContactForm').trigger("reset");
                    }

                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
                }
            });
        }, false);
    }
    else {
        document.getElementById('mybtn').attachEvent("onclick", function() {
            $.ajax({
                type: "POST",
                //url: "email.php", //if using PHP
                url: "http://listestsrv.stjohnstest.com/webserviceemail/webserviceemail.asmx/sendEmail",
                //data: { 'val':$("#myContactForm").serializeJSON() }, //if using PHP
                data: "{'name':'" + $('#contactName').val() + "', 'email':'" + $('#email').val() + "', 'msg':'" + $('#contactMsg').val() + "'}",
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    $('#waitingForSendingEmail').show();
                    $('#waitingForSendingEmail').html("<img src='./assests/images/loading16.gif'/>");
                },
                success: function (response) {
                    //alert(response.d)

                    //alert(data); // show response from the php script.
                    //$('#myContactForm').hide();
                    $('#waitingForSendingEmail').hide()
                    if (response.d == 1001) {

                        $('#success').html("<div class='alert alert-success'>");

                        $('#success > .alert-success')
                            .append("<strong>Your message has been sent.<br>Thanks for your feedback</strong>");
                        $('#success > .alert-success')
                            .append('</div>');

                        $('#myContactForm').trigger("reset");
                        $('#myContactForm').hide();
                    }
                    else {
                        //alert(data);
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");

                        $('#success > .alert-danger').append("<strong>Sorry, it seems that the mail server is not responding...</strong>");
                        $('#success > .alert-danger').append('</div>');
                        //clear all fields
                        $('#myContactForm').trigger("reset");
                    }

                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert(xmlHttpRequest.responseText + "| " + textStatus + "| " + errorThrown);
                }
            });
        });
    }


});