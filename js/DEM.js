/*------------------------------------------------------+
 |  Writing more code here for minimizing index.html    |
 |  Javascript on CSJ ------ MAPCENTRE Application      |
 +-----------------------------------------------------*/
//Global here
var identifyMyTask;
var identifyMyParams;

/*------------------------+
 |       DEM FUNCTIONS    |
 +-----------------------*/
function showElevationValues(){ /*Function coming from myDragDrop.js*/
    require([
        "esri/map", "esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters"], function(Map, IdentifyTask, IdentifyParameters) {

        /*
        for(var j = 0; j < map.layerIds.length; j++) {
            var layer = map.getLayer(map.layerIds[j]);
            alert(layer.id + ' ' + layer.url + ' ' + layer.visible);
        }
        */
        $( "#elevationValue" ).text("Click on the map for getting elevation values");
        document.getElementById("elevationDIV").style.height = "50px";
        document.getElementById("elevationDIV").style.width = "200px";
        document.getElementById("elevationDIV").style.marginLeft = "-100px";
        document.getElementById("elevationDIV").style.display = "block";


        identifyMyTask = null; //Reinitialization for each identify
        identifyMyParams = null; //Reinitialization for each identify

        //create identify tasks and setup parameters
        identifyMyTask = new IdentifyTask("http://lisapp01:6080/arcgis/rest/services/MapCentre/STJ_MC_2013_DEM_MapService/MapServer");

        identifyMyParams = new IdentifyParameters();
        identifyMyParams.tolerance = 1;
        identifyMyParams.returnGeometry = false;
        identifyMyParams.layerIds = [0];
        identifyMyParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
        identifyMyParams.width = map.width;
        identifyMyParams.height = map.height;

    });
}

